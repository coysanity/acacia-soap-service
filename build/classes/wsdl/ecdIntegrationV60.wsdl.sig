<?xml version="1.0" encoding="UTF-8"?>

<wsdl:definitions xmlns:ecm="urn:com:ecmhp"
	xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:xsd="http://www.w3.org/2001/XMLSchema"
	xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" name="ecdSpecV60"
	targetNamespace="urn:com:ecmhp">

	<xsd:annotation>
		<xsd:documentation xml:lang="en">
			Copyright (C) 2013 and all subsequent years, eCM (eCurrency.net).  All rights reserved.
		</xsd:documentation>
	</xsd:annotation>

	<!--  ====================================
	Define various complex types used to construct requests and responses
	 -->
	<wsdl:types>
		<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:ecm="urn:com:ecmhp" xmlns="http://www.tm-xml.org/XMLSchema/common" attributeFormDefault="unqualified" elementFormDefault="qualified" targetNamespace="urn:com:ecmhp">

			<xsd:include schemaLocation="ecdCommonModelV60.xsd" />

			<xsd:element name="getEcdRequest" type="ecm:getEcdRequest">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">
						=====
						getEcdRequest:: is one of several actual requests accepted by the eCMS system. The content of the request is as documented above.
						ecdHeader is an arbitrary string guaranteed to be returned by eCMS unchanged with the response corresponding to this request
						=====
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="getEcdResponse" type="ecm:getEcdResponse">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">
						=====
						getEcdResponse:: is one of several responses emitted by the eCMS system. The content of the response is as documented above.
						The response returns information about the ECD as known by eCMS at the time the corresponding request is processed.
						=====
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>

			<xsd:element name="getSubscriberRequest" type="ecm:getSubscriberRequest">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">
						=====
						getSubscriberRequest:: is one of several actual requests accepted by the eCMS system. The content of the request is as documented above.
						ecdHeader is an arbitrary string guaranteed to be returned by eCMS unchanged with the response corresponding to this request
						=====
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="getSubscriberResponse" type="ecm:getSubscriberResponse">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">
						=====
						getSubscriberResponse:: is one of several responses emitted by the eCMS system. The content of the response is as documented above.
						The response returns information about the subscriber as known by eCMS at the time the corresponding request is processed.
						=====
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>

			<xsd:element name="setSubscriberRequest" type="ecm:setSubscriberRequest">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">
						=====
						setSubscriberRequest:: is one of several actual requests accepted by the eCMS system. The content of the request is as documented above.
						The requests sets values of non-currency subscriber attributes
						=====
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="setSubscriberResponse" type="ecm:setSubscriberResponse">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">
						=====
						setSubscriberResponse:: is one of several responses emitted by the eCMS system. The content of the response is as documented above.
						The response confirms execution of teh set request and returns information about the subscriber as known by eCMS after processing of the corresponding request.
						=====
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>

			<xsd:element name="addSubscriberRequest" type="ecm:addSubscriberRequest">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">
						=====
						addSubscriberRequest:: is one of several actual requests accepted by the eCMS system. The content of the request is as documented above.
						Thasi request causes a new subscriber to be added to eCMS.
						=====
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="addSubscriberResponse" type="ecm:addSubscriberResponse">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">
						=====
						addSubscriberResponse:: is one of several responses emitted by the eCMS system. The content of the response is as documented above.
						Upon successful completion of the corresponding request a new subscriber will have been added to the eCMS. The initial eCurrency unit assigned to the new
						subscriber has value of zero.
						=====
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>

            <xsd:element name="ecdTransferRequest" type="ecm:ecdTransferRequest">
                <xsd:annotation>
                    <xsd:documentation xml:lang="en">
                        =====
ecdTransferRequest:: is one of several actual requests accepted by the eCMS system. The content of the request is as documented above.
This request supports currency distribution from central bank master vault to commerial bank master vault or ECD master vault.
                        =====
                    </xsd:documentation>
                </xsd:annotation>
            </xsd:element>
            <xsd:element name="ecdTransferResponse" type="ecm:ecdTransferResponse">
                <xsd:annotation>
                    <xsd:documentation xml:lang="en">
                        =====
ecdTransferResponse:: is one of several responses emitted by the eCMS system. The content of the response is as documented above.
The response returns information about the new state of participating eCurrency units, including cryptograms to be persisted by the commerical bank core banking or ECD payment systems.
                        =====
                    </xsd:documentation>
                </xsd:annotation>
            </xsd:element>

			<xsd:element name="subscriberTransferRequest" type="ecm:subscriberTransferRequest">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">
						=====
subscriberTransferRequest:: is one of several actual requests accepted by the eCMS system. The content of the request is as documented above.
This request is the 'workhorse' of the system. From the eCurrency perspective, most transactions are transfers of currency between two parties.
						=====
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="subscriberTransferResponse" type="ecm:subscriberTransferResponse">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">
						=====
subscriberTransferResponse:: is one of several responses emitted by the eCMS system. The content of the response is as documented above.
The response returns information about the new state of participating subscriber eCurrency units, including cryptograms to be persisted by the ECD payment systems
						=====
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>

			<xsd:element name="cashInRequest" type="ecm:cashInRequest">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">
						=====
cashInRequest:: is one of several actual requests accepted by the eCMS system. The content of the request is as documented above.
All transactions adding value to subscriber eCurrency units from outside of the eCurrency system are considered cash-ins
						=====
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="cashInResponse" type="ecm:cashInResponse">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">
						=====
cashInResponse:: is one of several responses emitted by the eCMS system. The content of the response is as documented above.
The response returns information about the new state of subscriber's eCurrency unit, including cryptograms to be persisted by the ECD payment systems
						=====
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>

			<xsd:element name="cashOutRequest" type="ecm:cashOutRequest">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">
						=====
cashOutRequest:: is one of several actual requests accepted by the eCMS system. The content of the request is as documented above.
All transactions removing value from subscriber eCurrency units and moving it outside of the eCurrency system are considered cash-outs
						=====
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="cashOutResponse" type="ecm:cashOutResponse">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">
						=====
cashOutResponse:: is one of several responses emitted by the eCMS system. The content of the response is as documented above.
The response returns information about the new state of subscriber's eCurrency unit, including cryptograms to be persisted by the ECD payment systems.
						=====
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>

			<xsd:element name="deleteSubscriberRequest" type="ecm:deleteSubscriberRequest">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">
						=====
deleteSubscriberRequest:: is one of several actual requests accepted by the eCMS system. The content of the request is as documented above.
The requests results in removal of a subscriber from eCMS. Only subscribers with zero value in their eCurrency units can be removed
						=====
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="deleteSubscriberResponse" type="ecm:deleteSubscriberResponse">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">
						=====
deleteSubscriberResponse:: is one of several responses emitted by the eCMS system. The content of the response is as documented above.
The response confirms successful deletion of a subscriber
						=====
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>

			<xsd:element name="transferGroupRequest" type="ecm:transferGroupRequest">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">
						=====
						batchRequest:: process a number of requests in a single atomic transaction. Requests are processed in order.
						The result of execution will be either all successes (transaction committed atomically) or nothing is changed (transaction aborted/rollback).
						=====
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="transferGroupResponse" type="ecm:transferGroupResponse">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">
						=====
						batchResponse:: the result of execution in the order of requests submitted
						=====
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>

			<xsd:element name="faultResponse" type="ecm:faultResponse">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">
						=====
faultResponse:: is the content of the SOAP fault message returned in case of processing or data validation errors
All error conditions return the same faultResponse structure. This allows for streamlined fault processing.
						=====
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>

		</xsd:schema>
	</wsdl:types>

	<!-- =================================================
		Actual wsdl messages built from Requests and Responses defined above
	 -->
	<wsdl:message name="getEcdRequestMessage">
		<wsdl:part name="parameters" element="ecm:getEcdRequest">
		</wsdl:part>
	</wsdl:message>
	<wsdl:message name="getEcdResponseMessage">
		<wsdl:part name="parameters" element="ecm:getEcdResponse">
		</wsdl:part>
	</wsdl:message>

	<wsdl:message name="getSubscriberRequestMessage">
		<wsdl:part name="parameters" element="ecm:getSubscriberRequest">
		</wsdl:part>
	</wsdl:message>
	<wsdl:message name="getSubscriberResponseMessage">
		<wsdl:part name="parameters" element="ecm:getSubscriberResponse">
		</wsdl:part>
	</wsdl:message>

	<wsdl:message name="setSubscriberRequestMessage">
		<wsdl:part name="parameters" element="ecm:setSubscriberRequest">
		</wsdl:part>
	</wsdl:message>
	<wsdl:message name="setSubscriberResponseMessage">
		<wsdl:part name="parameters" element="ecm:setSubscriberResponse">
		</wsdl:part>
	</wsdl:message>

	<wsdl:message name="addSubscriberRequestMessage">
		<wsdl:part name="parameters" element="ecm:addSubscriberRequest">
		</wsdl:part>
	</wsdl:message>
	<wsdl:message name="addSubscriberResponseMessage">
		<wsdl:part name="parameters" element="ecm:addSubscriberResponse">
		</wsdl:part>
	</wsdl:message>

    <wsdl:message name="deleteSubscriberRequestMessage">
        <wsdl:part name="parameters" element="ecm:deleteSubscriberRequest">
        </wsdl:part>
    </wsdl:message>
    <wsdl:message name="deleteSubscriberResponseMessage">
        <wsdl:part name="parameters" element="ecm:deleteSubscriberResponse">
        </wsdl:part>
    </wsdl:message>

    <wsdl:message name="ecdTransferRequestMessage">
        <wsdl:part name="parameters" element="ecm:ecdTransferRequest">
        </wsdl:part>
    </wsdl:message>
    <wsdl:message name="ecdTransferResponseMessage">
        <wsdl:part name="parameters" element="ecm:ecdTransferResponse">
        </wsdl:part>
    </wsdl:message>

    <wsdl:message name="subscriberTransferRequestMessage">
        <wsdl:part name="parameters" element="ecm:subscriberTransferRequest">
        </wsdl:part>
    </wsdl:message>
    <wsdl:message name="subscriberTransferResponseMessage">
        <wsdl:part name="parameters" element="ecm:subscriberTransferResponse">
        </wsdl:part>
    </wsdl:message>

	<wsdl:message name="cashInRequestMessage">
		<wsdl:part name="parameters" element="ecm:cashInRequest">
		</wsdl:part>
	</wsdl:message>
	<wsdl:message name="cashInResponseMessage">
		<wsdl:part name="parameters" element="ecm:cashInResponse">
		</wsdl:part>
	</wsdl:message>

    <wsdl:message name="cashOutRequestMessage">
        <wsdl:part name="parameters" element="ecm:cashOutRequest">
        </wsdl:part>
    </wsdl:message>
    <wsdl:message name="cashOutResponseMessage">
        <wsdl:part name="parameters" element="ecm:cashOutResponse">
        </wsdl:part>
    </wsdl:message>

	<wsdl:message name="transferGroupRequestMessage">
		<wsdl:part name="parameters" element="ecm:transferGroupRequest">
		</wsdl:part>
	</wsdl:message>
	<wsdl:message name="transferGroupResponseMessage">
		<wsdl:part name="parameters" element="ecm:transferGroupResponse">
		</wsdl:part>
	</wsdl:message>

	<wsdl:message name="faultMessage">
		<wsdl:part name="parameters" element="ecm:faultResponse"></wsdl:part>
	</wsdl:message>

	<!-- ===========================================
		Finally port definition built from all of the above messages
	-->

	<wsdl:portType name="ecdV60SOAPPort">
		<wsdl:operation name="getEcd">
			<wsdl:input message="ecm:getEcdRequestMessage"></wsdl:input>
			<wsdl:output message="ecm:getEcdResponseMessage"></wsdl:output>
            <wsdl:fault name="fault" message="ecm:faultMessage"></wsdl:fault>
        </wsdl:operation>
		<wsdl:operation name="getSubscriber">
			<wsdl:input message="ecm:getSubscriberRequestMessage"></wsdl:input>
			<wsdl:output message="ecm:getSubscriberResponseMessage"></wsdl:output>
            <wsdl:fault name="fault" message="ecm:faultMessage"></wsdl:fault>
        </wsdl:operation>
		<wsdl:operation name="addSubscriber">
			<wsdl:input message="ecm:addSubscriberRequestMessage"></wsdl:input>
			<wsdl:output message="ecm:addSubscriberResponseMessage"></wsdl:output>
			<wsdl:fault name="fault" message="ecm:faultMessage"></wsdl:fault>
		</wsdl:operation>
		<wsdl:operation name="setSubscriber">
			<wsdl:input message="ecm:setSubscriberRequestMessage"></wsdl:input>
			<wsdl:output message="ecm:setSubscriberResponseMessage"></wsdl:output>
			<wsdl:fault name="fault" message="ecm:faultMessage"></wsdl:fault>
		</wsdl:operation>
        <wsdl:operation name="deleteSubscriber">
            <wsdl:input message="ecm:deleteSubscriberRequestMessage"></wsdl:input>
            <wsdl:output message="ecm:deleteSubscriberResponseMessage"></wsdl:output>
            <wsdl:fault name="fault" message="ecm:faultMessage"></wsdl:fault>
        </wsdl:operation>
        <wsdl:operation name="performEcdTransfer">
            <wsdl:input message="ecm:ecdTransferRequestMessage"></wsdl:input>
            <wsdl:output message="ecm:ecdTransferResponseMessage"></wsdl:output>
            <wsdl:fault name="fault" message="ecm:faultMessage"></wsdl:fault>
        </wsdl:operation>
        <wsdl:operation name="performSubscriberTransfer">
            <wsdl:input message="ecm:subscriberTransferRequestMessage"></wsdl:input>
            <wsdl:output message="ecm:subscriberTransferResponseMessage"></wsdl:output>
            <wsdl:fault name="fault" message="ecm:faultMessage"></wsdl:fault>
        </wsdl:operation>
		<wsdl:operation name="performCashIn">
			<wsdl:input message="ecm:cashInRequestMessage"></wsdl:input>
			<wsdl:output message="ecm:cashInResponseMessage"></wsdl:output>
			<wsdl:fault name="fault" message="ecm:faultMessage"></wsdl:fault>
		</wsdl:operation>
		<wsdl:operation name="performCashOut">
			<wsdl:input message="ecm:cashOutRequestMessage"></wsdl:input>
			<wsdl:output message="ecm:cashOutResponseMessage"></wsdl:output>
			<wsdl:fault name="fault" message="ecm:faultMessage"></wsdl:fault>
		</wsdl:operation>
		<wsdl:operation name="performTransferGroup">
			<wsdl:input message="ecm:transferGroupRequestMessage"></wsdl:input>
			<wsdl:output message="ecm:transferGroupResponseMessage"></wsdl:output>
			<wsdl:fault name="fault" message="ecm:faultMessage"></wsdl:fault>
		</wsdl:operation>

	</wsdl:portType>

	<!--
	Binding of messages to a port
	 -->
	<wsdl:binding name="ecdV60SOAP" type="ecm:ecdV60SOAPPort">
		<soap:binding style="document"
			transport="http://schemas.xmlsoap.org/soap/http" />
		<wsdl:operation name="getEcd">
			<soap:operation />
			<wsdl:input>
				<soap:body use="literal" />
			</wsdl:input>
			<wsdl:output>
				<soap:body use="literal" />
			</wsdl:output>
			<wsdl:fault name="fault">
				<soap:fault use="literal" name="fault"/>
			</wsdl:fault>
		</wsdl:operation>

		<wsdl:operation name="getSubscriber">
			<soap:operation />
			<wsdl:input>
				<soap:body use="literal" />
			</wsdl:input>
			<wsdl:output>
				<soap:body use="literal" />
			</wsdl:output>
			<wsdl:fault name="fault">
				<soap:fault use="literal" name="fault"/>
			</wsdl:fault>
		</wsdl:operation>

		<wsdl:operation name="addSubscriber">
			<soap:operation />
			<wsdl:input>
				<soap:body use="literal" />
			</wsdl:input>
			<wsdl:output>
				<soap:body use="literal" />
			</wsdl:output>
			<wsdl:fault name="fault">
				<soap:fault use="literal" name="fault"/>
			</wsdl:fault>
		</wsdl:operation>

		<wsdl:operation name="setSubscriber">
			<soap:operation />
			<wsdl:input>
				<soap:body use="literal" />
			</wsdl:input>
			<wsdl:output>
				<soap:body use="literal" />
			</wsdl:output>
			<wsdl:fault name="fault">
				<soap:fault use="literal" name="fault"/>
			</wsdl:fault>
		</wsdl:operation>

		<wsdl:operation name="deleteSubscriber">
			<soap:operation />
			<wsdl:input>
				<soap:body use="literal" />
			</wsdl:input>
			<wsdl:output>
				<soap:body use="literal" />
			</wsdl:output>
			<wsdl:fault name="fault">
				<soap:fault use="literal" name="fault"/>
			</wsdl:fault>
		</wsdl:operation>

        <wsdl:operation name="performEcdTransfer">
            <soap:operation />
            <wsdl:input>
                <soap:body use="literal" />
            </wsdl:input>
            <wsdl:output>
                <soap:body use="literal" />
            </wsdl:output>
            <wsdl:fault name="fault">
                <soap:fault use="literal" name="fault"/>
            </wsdl:fault>
        </wsdl:operation>

        <wsdl:operation name="performSubscriberTransfer">
            <soap:operation />
            <wsdl:input>
                <soap:body use="literal" />
            </wsdl:input>
            <wsdl:output>
                <soap:body use="literal" />
            </wsdl:output>
            <wsdl:fault name="fault">
                <soap:fault use="literal" name="fault"/>
            </wsdl:fault>
        </wsdl:operation>

		<wsdl:operation name="performCashIn">
			<soap:operation />
			<wsdl:input>
				<soap:body use="literal" />
			</wsdl:input>
			<wsdl:output>
				<soap:body use="literal" />
			</wsdl:output>
			<wsdl:fault name="fault">
				<soap:fault use="literal" name="fault"/>
			</wsdl:fault>
		</wsdl:operation>

		<wsdl:operation name="performCashOut">
			<soap:operation  />
			<wsdl:input>
				<soap:body use="literal" />
			</wsdl:input>
			<wsdl:output>
				<soap:body use="literal" />
			</wsdl:output>
			<wsdl:fault name="fault">
				<soap:fault use="literal" name="fault"/>
			</wsdl:fault>
		</wsdl:operation>

		<wsdl:operation name="performTransferGroup">
			<soap:operation  />
			<wsdl:input>
				<soap:body use="literal" />
			</wsdl:input>
			<wsdl:output>
				<soap:body use="literal" />
			</wsdl:output>
			<wsdl:fault name="fault">
				<soap:fault use="literal" name="fault"/>
			</wsdl:fault>
		</wsdl:operation>
	</wsdl:binding>

	<wsdl:service name="ecdV60">
		<wsdl:port name="ecdV60SOAPPort" binding="ecm:ecdV60SOAP">
			<soap:address location="http://www.ecmhp.com/" />


            <wsp:Policy wsu:Id="SigOnly"
                xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"
                xmlns:wsp="http://schemas.xmlsoap.org/ws/2004/09/policy">
                <wsp:ExactlyOne>
                    <wsp:All>
                        <sp:AsymmetricBinding
                            xmlns:sp="http://schemas.xmlsoap.org/ws/2005/07/securitypolicy">
                            <wsp:Policy>
                                <sp:InitiatorToken>
                                    <wsp:Policy>
                                        <sp:X509Token
                                            sp:IncludeToken="http://schemas.xmlsoap.org/ws/2005/07/securitypolicy/IncludeToken/Never">
                                            <wsp:Policy>
                                                <sp:RequireThumbprintReference />
                                                <sp:WssX509V3Token10 />
                                            </wsp:Policy>
                                        </sp:X509Token>
                                    </wsp:Policy>
                                </sp:InitiatorToken>
                                <sp:RecipientToken>
                                    <wsp:Policy>
                                        <sp:X509Token
                                            sp:IncludeToken="http://schemas.xmlsoap.org/ws/2005/07/securitypolicy/IncludeToken/Never">
                                            <wsp:Policy>
                                                <sp:RequireThumbprintReference />
                                                <sp:WssX509V3Token10 />
                                            </wsp:Policy>
                                        </sp:X509Token>
                                    </wsp:Policy>
                                </sp:RecipientToken>
                                <sp:AlgorithmSuite>
                                    <wsp:Policy>
                                        <sp:Basic256Sha256Rsa15 />
                                    </wsp:Policy>
                                </sp:AlgorithmSuite>
                                <sp:Layout>
                                    <wsp:Policy>
                                        <sp:Strict />
                                    </wsp:Policy>
                                </sp:Layout>
                            </wsp:Policy>
                        </sp:AsymmetricBinding>
                        <sp:Wss10 xmlns:sp="http://schemas.xmlsoap.org/ws/2005/07/securitypolicy">
                            <wsp:Policy>
                                <sp:MustSupportRefKeyIdentifier />
                                <sp:MustSupportRefIssuerSerial />
                            </wsp:Policy>
                        </sp:Wss10>

                        <sp:SignedParts
                            xmlns:sp="http://schemas.xmlsoap.org/ws/2005/07/securitypolicy">
                            <sp:Body />
                            <sp:Header Name="Action" Namespace="http://www.w3.org/2005/08/addressing" />
                            <sp:Header Name="To" Namespace="http://www.w3.org/2005/08/addressing" />
                        </sp:SignedParts>
                    </wsp:All>
                </wsp:ExactlyOne>
            </wsp:Policy>

		</wsdl:port>
	</wsdl:service>
</wsdl:definitions>

