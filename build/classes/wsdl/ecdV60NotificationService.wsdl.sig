<?xml version="1.0" encoding="UTF-8"?>
<wsdl:definitions name="ecdV60NotificationService"
	targetNamespace="urn:com:ecmhp" xmlns:ecm="urn:com:ecmhp"
	xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:xsd="http://www.w3.org/2001/XMLSchema"
	xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/">
	<wsdl:types>
		<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema"
			xmlns="http://www.tm-xml.org/XMLSchema/common" attributeFormDefault="unqualified"
			elementFormDefault="qualified" targetNamespace="urn:com:ecmhp"
			xmlns:ecm="urn:com:ecmhp">

			<xsd:include schemaLocation="ecdCommonModelV60.xsd" />

			<xsd:element name="setEcdRequest" type="ecm:setEcdRequest">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">
						=====
						setEcdRequest:: is used by eCMS to notifies ECD to update with the latest ECD data
						=====
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="setEcdResponse" type="ecm:setEcdResponse">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">
						=====
						setEcdResponse:: is used by ECD to confirm the set request from eCMS
						=====
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>

            <xsd:element name="ecdTransferRequest" type="ecm:ecdTransferRequest">
                <xsd:annotation>
                    <xsd:documentation xml:lang="en">
                        =====
						ecdTransferRequest:: is used by eCMS interconnect coordinator (IOX) to request ECD to
                        perform a ecdTransfer using liquidity vault and the addressed ECD vault
                        =====
                    </xsd:documentation>
                </xsd:annotation>
            </xsd:element>
            <xsd:element name="ecdTransferResponse" type="ecm:ecdTransferResponse">
                <xsd:annotation>
                    <xsd:documentation xml:lang="en">
                        =====
						ecdTransferResponse:: is used by ECD to inform IOX the result of the ecdTransfer
                        =====
                    </xsd:documentation>
                </xsd:annotation>
            </xsd:element>

            <xsd:element name="subscriberTransferRequest" type="ecm:subscriberTransferRequest">
                <xsd:annotation>
                    <xsd:documentation xml:lang="en">
                        =====
						subscriberTransferRequest:: is used by eCMS interconnect coordinator (IOX) to request ECD to
                        perform a subscriberTransfer using liquidity vault and the addressed subscriber vault 
                        =====
                    </xsd:documentation>
                </xsd:annotation>
            </xsd:element>
            <xsd:element name="subscriberTransferResponse" type="ecm:subscriberTransferResponse">
                <xsd:annotation>
                    <xsd:documentation xml:lang="en">
                        =====
						subscriberTransferResponse:: is used by ECD to inform IOX the result of the subscriberTransfer 
                        =====
                    </xsd:documentation>
                </xsd:annotation>
            </xsd:element>

			<xsd:element name="cashInRequest" type="ecm:cashInRequest">
                <xsd:annotation>
                    <xsd:documentation xml:lang="en">
                        =====
						cashInRequest:: is used by eCMS interconnect coordinator (IOX) to request ECD to
                        perform a cashIn using liquidity vault and the addressed ECD vault or subscriber vault 
                        =====
                    </xsd:documentation>
                </xsd:annotation>
            </xsd:element>
            <xsd:element name="cashInResponse" type="ecm:cashInResponse">
                <xsd:annotation>
                    <xsd:documentation xml:lang="en">
                        =====
						cashInResponse:: is used by ECD to inform IOX the result of the cashIn
                        =====
                    </xsd:documentation>
                </xsd:annotation>
            </xsd:element>

            <xsd:element name="cashOutRequest" type="ecm:cashOutRequest">
                <xsd:annotation>
                    <xsd:documentation xml:lang="en">
                        =====
						cashOutRequest:: is used by eCMS interconnect coordinator (IOX) to request ECD to
                        perform a cashOut using liquidity vault and the addressed ECD vault or subscriber vault
                        =====
                    </xsd:documentation>
                </xsd:annotation>
            </xsd:element>
            <xsd:element name="cashOutResponse" type="ecm:cashOutResponse">
                <xsd:annotation>
                    <xsd:documentation xml:lang="en">
                        =====
						cashOutResponse:: is used by ECD to inform IOX the result of the cashOut
                        =====
                    </xsd:documentation>
                </xsd:annotation>
            </xsd:element>

            <xsd:element name="faultResponse" type="ecm:faultResponse">
                <xsd:annotation>
                    <xsd:documentation xml:lang="en">
                        =====
                        faultResponse:: is the content of the SOAP fault message returned in case of
                        processing or data validation errors All error conditions return the same
                        faultResponse structure. This allows for streamlined fault processing.
                        =====
                    </xsd:documentation>
                </xsd:annotation>
            </xsd:element>
		</xsd:schema>
	</wsdl:types>

	<wsdl:message name="setEcdRequestMessage">
		<wsdl:part name="parameters" element="ecm:setEcdRequest">
		</wsdl:part>
	</wsdl:message>

	<wsdl:message name="setEcdResponseMessage">
		<wsdl:part name="parameters" element="ecm:setEcdResponse">
		</wsdl:part>
	</wsdl:message>

    <wsdl:message name="ecdTransferRequestMessage">
        <wsdl:part name="parameters" element="ecm:ecdTransferRequest">
        </wsdl:part>
    </wsdl:message>
    <wsdl:message name="ecdTransferResponseMessage">
        <wsdl:part name="parameters" element="ecm:ecdTransferResponse">
        </wsdl:part>
    </wsdl:message>

    <wsdl:message name="subscriberTransferRequestMessage">
        <wsdl:part name="parameters" element="ecm:subscriberTransferRequest">
        </wsdl:part>
    </wsdl:message>
    <wsdl:message name="subscriberTransferResponseMessage">
        <wsdl:part name="parameters" element="ecm:subscriberTransferResponse">
        </wsdl:part>
    </wsdl:message>

    <wsdl:message name="cashInRequestMessage">
        <wsdl:part name="parameters" element="ecm:cashInRequest">
        </wsdl:part>
    </wsdl:message>
    <wsdl:message name="cashInResponseMessage">
        <wsdl:part name="parameters" element="ecm:cashInResponse">
        </wsdl:part>
    </wsdl:message>

    <wsdl:message name="cashOutRequestMessage">
        <wsdl:part name="parameters" element="ecm:cashOutRequest">
        </wsdl:part>
    </wsdl:message>
    <wsdl:message name="cashOutResponseMessage">
        <wsdl:part name="parameters" element="ecm:cashOutResponse">
        </wsdl:part>
    </wsdl:message>

    <wsdl:message name="faultMessage">
        <wsdl:part name="parameters" element="ecm:faultResponse">
        </wsdl:part>
    </wsdl:message>

	<wsdl:portType name="ecdV60NSSOAPPort">
		<wsdl:operation name="setEcd">
			<wsdl:input message="ecm:setEcdRequestMessage"></wsdl:input>
			<wsdl:output message="ecm:setEcdResponseMessage"></wsdl:output>
			<wsdl:fault name="fault" message="ecm:faultMessage"></wsdl:fault>
		</wsdl:operation>
        <wsdl:operation name="performEcdTransfer">
            <wsdl:input message="ecm:ecdTransferRequestMessage"></wsdl:input>
            <wsdl:output message="ecm:ecdTransferResponseMessage"></wsdl:output>
            <wsdl:fault name="fault" message="ecm:faultMessage"></wsdl:fault>
        </wsdl:operation>
        <wsdl:operation name="performSubscriberTransfer">
            <wsdl:input message="ecm:subscriberTransferRequestMessage"></wsdl:input>
            <wsdl:output message="ecm:subscriberTransferResponseMessage"></wsdl:output>
            <wsdl:fault name="fault" message="ecm:faultMessage"></wsdl:fault>
        </wsdl:operation>
        <wsdl:operation name="performCashIn">
            <wsdl:input message="ecm:cashInRequestMessage"></wsdl:input>
            <wsdl:output message="ecm:cashInResponseMessage"></wsdl:output>
            <wsdl:fault name="fault" message="ecm:faultMessage"></wsdl:fault>
        </wsdl:operation>
        <wsdl:operation name="performCashOut">
            <wsdl:input message="ecm:cashOutRequestMessage"></wsdl:input>
            <wsdl:output message="ecm:cashOutResponseMessage"></wsdl:output>
            <wsdl:fault name="fault" message="ecm:faultMessage"></wsdl:fault>
        </wsdl:operation>
	</wsdl:portType>
	
	<wsdl:binding name="ecdV60NSSOAP" type="ecm:ecdV60NSSOAPPort">
		<soap:binding style="document"
			transport="http://schemas.xmlsoap.org/soap/http" />
		<wsdl:operation name="setEcd">
			<soap:operation />
			<wsdl:input>
				<soap:body use="literal" />
			</wsdl:input>
			<wsdl:output>
				<soap:body use="literal" />
			</wsdl:output>
			<wsdl:fault name="fault">
				<soap:fault name="fault" use="literal" />
			</wsdl:fault>
		</wsdl:operation>

        <wsdl:operation name="performEcdTransfer">
            <soap:operation />
            <wsdl:input>
                <soap:body use="literal" />
            </wsdl:input>
            <wsdl:output>
                <soap:body use="literal" />
            </wsdl:output>
            <wsdl:fault name="fault">
                <soap:fault use="literal" name="fault"/>
            </wsdl:fault>
        </wsdl:operation>

        <wsdl:operation name="performSubscriberTransfer">
            <soap:operation />
            <wsdl:input>
                <soap:body use="literal" />
            </wsdl:input>
            <wsdl:output>
                <soap:body use="literal" />
            </wsdl:output>
            <wsdl:fault name="fault">
                <soap:fault use="literal" name="fault"/>
            </wsdl:fault>
        </wsdl:operation>

        <wsdl:operation name="performCashIn">
            <soap:operation />
            <wsdl:input>
                <soap:body use="literal" />
            </wsdl:input>
            <wsdl:output>
                <soap:body use="literal" />
            </wsdl:output>
            <wsdl:fault name="fault">
                <soap:fault use="literal" name="fault"/>
            </wsdl:fault>
        </wsdl:operation>

        <wsdl:operation name="performCashOut">
            <soap:operation  />
            <wsdl:input>
                <soap:body use="literal" />
            </wsdl:input>
            <wsdl:output>
                <soap:body use="literal" />
            </wsdl:output>
            <wsdl:fault name="fault">
                <soap:fault use="literal" name="fault"/>
            </wsdl:fault>
        </wsdl:operation>

	</wsdl:binding>
	
	<wsdl:service name="ecdV60NS">
		<wsdl:port name="ecdV60NSSOAPPort" binding="ecm:ecdV60NSSOAP">
			<soap:address location="http://www.ecmhp.com/" />

            <wsp:Policy wsu:Id="SigOnly"
                xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"
                xmlns:wsp="http://schemas.xmlsoap.org/ws/2004/09/policy">
                <wsp:ExactlyOne>
                    <wsp:All>
                        <sp:AsymmetricBinding
                            xmlns:sp="http://schemas.xmlsoap.org/ws/2005/07/securitypolicy">
                            <wsp:Policy>
                                <sp:InitiatorToken>
                                    <wsp:Policy>
                                        <sp:X509Token
                                            sp:IncludeToken="http://schemas.xmlsoap.org/ws/2005/07/securitypolicy/IncludeToken/Never">
                                            <wsp:Policy>
                                                <sp:RequireThumbprintReference />
                                                <sp:WssX509V3Token10 />
                                            </wsp:Policy>
                                        </sp:X509Token>
                                    </wsp:Policy>
                                </sp:InitiatorToken>
                                <sp:RecipientToken>
                                    <wsp:Policy>
                                        <sp:X509Token
                                            sp:IncludeToken="http://schemas.xmlsoap.org/ws/2005/07/securitypolicy/IncludeToken/Never">
                                            <wsp:Policy>
                                                <sp:RequireThumbprintReference />
                                                <sp:WssX509V3Token10 />
                                            </wsp:Policy>
                                        </sp:X509Token>
                                    </wsp:Policy>
                                </sp:RecipientToken>
                                <sp:AlgorithmSuite>
                                    <wsp:Policy>
                                        <sp:Basic256Sha256Rsa15 />
                                    </wsp:Policy>
                                </sp:AlgorithmSuite>
                                <sp:Layout>
                                    <wsp:Policy>
                                        <sp:Strict />
                                    </wsp:Policy>
                                </sp:Layout>
                            </wsp:Policy>
                        </sp:AsymmetricBinding>
                        <sp:Wss10 xmlns:sp="http://schemas.xmlsoap.org/ws/2005/07/securitypolicy">
                            <wsp:Policy>
                                <sp:MustSupportRefKeyIdentifier />
                                <sp:MustSupportRefIssuerSerial />
                            </wsp:Policy>
                        </sp:Wss10>

                        <sp:SignedParts
                            xmlns:sp="http://schemas.xmlsoap.org/ws/2005/07/securitypolicy">
                            <sp:Body />
                            <sp:Header Name="Action" Namespace="http://www.w3.org/2005/08/addressing" />
                            <sp:Header Name="To" Namespace="http://www.w3.org/2005/08/addressing" />
                        </sp:SignedParts>
                    </wsp:All>
                </wsp:ExactlyOne>
            </wsp:Policy>

		</wsdl:port>
	</wsdl:service>
	<xsd:annotation>
		<xsd:documentation xml:lang="en">
			Copyright (C) 2014 and all subsequent years, eCM (ecmhp.com). All rights reserved.
		</xsd:documentation>
	</xsd:annotation>
</wsdl:definitions>
