package com.acacia.rcbc;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "possibleIpTypes")
@XmlEnum
public enum PossibleIpTypes {
	
	@XmlEnumValue("ipv4")
    IPV_4("ipv4"),
    @XmlEnumValue("ipv6")
    IPV_6("ipv6");
    private final String value;

    PossibleIpTypes(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PossibleIpTypes fromValue(String v) {
        for (PossibleIpTypes c: PossibleIpTypes.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
