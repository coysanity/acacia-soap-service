package com.acacia.rcbc;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "possibleEcdVaultTypes")
@XmlEnum
public enum PossibleEcdVaultTypes {
	
	ECDMST,
    BRSG,
    LIQ;

    public String value() {
        return name();
    }

    public static PossibleEcdVaultTypes fromValue(String v) {
        return valueOf(v);
    }
	

}
