package com.acacia.rcbc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ecdWithCurrencyDescriptor", propOrder = {
    "ecd",
    "currencyVault"
})
public class EcdWithCurrencyDescriptor {
	
	@XmlElement(required = true)
    protected EcdDescriptor ecd;
    protected CurrencyVaultDescriptor currencyVault;
    
	public EcdDescriptor getEcd() {
		return ecd;
	}
	
	public void setEcd(EcdDescriptor ecd) {
		this.ecd = ecd;
	}
	
	public CurrencyVaultDescriptor getCurrencyVault() {
		return currencyVault;
	}
	
	public void setCurrencyVault(CurrencyVaultDescriptor currencyVault) {
		this.currencyVault = currencyVault;
	}
    
    

}
