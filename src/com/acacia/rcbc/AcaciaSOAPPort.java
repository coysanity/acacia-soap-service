package com.acacia.rcbc;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService(targetNamespace = "com.acacia.rcbc", name = "AcaciaSOAPPortImpl")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface AcaciaSOAPPort {
	
	@WebMethod
    @WebResult(name = "getEcdResponse", targetNamespace = "com.acacia.rcbc", partName = "parameters")
    public GetEcdResponse getEcd(
        @WebParam(partName = "parameters", name = "getEcdRequest", targetNamespace = "com.acacia.rcbc")
        GetEcdRequest parameters
    ) throws FaultMessage;
	
	@WebMethod
	@WebResult(name = "addSubscriberResponse", targetNamespace = "com.acacia.rcbc", partName = "parameters")
    public AddSubscriberResponse addSubscriber(
        @WebParam(partName = "parameters", name = "addSubscriberRequest", targetNamespace = "com.acacia.rcbc")
        AddSubscriberRequest parameters
    ) throws FaultMessage;
	
	@WebMethod
    @WebResult(name = "cashInResponse", targetNamespace = "com.acacia.rcbc", partName = "parameters")
    public CashInResponse performCashIn(
        @WebParam(partName = "parameters", name = "cashInRequest", targetNamespace = "com.acacia.rcbc")
        CashInRequest parameters
    ) throws FaultMessage;
	
	@WebMethod
    @WebResult(name = "cashOutResponse", targetNamespace = "com.acacia.rcbc", partName = "parameters")
    public CashOutResponse performCashOut(
        @WebParam(partName = "parameters", name = "cashOutRequest", targetNamespace = "com.acacia.rcbc")
        CashOutRequest parameters
    ) throws FaultMessage;
	
	@WebMethod
    @WebResult(name = "subscriberTransferResponse", targetNamespace = "com.acacia.rcbc", partName = "parameters")
    public SubscriberTransferResponse performSubscriberTransfer(
        @WebParam(partName = "parameters", name = "subscriberTransferRequest", targetNamespace = "com.acacia.rcbc")
        SubscriberTransferRequest parameters
    ) throws FaultMessage;
	
	@WebMethod
    @WebResult(name = "setSubscriberResponse", targetNamespace = "com.acacia.rcbc", partName = "parameters")
    public SetSubscriberResponse setSubscriber(
        @WebParam(partName = "parameters", name = "setSubscriberRequest", targetNamespace = "com.acacia.rcbc")
        SetSubscriberRequest parameters
    ) throws FaultMessage;

    @WebMethod
    @WebResult(name = "getSubscriberResponse", targetNamespace = "com.acacia.rcbc", partName = "parameters")
    public GetSubscriberResponse getSubscriber(
        @WebParam(partName = "parameters", name = "getSubscriberRequest", targetNamespace = "com.acacia.rcbc")
        GetSubscriberRequest parameters
    ) throws FaultMessage;
    
    @WebMethod
    @WebResult(name = "deleteSubscriberResponse", targetNamespace = "com.acacia.rcbc", partName = "parameters")
    public DeleteSubscriberResponse deleteSubscriber(
        @WebParam(partName = "parameters", name = "deleteSubscriberRequest", targetNamespace = "com.acacia.rcbc")
        DeleteSubscriberRequest parameters
    ) throws FaultMessage;
	
	
}
