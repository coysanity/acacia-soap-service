package com.acacia.rcbc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "faultDescriptor", propOrder = {
    "faultCode",
    "groupEntryNo",
    "groupEntryFaultCode",
    "resultMessage"
})
public class FaultDescriptor {
	
	@XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected PossibleFaults faultCode;
    protected Integer groupEntryNo;
    @XmlSchemaType(name = "NMTOKEN")
    protected PossibleFaults groupEntryFaultCode;
    @XmlElement(required = true)
    protected String resultMessage;
    
    public PossibleFaults getFaultCode() {
        return faultCode;
    }
    
    public void setFaultCode(PossibleFaults value) {
        this.faultCode = value;
    }
    
    public Integer getGroupEntryNo() {
        return groupEntryNo;
    }
    
    public void setGroupEntryNo(Integer value) {
        this.groupEntryNo = value;
    }
    
    public PossibleFaults getGroupEntryFaultCode() {
        return groupEntryFaultCode;
    }
    
    public void setGroupEntryFaultCode(PossibleFaults value) {
        this.groupEntryFaultCode = value;
    }
    
    public String getResultMessage() {
        return resultMessage;
    }
    
    public void setResultMessage(String value) {
        this.resultMessage = value;
    }

}
