package com.acacia.rcbc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "subscriberDescriptor", propOrder = {
    "ecd",
    "subscriberID",
    "subscriberType",
    "subscriberState",
    "ecdSubscriberType"
})
public class SubscriberDescriptor {
	
	@XmlElement(required = true)
    protected EcdDescriptor ecd;
    @XmlElement(required = true)
    protected String subscriberID;
    @XmlSchemaType(name = "NMTOKEN")
    protected PossibleSubscriberTypes subscriberType;
    @XmlSchemaType(name = "NMTOKEN")
    protected PossibleSubscriberStates subscriberState;
    protected String ecdSubscriberType;
    
    public EcdDescriptor getEcd() {
        return ecd;
    }
    
    public void setEcd(EcdDescriptor value) {
        this.ecd = value;
    }
    
    public String getSubscriberID() {
        return subscriberID;
    }
    
    public void setSubscriberID(String value) {
        this.subscriberID = value;
    }
    
    public PossibleSubscriberTypes getSubscriberType() {
        return subscriberType;
    }
    
    public void setSubscriberType(PossibleSubscriberTypes value) {
        this.subscriberType = value;
    }
    
    public PossibleSubscriberStates getSubscriberState() {
        return subscriberState;
    }
    
    public void setSubscriberState(PossibleSubscriberStates value) {
        this.subscriberState = value;
    }
    
    public String getEcdSubscriberType() {
        return ecdSubscriberType;
    }
    
    public void setEcdSubscriberType(String value) {
        this.ecdSubscriberType = value;
    }
    
    
	
	
	
	

}
