package com.acacia.rcbc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cashInResponse", propOrder = {
    "ecdHeader",
    "descriptor",
    "result"
})
public class CashInResponse {
	
	@XmlElement(required = true)
    protected String ecdHeader;
    @XmlElement(required = true)
    protected CashInDescriptor descriptor;
    @XmlElement(required = true)
    protected ResultDescriptor result;
    
	public String getEcdHeader() {
		return ecdHeader;
	}
	
	public void setEcdHeader(String ecdHeader) {
		this.ecdHeader = ecdHeader;
	}
	
	public CashInDescriptor getDescriptor() {
		return descriptor;
	}
	
	public void setDescriptor(CashInDescriptor descriptor) {
		this.descriptor = descriptor;
	}
	
	public ResultDescriptor getResult() {
		return result;
	}
	
	public void setResult(ResultDescriptor result) {
		this.result = result;
	}
    
    

}
