package com.acacia.rcbc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "locationDescriptor", propOrder = {
    "locationType",
    "locationGeo",
    "locationIp",
    "locationPostal",
    "locationOther"
})
public class LocationDescriptor {
	
	@XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected PossibleLocationTypes locationType;
    protected GeoLocationType locationGeo;
    protected IpLocationType locationIp;
    protected String locationPostal;
    protected String locationOther;
    
	public PossibleLocationTypes getLocationType() {
		return locationType;
	}
	
	public void setLocationType(PossibleLocationTypes locationType) {
		this.locationType = locationType;
	}
	
	public GeoLocationType getLocationGeo() {
		return locationGeo;
	}
	
	public void setLocationGeo(GeoLocationType locationGeo) {
		this.locationGeo = locationGeo;
	}
	
	public IpLocationType getLocationIp() {
		return locationIp;
	}
	
	public void setLocationIp(IpLocationType locationIp) {
		this.locationIp = locationIp;
	}
	
	public String getLocationPostal() {
		return locationPostal;
	}
	
	public void setLocationPostal(String locationPostal) {
		this.locationPostal = locationPostal;
	}
	
	public String getLocationOther() {
		return locationOther;
	}
	
	public void setLocationOther(String locationOther) {
		this.locationOther = locationOther;
	}
    
    

}
