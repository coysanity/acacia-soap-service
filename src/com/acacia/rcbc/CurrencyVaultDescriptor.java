package com.acacia.rcbc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "currencyVaultDescriptor", propOrder = {
    "amount",
    "vaultType",
    "currencyObject"
})
public class CurrencyVaultDescriptor {
	
	@XmlElement(required = true)
    protected AmountDescriptor amount;
    @XmlSchemaType(name = "NMTOKEN")
    protected PossibleVaultTypes vaultType;
    @XmlElement(required = true)
    protected String currencyObject;
    
	public AmountDescriptor getAmount() {
		return amount;
	}
	public void setAmount(AmountDescriptor amount) {
		this.amount = amount;
	}
	public PossibleVaultTypes getVaultType() {
		return vaultType;
	}
	public void setVaultType(PossibleVaultTypes vaultType) {
		this.vaultType = vaultType;
	}
	public String getCurrencyObject() {
		return currencyObject;
	}
	public void setCurrencyObject(String currencyObject) {
		this.currencyObject = currencyObject;
	}
    
    

}
