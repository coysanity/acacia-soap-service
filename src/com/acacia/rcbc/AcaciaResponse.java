package com.acacia.rcbc;

public class AcaciaResponse {
	
	private String resultMessage;
	private String eCurrencyTransactionId;
	private String eCurrencyTransactionTime;
	
	public AcaciaResponse(String message, String transactionId, String transactionTime) {
		this.setResultMessage(message);
		this.seteCurrencyTransactionId(transactionId);
		this.seteCurrencyTransactionTime(transactionTime);
	}

	public String getResultMessage() {
		return resultMessage;
	}

	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}

	public String geteCurrencyTransactionId() {
		return eCurrencyTransactionId;
	}

	public void seteCurrencyTransactionId(String eCurrencyTransactionId) {
		this.eCurrencyTransactionId = eCurrencyTransactionId;
	}

	public String geteCurrencyTransactionTime() {
		return eCurrencyTransactionTime;
	}

	public void seteCurrencyTransactionTime(String eCurrencyTransactionTime) {
		this.eCurrencyTransactionTime = eCurrencyTransactionTime;
	}

	
}
