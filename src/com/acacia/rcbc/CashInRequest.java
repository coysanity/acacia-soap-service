package com.acacia.rcbc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cashInRequest", propOrder = {
    "ecdHeader",
    "descriptor"
})
public class CashInRequest {
	
	@XmlElement(required = true, defaultValue = "")
    protected String ecdHeader;
    @XmlElement(required = true)
    protected CashInDescriptor descriptor;
    
	public String getEcdHeader() {
		return ecdHeader;
	}
	
	public void setEcdHeader(String ecdHeader) {
		this.ecdHeader = ecdHeader;
	}
	
	public CashInDescriptor getDescriptor() {
		return descriptor;
	}
	
	public void setDescriptor(CashInDescriptor descriptor) {
		this.descriptor = descriptor;
	}
    
    

}
