package com.acacia.rcbc;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


@XmlType(name = "possibleLocationTypes")
@XmlEnum
public enum PossibleLocationTypes {
	
	@XmlEnumValue("geoCoordinates")
    GEO_COORDINATES("geoCoordinates"),
    @XmlEnumValue("postalCode")
    POSTAL_CODE("postalCode"),
    @XmlEnumValue("ipAddress")
    IP_ADDRESS("ipAddress"),
    @XmlEnumValue("other")
    OTHER("other");
    private final String value;

    PossibleLocationTypes(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PossibleLocationTypes fromValue(String v) {
        for (PossibleLocationTypes c: PossibleLocationTypes.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
    
}
