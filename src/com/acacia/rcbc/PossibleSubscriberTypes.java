package com.acacia.rcbc;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "possibleSubscriberTypes")
@XmlEnum
public enum PossibleSubscriberTypes {
	
	SUB,
    MER,
    AGN,
    ESC,
    S,
    M,
    A,
    E;
	
	public String value() {
        return name();
    }

    public static PossibleSubscriberTypes fromValue(String v) {
        return valueOf(v);
    }
	

}
