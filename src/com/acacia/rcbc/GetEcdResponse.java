package com.acacia.rcbc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getEcdResponse", propOrder = {
    "ecdHeader",
    "ecdWithCurrency",
    "result"
})
public class GetEcdResponse {
	
	@XmlElement(required = true)
    protected String ecdHeader;
    @XmlElement(required = true)
    protected EcdWithCurrencyDescriptor ecdWithCurrency;
    @XmlElement(required = true)
    protected ResultDescriptor result;
    
	public String getEcdHeader() {
		return ecdHeader;
	}
	
	public void setEcdHeader(String ecdHeader) {
		this.ecdHeader = ecdHeader;
	}
	
	public EcdWithCurrencyDescriptor getEcdWithCurrency() {
		return ecdWithCurrency;
	}
	
	public void setEcdWithCurrency(EcdWithCurrencyDescriptor ecdWithCurrency) {
		this.ecdWithCurrency = ecdWithCurrency;
	}
	
	public ResultDescriptor getResult() {
		return result;
	}
	
	public void setResult(ResultDescriptor result) {
		this.result = result;
	}
    
}
