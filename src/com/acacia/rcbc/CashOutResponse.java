package com.acacia.rcbc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cashOutResponse", propOrder = {
    "ecdHeader",
    "descriptor",
    "result"
})
public class CashOutResponse {
	
	@XmlElement(required = true)
    protected String ecdHeader;
    @XmlElement(required = true)
    protected CashOutDescriptor descriptor;
    @XmlElement(required = true)
    protected ResultDescriptor result;
    
	public String getEcdHeader() {
		return ecdHeader;
	}
	
	public void setEcdHeader(String ecdHeader) {
		this.ecdHeader = ecdHeader;
	}
	
	public CashOutDescriptor getDescriptor() {
		return descriptor;
	}
	
	public void setDescriptor(CashOutDescriptor descriptor) {
		this.descriptor = descriptor;
	}
	
	public ResultDescriptor getResult() {
		return result;
	}
	
	public void setResult(ResultDescriptor result) {
		this.result = result;
	}
    
}
