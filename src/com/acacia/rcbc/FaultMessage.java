package com.acacia.rcbc;

import javax.xml.ws.WebFault;

@WebFault(name = "faultResponse", targetNamespace = "com.acacia.rcbc")
public class FaultMessage extends Exception {
	
	private FaultResponse faultResponse;

    public FaultMessage() {
        super();
    }
    
    public FaultMessage(String message) {
        super(message);
    }
    
    public FaultMessage(String message, Throwable cause) {
        super(message, cause);
    }

    public FaultMessage(String message, FaultResponse faultResponse) {
        super(message);
        this.faultResponse = faultResponse;
    }

    public FaultMessage(String message, FaultResponse faultResponse, Throwable cause) {
        super(message, cause);
        this.faultResponse = faultResponse;
    }

    public FaultResponse getFaultInfo() {
        return this.faultResponse;
    }
}
