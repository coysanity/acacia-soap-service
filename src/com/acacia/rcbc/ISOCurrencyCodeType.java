package com.acacia.rcbc;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "ISOCurrencyCodeType")
@XmlEnum
public enum ISOCurrencyCodeType {
	
    /**
     * Dirham
     * 
     */
    AED,

    /**
     * Afghani
     * 
     */
    AFN,

    /**
     * Lek
     * 
     */
    ALL,

    /**
     * Dram
     * 
     */
    AMD,

    /**
     * Netherlands Antillian Guilder
     * 
     */
    ANG,

    /**
     * Kwanza
     * 
     */
    AOA,

    /**
     * Argentine Peso
     * 
     */
    ARS,

    /**
     * Australian Dollar
     * 
     */
    AUD,

    /**
     * Aruban Guilder
     * 
     */
    AWG,

    /**
     * Azerbaijanian Manat
     * 
     */
    AZN,

    /**
     * Convertible Mark
     * 
     */
    BAM,

    /**
     * Barbados Dollar
     * 
     */
    BBD,

    /**
     * Taka
     * 
     */
    BDT,

    /**
     * Bulgarian Lev
     * 
     */
    BGN,

    /**
     * Bahraini Dinar
     * 
     */
    BHD,

    /**
     * Burundi Franc
     * 
     */
    BIF,

    /**
     * Bermudian Dollar (customarily: Bermuda Dollar)
     * 
     */
    BMD,

    /**
     * Brunei Dollar
     * 
     */
    BND,

    /**
     * Boliviano
     * 
     */
    BOB,

    /**
     * Brazilian Real
     * 
     */
    BRL,

    /**
     * Bahamian Dollar
     * 
     */
    BSD,

    /**
     * Ngultrum
     * 
     */
    BTN,

    /**
     * Pula
     * 
     */
    BWP,

    /**
     * Belarussian Ruble
     * 
     */
    BYR,

    /**
     * Belize Dollar
     * 
     */
    BZD,

    /**
     * Canadian Dollar
     * 
     */
    CAD,

    /**
     * Franc Congolais
     * 
     */
    CDF,

    /**
     * Swiss Franc
     * 
     */
    CHF,

    /**
     * Chilean Peso
     * 
     */
    CLP,

    /**
     * Yuan Renminbi
     * 
     */
    CNY,

    /**
     * Colombian Peso
     * 
     */
    COP,

    /**
     * Costa Rican Colon
     * 
     */
    CRC,

    /**
     * Cuban Peso
     * 
     */
    CUP,

    /**
     * Cape Verde Escudo
     * 
     */
    CVE,

    /**
     * Czech Koruna
     * 
     */
    CZK,

    /**
     * Djibouti Franc
     * 
     */
    DJF,

    /**
     * Danish Krone
     * 
     */
    DKK,

    /**
     * Dominican Peso
     * 
     */
    DOP,

    /**
     * Algerian Dinar
     * 
     */
    DZD,

    /**
     * Kroon
     * 
     */
    EEK,

    /**
     * Egyptian Pound
     * 
     */
    EGP,

    /**
     * Nakfa
     * 
     */
    ERN,

    /**
     * Ethopian Birr
     * 
     */
    ETB,

    /**
     * Euro
     * 
     */
    EUR,

    /**
     * Fiji Dollar
     * 
     */
    FJD,

    /**
     * Falkland Islands Pound
     * 
     */
    FKP,

    /**
     * Pound Sterling
     * 
     */
    GBP,

    /**
     * Lari
     * 
     */
    GEL,

    /**
     * Cedi
     * 
     */
    GHS,

    /**
     * Gibraltar Pound
     * 
     */
    GIP,

    /**
     * Dalasi
     * 
     */
    GMD,

    /**
     * Guinea Franc
     * 
     */
    GNF,

    /**
     * Quetzal
     * 
     */
    GTQ,

    /**
     * Guyana Dollar
     * 
     */
    GYD,

    /**
     * Guinea-Bisau Peso
     * 
     */
    GWP,

    /**
     * Honk Kong Dollar
     * 
     */
    HKD,

    /**
     * Lempira
     * 
     */
    HNL,

    /**
     * Kuna
     * 
     */
    HRK,

    /**
     * Gourde
     * 
     */
    HTG,

    /**
     * Forint
     * 
     */
    HUF,

    /**
     * Rupiah
     * 
     */
    IDR,

    /**
     * New Israeli Sheqel
     * 
     */
    ILS,

    /**
     * Indian Rupee
     * 
     */
    INR,

    /**
     * Iraqi Dinar
     * 
     */
    IQD,

    /**
     * Iranian Rial
     * 
     */
    IRR,

    /**
     * Iceland Krona
     * 
     */
    ISK,

    /**
     * Jamaican Dollar
     * 
     */
    JMD,

    /**
     * Jordanian Dinar
     * 
     */
    JOD,

    /**
     * Yen
     * 
     */
    JPY,

    /**
     * Kenyan Shilling
     * 
     */
    KES,

    /**
     * Som
     * 
     */
    KGS,

    /**
     * Riel
     * 
     */
    KHR,

    /**
     * Comoro Franc
     * 
     */
    KMF,

    /**
     * North Korean Won
     * 
     */
    KPW,

    /**
     * Won
     * 
     */
    KRW,

    /**
     * Kuwaiti Dinar
     * 
     */
    KWD,

    /**
     * Cayman Islands Dollar
     * 
     */
    KYD,

    /**
     * Tenge
     * 
     */
    KZT,

    /**
     * Kip
     * 
     */
    LAK,

    /**
     * Lebanese Pound
     * 
     */
    LBP,

    /**
     * Sri Lanka Rupee
     * 
     */
    LKR,

    /**
     * Liberian Dollar
     * 
     */
    LRD,

    /**
     * Loti
     * 
     */
    LSL,

    /**
     * Lithuanian Litas
     * 
     */
    LTL,

    /**
     * Latvian Lats
     * 
     */
    LVL,

    /**
     * Libyan Dinar
     * 
     */
    LYD,

    /**
     * Morrocan Dirham
     * 
     */
    MAD,

    /**
     * Moldovan Leu
     * 
     */
    MDL,

    /**
     * Malagasy Ariary
     * 
     */
    MGA,

    /**
     * Denar
     * 
     */
    MKD,

    /**
     * Kyat
     * 
     */
    MMK,

    /**
     * Tugrik
     * 
     */
    MNT,

    /**
     * Pataca
     * 
     */
    MOP,

    /**
     * Ouguiya
     * 
     */
    MRO,

    /**
     * Mauritius Rupee
     * 
     */
    MUR,

    /**
     * Rufiyaa
     * 
     */
    MVR,

    /**
     * Kwacha
     * 
     */
    MWK,

    /**
     * Mexican Peso
     * 
     */
    MXN,

    /**
     * Malaysian Ringgit
     * 
     */
    MYR,

    /**
     * Metical
     * 
     */
    MZN,

    /**
     * Namibia Dollar
     * 
     */
    NAD,

    /**
     * Naira
     * 
     */
    NGN,

    /**
     * Cordoba Oro
     * 
     */
    NIO,

    /**
     * Norwegian Krone
     * 
     */
    NOK,

    /**
     * Nepalese Rupee
     * 
     */
    NPR,

    /**
     * New Zealand Dollar
     * 
     */
    NZD,

    /**
     * Rial Omani
     * 
     */
    OMR,

    /**
     * Balboa
     * 
     */
    PAB,

    /**
     * Nuevo Sol
     * 
     */
    PEN,

    /**
     * Kina
     * 
     */
    PGK,

    /**
     * Philippine Peso
     * 
     */
    PHP,

    /**
     * Pakistan Rupee
     * 
     */
    PKR,

    /**
     * Zloty
     * 
     */
    PLN,

    /**
     * Guarani
     * 
     */
    PYG,

    /**
     * Qatari Rial
     * 
     */
    QAR,

    /**
     * New Leu
     * 
     */
    RON,

    /**
     * Serbian Dinar
     * 
     */
    RSD,

    /**
     * Russian Ruble
     * 
     */
    RUB,

    /**
     * Rwanda Franc
     * 
     */
    RWF,

    /**
     * Saudi Riyal
     * 
     */
    SAR,

    /**
     * Solomon Islands Dollar
     * 
     */
    SBD,

    /**
     * Seychelles Rupee
     * 
     */
    SCR,

    /**
     * Sudanese Pound
     * 
     */
    SDG,

    /**
     * Swedish Krona
     * 
     */
    SEK,

    /**
     * Singapore Dollar
     * 
     */
    SGD,

    /**
     * St. Helena Pound
     * 
     */
    SHP,

    /**
     * Slovak Koruna
     * 
     */
    SKK,

    /**
     * Leone
     * 
     */
    SLL,

    /**
     * Somali Shilling
     * 
     */
    SOS,

    /**
     * Suriname Dollar
     * 
     */
    SRD,

    /**
     * Dobra
     * 
     */
    STD,

    /**
     * El Salvador Colon
     * 
     */
    SVC,

    /**
     * Syrian Pound
     * 
     */
    SYP,

    /**
     * Lilangeni
     * 
     */
    SZL,

    /**
     * Baht
     * 
     */
    THB,

    /**
     * Somoni
     * 
     */
    TJS,

    /**
     * Manat
     * 
     */
    TMM,

    /**
     * Tunisian Dinar
     * 
     */
    TND,

    /**
     * Pa'anga
     * 
     */
    TOP,

    /**
     * New Turkish Lira
     * 
     */
    TRY,

    /**
     * Trinidad and Tobago Dollar
     * 
     */
    TTD,

    /**
     * New Taiwan Dollar
     * 
     */
    TWD,

    /**
     * Tanzanian Shilling
     * 
     */
    TZS,

    /**
     * Hryvnia
     * 
     */
    UAH,

    /**
     * Uganda Shilling
     * 
     */
    UGX,

    /**
     * US Dollar
     * 
     */
    USD,

    /**
     * Peso Uruguayo
     * 
     */
    UYU,

    /**
     * Uzbekistan Sum
     * 
     */
    UZS,

    /**
     * Bolivar Fuerte
     * 
     */
    VEF,

    /**
     * Dong
     * 
     */
    VND,

    /**
     * Vatu
     * 
     */
    VUV,

    /**
     * Tala
     * 
     */
    WST,

    /**
     * CFA Franc
     * 
     */
    XAF,

    /**
     * Silver
     * 
     */
    XAG,

    /**
     * Gold
     * 
     */
    XAU,

    /**
     * East Carribean Dollar
     * 
     */
    XCD,

    /**
     * SDR
     * 
     */
    XDR,

    /**
     * CFA Franc
     * 
     */
    XOF,

    /**
     * Palladium
     * 
     */
    XPD,

    /**
     * CFP Franc
     * 
     */
    XPF,

    /**
     * Platinum
     * 
     */
    XPT,

    /**
     * Yemeni Rial
     * 
     */
    YER,

    /**
     * Rand
     * 
     */
    ZAR,

    /**
     * Kwacha
     * 
     */
    ZMK,

    /**
     * Zimbabwe Dollar
     * 
     */
    ZWR;

    public String value() {
        return name();
    }

    public static ISOCurrencyCodeType fromValue(String v) {
        return valueOf(v);
    }

}
