package com.acacia.rcbc;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


@XmlType(name = "possibleFaults")
@XmlEnum
public enum PossibleFaults {

	@XmlEnumValue("SendNotExist")
    SEND_NOT_EXIST("SendNotExist"),
    @XmlEnumValue("RcvNotExist")
    RCV_NOT_EXIST("RcvNotExist"),
    @XmlEnumValue("SendInvCrypt")
    SEND_INV_CRYPT("SendInvCrypt"),
    @XmlEnumValue("RcvInvCrypt")
    RCV_INV_CRYPT("RcvInvCrypt"),
    @XmlEnumValue("SendInvData")
    SEND_INV_DATA("SendInvData"),
    @XmlEnumValue("RcvInvData")
    RCV_INV_DATA("RcvInvData"),
    @XmlEnumValue("TransInvData")
    TRANS_INV_DATA("TransInvData"),
    @XmlEnumValue("SendInsuffFunds")
    SEND_INSUFF_FUNDS("SendInsuffFunds"),
    @XmlEnumValue("SendInactive")
    SEND_INACTIVE("SendInactive"),
    @XmlEnumValue("RcvInactive")
    RCV_INACTIVE("RcvInactive"),
    @XmlEnumValue("InconsistentCurrencyCode")
    INCONSISTENT_CURRENCY_CODE("InconsistentCurrencyCode"),
    @XmlEnumValue("InvalidSerialNumber")
    INVALID_SERIAL_NUMBER("InvalidSerialNumber"),
    @XmlEnumValue("SubNotExist")
    SUB_NOT_EXIST("SubNotExist"),
    @XmlEnumValue("SubAlreadyExists")
    SUB_ALREADY_EXISTS("SubAlreadyExists"),
    @XmlEnumValue("SubNotZero")
    SUB_NOT_ZERO("SubNotZero"),
    @XmlEnumValue("EcdNotExist")
    ECD_NOT_EXIST("EcdNotExist"),
    @XmlEnumValue("InteropError")
    INTEROP_ERROR("InteropError"),
    @XmlEnumValue("TransferGroupError")
    TRANSFER_GROUP_ERROR("TransferGroupError"),
    @XmlEnumValue("Unavail")
    UNAVAIL("Unavail"),
    @XmlEnumValue("Suspended")
    SUSPENDED("Suspended"),
    @XmlEnumValue("System")
    SYSTEM("System"),
    @XmlEnumValue("Other")
    OTHER("Other");
    private final String value;

    PossibleFaults(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PossibleFaults fromValue(String v) {
        for (PossibleFaults c: PossibleFaults.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
    
}
