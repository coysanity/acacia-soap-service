package com.acacia.rcbc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import java.util.Scanner;

import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPFault;

import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;


@WebService(
		endpointInterface = "com.acacia.rcbc.AcaciaSOAPPort",
		portName = "AcaciaSOAPPort",
		serviceName = "AcaciaService",
		targetNamespace = "com.acacia.rcbc"
		)
public class AcaciaSOAPPortImpl implements AcaciaSOAPPort {
		
    private static String TARGET_ADDR;
    private static String TARGET_PORT;
    private static String ECM_COUNTRY;
    private static String ECM_COUNTRY_UPPERCASE;
    private static String ECM_EMINAME;
    private static String ECD_VERSION;
    private static String CURRENCY_CODE;

    private static QName SERVICE_NAME;
    
    private static String TARGET_URL;

    private static String WSDL_LOCATION;

    private static String ECD_ID;
    private static com.ecm.test.soap.ISOCountryCodeType COUNTRY_CODE;
    private static com.ecm.test.soap.ISOCurrencyCodeType CURR_CODE;
    
    private static String SUB1 = "subscr-1";
    private static String SUB2 = "subscr-2";
    private static String AMT_0 = "0";
    private static String AMT_100 = "100";
    
    private static String cryptoSub1 = "";
    private static String cryptoSub2 = "";
    
    public GetEcdResponse getEcd(GetEcdRequest parameters) throws FaultMessage   { 
        try {
        	
			System.setProperty("javax.net.ssl.keyStore", this.getClass().getClassLoader().getResource("keys/ws_client.jks").toURI().getPath());
			System.setProperty("javax.net.ssl.keyStorePassword", "changeit");
			System.setProperty("javax.net.ssl.trustStore", this.getClass().getClassLoader().getResource("keys/cbgui.jts").toURI().getPath());
			System.setProperty("javax.net.ssl.trustStorePassword", "changeit");
			
			Properties prop = new Properties();
			InputStream input = this.getClass().getClassLoader().getResourceAsStream("config/config.properties");
			prop.load(input);
			
			TARGET_ADDR = prop.getProperty("TARGET_ADDR");
			TARGET_PORT = prop.getProperty("TARGET_PORT");
			ECM_COUNTRY = prop.getProperty("ECM_COUNTRY");
			ECM_COUNTRY_UPPERCASE = prop.getProperty("ECM_COUNTRY_UPPERCASE");
			ECM_EMINAME = prop.getProperty("ECM_EMINAME");
			ECD_VERSION = prop.getProperty("ECD_VERSION");
			CURRENCY_CODE = prop.getProperty("CURRENCY_CODE");
			WSDL_LOCATION = prop.getProperty("WSDL_LOCATION"); 
			
			SERVICE_NAME = new QName("urn:com:ecmhp", "ecdV" + ECD_VERSION);
			if (TARGET_PORT.equals("80") || TARGET_PORT.equals("8080")) {
				TARGET_URL = "http://";
			} else if (TARGET_PORT.equals("443") || TARGET_PORT.equals("8443")) {
				TARGET_URL = "https://";
			} else {
				System.out.println("Only 80/443 or 8080/8443 ports are supported. Exit...");
				System.exit(0);
			}
			TARGET_URL += TARGET_ADDR + ":" + TARGET_PORT + "/emiIntegrator/services/ecdV" + ECD_VERSION + "/";
			
			ECD_ID = ECM_COUNTRY + "_" + ECM_EMINAME;
			COUNTRY_CODE = com.ecm.test.soap.ISOCountryCodeType.valueOf(ECM_COUNTRY_UPPERCASE);
			CURR_CODE = com.ecm.test.soap.ISOCurrencyCodeType.valueOf(CURRENCY_CODE);            
			
			URL wsdlURL = this.getClass().getClassLoader().getResource(WSDL_LOCATION);
			
			com.ecm.test.soap.EcdV60 ss = new com.ecm.test.soap.EcdV60(wsdlURL, SERVICE_NAME);
			com.ecm.test.soap.EcdV60SOAPPort port = ss.getEcdV60SOAPPort();  
			
			String signProp = this.getClass().getClassLoader().getResource("keys/wss_client.properties").toURI().getPath(); 
			java.util.Map<String, Object> requestContext = ((javax.xml.ws.BindingProvider)port).getRequestContext();
			requestContext.put(org.apache.cxf.message.Message.ENDPOINT_ADDRESS, TARGET_URL);
			requestContext.put("security.signature.properties", signProp);
			requestContext.put("security.callback-handler", "com.ecm.test.soap.PWCBHandler");
			
			String requestEcdHeader = parameters.ecdHeader;
			String requestCbID = parameters.ecd.cbID;
		    String requestCommercialBankID = parameters.ecd.commercialBankID;
		    com.ecm.test.soap.ISOCountryCodeType requestCountryCode = com.ecm.test.soap.ISOCountryCodeType.valueOf(parameters.ecd.countryID.name());		    
		    String requestEcdID = parameters.ecd.ecdID;
		    String requestPartitionID = parameters.ecd.partitionID;
		    com.ecm.test.soap.PossibleEcdVaultTypes requestEcdVaultType = com.ecm.test.soap.PossibleEcdVaultTypes.valueOf(parameters.vaultType.name());
		    
	        com.ecm.test.soap.GetEcdRequest _getEcd_parameters = new com.ecm.test.soap.GetEcdRequest();
	        _getEcd_parameters.setEcdHeader(requestEcdHeader);
	        com.ecm.test.soap.EcdDescriptor _getEcd_parametersEcd = new com.ecm.test.soap.EcdDescriptor();
	        _getEcd_parametersEcd.setCountryID(requestCountryCode);
	        _getEcd_parametersEcd.setEcdID(requestEcdID);
	        _getEcd_parameters.setEcd(_getEcd_parametersEcd);
	        _getEcd_parameters.setVaultType(requestEcdVaultType);
	        
            com.ecm.test.soap.GetEcdResponse _getEcd__return = port.getEcd(_getEcd_parameters);
            
            GetEcdResponse _return = new GetEcdResponse();
            _return.setEcdHeader(_getEcd__return.getEcdHeader());
            EcdWithCurrencyDescriptor _returnEcdWithCurrency = new EcdWithCurrencyDescriptor();
            EcdDescriptor _returnEcdWithCurrencyEcd = new EcdDescriptor();
            ISOCountryCodeType _returnEcdWithCurrencyEcdCountryID = ISOCountryCodeType.valueOf(_getEcd__return.getEcdWithCurrency().getEcd().getCountryID().name());
            _returnEcdWithCurrencyEcd.setCountryID(_returnEcdWithCurrencyEcdCountryID);
            _returnEcdWithCurrencyEcd.setEcdID(_getEcd__return.getEcdWithCurrency().getEcd().getEcdID());
            _returnEcdWithCurrency.setEcd(_returnEcdWithCurrencyEcd);
            
            CurrencyVaultDescriptor _returnEcdWithCurrencyCurrencyVault = new CurrencyVaultDescriptor();
            AmountDescriptor _returnEcdWithCurrencyCurrencyVaultAmount = new AmountDescriptor();
            ISOCurrencyCodeType _returnEcdWithCurrencyCurrencyVaultAmountCurrencyCode = ISOCurrencyCodeType.valueOf(_getEcd__return.getEcdWithCurrency().getCurrencyVault().getAmount().getCurrencyCode().name());
            _returnEcdWithCurrencyCurrencyVaultAmount.setCurrencyCode(_returnEcdWithCurrencyCurrencyVaultAmountCurrencyCode);
            _returnEcdWithCurrencyCurrencyVaultAmount.setCurrencyAmount(_getEcd__return.getEcdWithCurrency().getCurrencyVault().getAmount().getCurrencyAmount());
            _returnEcdWithCurrencyCurrencyVault.setAmount(_returnEcdWithCurrencyCurrencyVaultAmount);
            //PossibleVaultTypes _returnEcdWithCurrencyCurrencyVaultVaultType = PossibleVaultTypes.valueOf(_getEcd__return.getEcdWithCurrency().getCurrencyVault().getVaultType().name());
            //_returnEcdWithCurrencyCurrencyVault.setVaultType(_returnEcdWithCurrencyCurrencyVaultVaultType);
            _returnEcdWithCurrencyCurrencyVault.setCurrencyObject(_getEcd__return.getEcdWithCurrency().getCurrencyVault().getCurrencyObject());
            _returnEcdWithCurrency.setCurrencyVault(_returnEcdWithCurrencyCurrencyVault);
            
            _return.setEcdWithCurrency(_returnEcdWithCurrency);
            ResultDescriptor _returnResult = new ResultDescriptor();
            _returnResult.setResultMessage(_getEcd__return.getResult().getResultMessage());
            _returnResult.seteCurrencyTransactionId(_getEcd__return.getResult().getECurrencyTransactionId());
            _returnResult.seteCurrencyTransactionTime(_getEcd__return.getResult().getECurrencyTransactionTime());
            _return.setResult(_returnResult);
            
            return _return;
        } catch (java.lang.Exception ex) {
        	//ex.printStackTrace();
            throw new RuntimeException(ex);
        }
    }
    			
	public AddSubscriberResponse addSubscriber(AddSubscriberRequest parameters) throws FaultMessage {
		
		try {        	
			System.setProperty("javax.net.ssl.keyStore", this.getClass().getClassLoader().getResource("keys/ws_client.jks").toURI().getPath());
			System.setProperty("javax.net.ssl.keyStorePassword", "changeit");
			System.setProperty("javax.net.ssl.trustStore", this.getClass().getClassLoader().getResource("keys/cbgui.jts").toURI().getPath());
			System.setProperty("javax.net.ssl.trustStorePassword", "changeit");        	
			
			Properties prop = new Properties();
			InputStream input = this.getClass().getClassLoader().getResourceAsStream("config/config.properties");
			prop.load(input);
			
			TARGET_ADDR = prop.getProperty("TARGET_ADDR");
			TARGET_PORT = prop.getProperty("TARGET_PORT");
			ECM_COUNTRY = prop.getProperty("ECM_COUNTRY");
			ECM_COUNTRY_UPPERCASE = prop.getProperty("ECM_COUNTRY_UPPERCASE");
			ECM_EMINAME = prop.getProperty("ECM_EMINAME");
			ECD_VERSION = prop.getProperty("ECD_VERSION");
			CURRENCY_CODE = prop.getProperty("CURRENCY_CODE");
			WSDL_LOCATION = prop.getProperty("WSDL_LOCATION"); 
			
			SERVICE_NAME = new QName("urn:com:ecmhp", "ecdV" + ECD_VERSION);
			if (TARGET_PORT.equals("80") || TARGET_PORT.equals("8080")) {
				TARGET_URL = "http://";
			} else if (TARGET_PORT.equals("443") || TARGET_PORT.equals("8443")) {
				TARGET_URL = "https://";
			} else {
				System.out.println("Only 80/443 or 8080/8443 ports are supported. Exit...");
				System.exit(0);
			}
			TARGET_URL += TARGET_ADDR + ":" + TARGET_PORT + "/emiIntegrator/services/ecdV" + ECD_VERSION + "/";
			
			ECD_ID = ECM_COUNTRY + "_" + ECM_EMINAME;
			COUNTRY_CODE = com.ecm.test.soap.ISOCountryCodeType.valueOf(ECM_COUNTRY_UPPERCASE);
			CURR_CODE = com.ecm.test.soap.ISOCurrencyCodeType.valueOf(CURRENCY_CODE);            
			
			URL wsdlURL = this.getClass().getClassLoader().getResource(WSDL_LOCATION);
			
			com.ecm.test.soap.EcdV60 ss = new com.ecm.test.soap.EcdV60(wsdlURL, SERVICE_NAME);
			com.ecm.test.soap.EcdV60SOAPPort port = ss.getEcdV60SOAPPort();  
			
			String signProp = this.getClass().getClassLoader().getResource("keys/wss_client.properties").toURI().getPath(); 
			java.util.Map<String, Object> requestContext = ((javax.xml.ws.BindingProvider)port).getRequestContext();
			requestContext.put(org.apache.cxf.message.Message.ENDPOINT_ADDRESS, TARGET_URL);
			requestContext.put("security.signature.properties", signProp);
			requestContext.put("security.callback-handler", "com.ecm.test.soap.PWCBHandler");			
				
			com.ecm.test.soap.AddSubscriberRequest _addSubscriber_parameters = new com.ecm.test.soap.AddSubscriberRequest();
		    _addSubscriber_parameters.setEcdHeader(parameters.ecdHeader);
		    com.ecm.test.soap.SubscriberDescriptor _addSubscriber_parametersSubscriber = new com.ecm.test.soap.SubscriberDescriptor();
		    com.ecm.test.soap.EcdDescriptor _addSubscriber_parametersSubscriberEcd = new com.ecm.test.soap.EcdDescriptor();
		    _addSubscriber_parametersSubscriberEcd.setCbID("CbID761129280");
		    _addSubscriber_parametersSubscriberEcd.setCommercialBankID("CommercialBankID504761796");
		    com.ecm.test.soap.ISOCountryCodeType _addSubscriber_parametersSubscriberEcdCountryID = com.ecm.test.soap.ISOCountryCodeType.valueOf(parameters.subscriber.ecd.countryID.name());
		    _addSubscriber_parametersSubscriberEcd.setCountryID(_addSubscriber_parametersSubscriberEcdCountryID);
		    _addSubscriber_parametersSubscriberEcd.setEcdID(parameters.subscriber.ecd.ecdID);
		    _addSubscriber_parametersSubscriberEcd.setPartitionID("PartitionID-458293");
		    _addSubscriber_parametersSubscriber.setEcd(_addSubscriber_parametersSubscriberEcd);
		    _addSubscriber_parametersSubscriber.setSubscriberID(parameters.subscriber.subscriberID);
		    com.ecm.test.soap.PossibleSubscriberTypes _addSubscriber_parametersSubscriberSubscriberType = com.ecm.test.soap.PossibleSubscriberTypes.valueOf(parameters.subscriber.subscriberType.name());
		    _addSubscriber_parametersSubscriber.setSubscriberType(_addSubscriber_parametersSubscriberSubscriberType);
		    com.ecm.test.soap.PossibleSubscriberStates _addSubscriber_parametersSubscriberSubscriberState = com.ecm.test.soap.PossibleSubscriberStates.valueOf(parameters.subscriber.subscriberState.name());
		    _addSubscriber_parametersSubscriber.setSubscriberState(_addSubscriber_parametersSubscriberSubscriberState);
		    _addSubscriber_parameters.setSubscriber(_addSubscriber_parametersSubscriber);
		    
		    com.ecm.test.soap.AddSubscriberResponse _addSubscriber__return = port.addSubscriber(_addSubscriber_parameters);
		    			
			AddSubscriberResponse _return = new AddSubscriberResponse();
            _return.setEcdHeader(_addSubscriber__return.getEcdHeader());
            SubscriberWithCurrencyDescriptor _returnSubscriberWithCurrency = new SubscriberWithCurrencyDescriptor();
            SubscriberDescriptor _returnSubscriberWithCurrencySubscriber = new SubscriberDescriptor();
            EcdDescriptor _returnSubscriberWithCurrencySubscriberEcd = new EcdDescriptor();
            ISOCountryCodeType _responseISOCountryCode = ISOCountryCodeType.valueOf(_addSubscriber__return.getSubscriberWithCurrency().getSubscriber().getEcd().getCountryID().name());
            _returnSubscriberWithCurrencySubscriberEcd.setCountryID(_responseISOCountryCode);
            _returnSubscriberWithCurrencySubscriberEcd.setEcdID(_addSubscriber__return.getSubscriberWithCurrency().getSubscriber().getEcd().getEcdID());
            _returnSubscriberWithCurrencySubscriber.setEcd(_returnSubscriberWithCurrencySubscriberEcd);            
            _returnSubscriberWithCurrencySubscriber.setSubscriberID(_addSubscriber__return.getSubscriberWithCurrency().getSubscriber().getSubscriberID());
            _returnSubscriberWithCurrencySubscriber.setSubscriberType(PossibleSubscriberTypes.valueOf(_addSubscriber__return.getSubscriberWithCurrency().getSubscriber().getSubscriberType().name()));
            _returnSubscriberWithCurrencySubscriber.setSubscriberState(PossibleSubscriberStates.valueOf(_addSubscriber__return.getSubscriberWithCurrency().getSubscriber().getSubscriberState().name()));
            _returnSubscriberWithCurrency.setSubscriber(_returnSubscriberWithCurrencySubscriber);            
            CurrencyVaultDescriptor _returnSubscriberWithCurrencyCurrencyVault = new CurrencyVaultDescriptor();
            AmountDescriptor _returnSubscriberWithCurrencyCurrencyVaultAmount = new AmountDescriptor();
            ISOCurrencyCodeType _returnSubscriberWithCurrencyCurrencyVaultAmountCurrencyCode = ISOCurrencyCodeType.valueOf(_addSubscriber__return.getSubscriberWithCurrency().getCurrencyVault().getAmount().getCurrencyCode().name());
            _returnSubscriberWithCurrencyCurrencyVaultAmount.setCurrencyCode(_returnSubscriberWithCurrencyCurrencyVaultAmountCurrencyCode);
            _returnSubscriberWithCurrencyCurrencyVaultAmount.setCurrencyAmount(_addSubscriber__return.getSubscriberWithCurrency().getCurrencyVault().getAmount().getCurrencyAmount());
            _returnSubscriberWithCurrencyCurrencyVault.setAmount(_returnSubscriberWithCurrencyCurrencyVaultAmount);
            _returnSubscriberWithCurrencyCurrencyVault.setCurrencyObject(_addSubscriber__return.getSubscriberWithCurrency().getCurrencyVault().getCurrencyObject());            
            _returnSubscriberWithCurrency.setCurrencyVault(_returnSubscriberWithCurrencyCurrencyVault);            
            _return.setSubscriberWithCurrency(_returnSubscriberWithCurrency);
            ResultDescriptor _returnResult = new ResultDescriptor();
            _returnResult.setResultMessage(_addSubscriber__return.getResult().getResultMessage());
            _returnResult.seteCurrencyTransactionId(_addSubscriber__return.getResult().getECurrencyTransactionId());
            _returnResult.seteCurrencyTransactionTime(_addSubscriber__return.getResult().getECurrencyTransactionTime());
            _return.setResult(_returnResult);
            return _return;
		} catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
	}
	
	
	public CashInResponse performCashIn(CashInRequest parameters) throws FaultMessage {
		
		try {
			System.setProperty("javax.net.ssl.keyStore", this.getClass().getClassLoader().getResource("keys/ws_client.jks").toURI().getPath());
			System.setProperty("javax.net.ssl.keyStorePassword", "changeit");
			System.setProperty("javax.net.ssl.trustStore", this.getClass().getClassLoader().getResource("keys/cbgui.jts").toURI().getPath());
			System.setProperty("javax.net.ssl.trustStorePassword", "changeit");        	
			
			Properties prop = new Properties();
			InputStream input = this.getClass().getClassLoader().getResourceAsStream("config/config.properties");
			prop.load(input);
			
			TARGET_ADDR = prop.getProperty("TARGET_ADDR");
			TARGET_PORT = prop.getProperty("TARGET_PORT");
			ECM_COUNTRY = prop.getProperty("ECM_COUNTRY");
			ECM_COUNTRY_UPPERCASE = prop.getProperty("ECM_COUNTRY_UPPERCASE");
			ECM_EMINAME = prop.getProperty("ECM_EMINAME");
			ECD_VERSION = prop.getProperty("ECD_VERSION");
			CURRENCY_CODE = prop.getProperty("CURRENCY_CODE");
			WSDL_LOCATION = prop.getProperty("WSDL_LOCATION"); 
			
			SERVICE_NAME = new QName("urn:com:ecmhp", "ecdV" + ECD_VERSION);
			if (TARGET_PORT.equals("80") || TARGET_PORT.equals("8080")) {
				TARGET_URL = "http://";
			} else if (TARGET_PORT.equals("443") || TARGET_PORT.equals("8443")) {
				TARGET_URL = "https://";
			} else {
				System.out.println("Only 80/443 or 8080/8443 ports are supported. Exit...");
				System.exit(0);
			}
			TARGET_URL += TARGET_ADDR + ":" + TARGET_PORT + "/emiIntegrator/services/ecdV" + ECD_VERSION + "/";
			
			ECD_ID = ECM_COUNTRY + "_" + ECM_EMINAME;
			COUNTRY_CODE = com.ecm.test.soap.ISOCountryCodeType.valueOf(ECM_COUNTRY_UPPERCASE);
			CURR_CODE = com.ecm.test.soap.ISOCurrencyCodeType.valueOf(CURRENCY_CODE);              
			
			URL wsdlURL = this.getClass().getClassLoader().getResource(WSDL_LOCATION);
			
			com.ecm.test.soap.EcdV60 ss = new com.ecm.test.soap.EcdV60(wsdlURL, SERVICE_NAME);
			com.ecm.test.soap.EcdV60SOAPPort port = ss.getEcdV60SOAPPort();  
			
			String signProp = this.getClass().getClassLoader().getResource("keys/wss_client.properties").toURI().getPath(); 
			java.util.Map<String, Object> requestContext = ((javax.xml.ws.BindingProvider)port).getRequestContext();
			requestContext.put(org.apache.cxf.message.Message.ENDPOINT_ADDRESS, TARGET_URL);
			requestContext.put("security.signature.properties", signProp);
			requestContext.put("security.callback-handler", "com.ecm.test.soap.PWCBHandler");
			
			com.ecm.test.soap.CashInRequest _performCashIn_parameters = new com.ecm.test.soap.CashInRequest();
		    _performCashIn_parameters.setEcdHeader(parameters.ecdHeader);
		    com.ecm.test.soap.CashInDescriptor _performCashIn_parametersDescriptor = new com.ecm.test.soap.CashInDescriptor();
		    com.ecm.test.soap.EcdWithCurrencyDescriptor _performCashIn_parametersDescriptorSourceEcd = new com.ecm.test.soap.EcdWithCurrencyDescriptor();
		    com.ecm.test.soap.EcdDescriptor _performCashIn_parametersDescriptorSourceEcdEcd = new com.ecm.test.soap.EcdDescriptor();
		    com.ecm.test.soap.ISOCountryCodeType _performCashIn_parametersDescriptorSourceEcdEcdCountryID = com.ecm.test.soap.ISOCountryCodeType.valueOf(parameters.descriptor.sourceEcd.ecd.countryID.name());
		    _performCashIn_parametersDescriptorSourceEcdEcd.setCountryID(_performCashIn_parametersDescriptorSourceEcdEcdCountryID);
		    _performCashIn_parametersDescriptorSourceEcdEcd.setEcdID(parameters.descriptor.sourceEcd.ecd.ecdID);
		    _performCashIn_parametersDescriptorSourceEcd.setEcd(_performCashIn_parametersDescriptorSourceEcdEcd);
		    com.ecm.test.soap.CurrencyVaultDescriptor _performCashIn_parametersDescriptorSourceEcdCurrencyVault = new com.ecm.test.soap.CurrencyVaultDescriptor();
		    com.ecm.test.soap.AmountDescriptor _performCashIn_parametersDescriptorSourceEcdCurrencyVaultAmount = new com.ecm.test.soap.AmountDescriptor();
		    com.ecm.test.soap.ISOCurrencyCodeType _performCashIn_parametersDescriptorSourceEcdCurrencyVaultAmountCurrencyCode = com.ecm.test.soap.ISOCurrencyCodeType.valueOf(parameters.descriptor.sourceEcd.currencyVault.amount.currencyCode.name());
		    _performCashIn_parametersDescriptorSourceEcdCurrencyVaultAmount.setCurrencyCode(_performCashIn_parametersDescriptorSourceEcdCurrencyVaultAmountCurrencyCode);
		    _performCashIn_parametersDescriptorSourceEcdCurrencyVaultAmount.setCurrencyAmount(parameters.descriptor.sourceEcd.currencyVault.amount.currencyAmount);
		    _performCashIn_parametersDescriptorSourceEcdCurrencyVault.setAmount(_performCashIn_parametersDescriptorSourceEcdCurrencyVaultAmount);	
		    com.ecm.test.soap.PossibleVaultTypes _performCashIn_parametersDescriptorSourceEcdCurrencyVaultVaultType = com.ecm.test.soap.PossibleVaultTypes.ECDMST;
		    _performCashIn_parametersDescriptorSourceEcdCurrencyVault.setVaultType(_performCashIn_parametersDescriptorSourceEcdCurrencyVaultVaultType);
		    _performCashIn_parametersDescriptorSourceEcdCurrencyVault.setCurrencyObject(parameters.descriptor.sourceEcd.currencyVault.currencyObject);
		    _performCashIn_parametersDescriptorSourceEcd.setCurrencyVault(_performCashIn_parametersDescriptorSourceEcdCurrencyVault);
		    _performCashIn_parametersDescriptor.setSourceEcd(_performCashIn_parametersDescriptorSourceEcd);
		    
		    com.ecm.test.soap.SubscriberWithCurrencyDescriptor _performCashIn_parametersDescriptorDestinationSubscriber = new com.ecm.test.soap.SubscriberWithCurrencyDescriptor();
		    com.ecm.test.soap.SubscriberDescriptor _performCashIn_parametersDescriptorDestinationSubscriberSubscriber = new com.ecm.test.soap.SubscriberDescriptor();
		    com.ecm.test.soap.EcdDescriptor _performCashIn_parametersDescriptorDestinationSubscriberSubscriberEcd = new com.ecm.test.soap.EcdDescriptor();
		    com.ecm.test.soap.ISOCountryCodeType _performCashIn_parametersDescriptorDestinationSubscriberSubscriberEcdCountryID = com.ecm.test.soap.ISOCountryCodeType.valueOf(parameters.descriptor.destinationSubscriber.subscriber.ecd.countryID.name());
		    _performCashIn_parametersDescriptorDestinationSubscriberSubscriberEcd.setCountryID(_performCashIn_parametersDescriptorDestinationSubscriberSubscriberEcdCountryID);
		    _performCashIn_parametersDescriptorDestinationSubscriberSubscriberEcd.setEcdID(parameters.descriptor.destinationSubscriber.subscriber.ecd.ecdID);
		    _performCashIn_parametersDescriptorDestinationSubscriberSubscriber.setEcd(_performCashIn_parametersDescriptorDestinationSubscriberSubscriberEcd);
		    _performCashIn_parametersDescriptorDestinationSubscriberSubscriber.setSubscriberID(parameters.descriptor.destinationSubscriber.subscriber.subscriberID);
		    _performCashIn_parametersDescriptorDestinationSubscriber.setSubscriber(_performCashIn_parametersDescriptorDestinationSubscriberSubscriber);
		    com.ecm.test.soap.CurrencyVaultDescriptor _performCashIn_parametersDescriptorDestinationSubscriberCurrencyVault = new com.ecm.test.soap.CurrencyVaultDescriptor();
		    com.ecm.test.soap.AmountDescriptor _performCashIn_parametersDescriptorDestinationSubscriberCurrencyVaultAmount = new com.ecm.test.soap.AmountDescriptor();
		    com.ecm.test.soap.ISOCurrencyCodeType _performCashIn_parametersDescriptorDestinationSubscriberCurrencyVaultAmountCurrencyCode = com.ecm.test.soap.ISOCurrencyCodeType.valueOf(parameters.descriptor.destinationSubscriber.currencyVault.amount.currencyCode.name());
		    _performCashIn_parametersDescriptorDestinationSubscriberCurrencyVaultAmount.setCurrencyCode(_performCashIn_parametersDescriptorDestinationSubscriberCurrencyVaultAmountCurrencyCode);
		    _performCashIn_parametersDescriptorDestinationSubscriberCurrencyVaultAmount.setCurrencyAmount(parameters.descriptor.destinationSubscriber.currencyVault.amount.currencyAmount);
		    _performCashIn_parametersDescriptorDestinationSubscriberCurrencyVault.setAmount(_performCashIn_parametersDescriptorDestinationSubscriberCurrencyVaultAmount);
		    _performCashIn_parametersDescriptorDestinationSubscriberCurrencyVault.setCurrencyObject(parameters.descriptor.destinationSubscriber.currencyVault.currencyObject);
		    _performCashIn_parametersDescriptorDestinationSubscriber.setCurrencyVault(_performCashIn_parametersDescriptorDestinationSubscriberCurrencyVault);
		    _performCashIn_parametersDescriptor.setDestinationSubscriber(_performCashIn_parametersDescriptorDestinationSubscriber);
		    
		    com.ecm.test.soap.TransactionDescriptor _performCashIn_parametersDescriptorTransaction = new com.ecm.test.soap.TransactionDescriptor();
		    com.ecm.test.soap.AmountDescriptor _performCashIn_parametersDescriptorTransactionAmount = new com.ecm.test.soap.AmountDescriptor();
		    com.ecm.test.soap.ISOCurrencyCodeType _performCashIn_parametersDescriptorTransactionAmountCurrencyCode = com.ecm.test.soap.ISOCurrencyCodeType.valueOf(parameters.descriptor.transaction.amount.currencyCode.name());
		    _performCashIn_parametersDescriptorTransactionAmount.setCurrencyCode(_performCashIn_parametersDescriptorTransactionAmountCurrencyCode);
		    _performCashIn_parametersDescriptorTransactionAmount.setCurrencyAmount(parameters.descriptor.transaction.amount.currencyAmount);
		    _performCashIn_parametersDescriptorTransaction.setAmount(_performCashIn_parametersDescriptorTransactionAmount);
		    _performCashIn_parametersDescriptorTransaction.setEcdTransactionId(parameters.descriptor.transaction.ecdTransactionId);
		    _performCashIn_parametersDescriptorTransaction.setEcdTransactionTime(parameters.descriptor.transaction.ecdTransactionTime);
		    _performCashIn_parametersDescriptorTransaction.setAdditionalEcdTransactionData(parameters.descriptor.transaction.additionalEcdTransactionData);
		    _performCashIn_parametersDescriptorTransaction.setEcdTransactionType(parameters.descriptor.transaction.ecdTransactionType);
		    _performCashIn_parametersDescriptor.setTransaction(_performCashIn_parametersDescriptorTransaction);
		    _performCashIn_parameters.setDescriptor(_performCashIn_parametersDescriptor);
		    	   
		    com.ecm.test.soap.CashInResponse _performCashIn__return = port.performCashIn(_performCashIn_parameters);			
		    
			CashInResponse _return = new CashInResponse();
            _return.setEcdHeader(_performCashIn__return.getEcdHeader());
            CashInDescriptor _returnDescriptor = new CashInDescriptor();
            EcdWithCurrencyDescriptor _returnDescriptorSourceEcd = new EcdWithCurrencyDescriptor();
            EcdDescriptor _returnDescriptorSourceEcdEcd = new EcdDescriptor();
            _returnDescriptorSourceEcdEcd.setCountryID(ISOCountryCodeType.valueOf(_performCashIn__return.getDescriptor().getSourceEcd().getEcd().getCountryID().name()));
            _returnDescriptorSourceEcdEcd.setEcdID(_performCashIn__return.getDescriptor().getSourceEcd().getEcd().getEcdID());
            _returnDescriptorSourceEcdEcd.setPartitionID(_performCashIn__return.getDescriptor().getSourceEcd().getEcd().getPartitionID());
            _returnDescriptorSourceEcd.setEcd(_returnDescriptorSourceEcdEcd);
            CurrencyVaultDescriptor _returnDescriptorSourceEcdCurrencyVault = new CurrencyVaultDescriptor();
            AmountDescriptor _returnDescriptorSourceEcdCurrencyVaultAmount = new AmountDescriptor();
            _returnDescriptorSourceEcdCurrencyVaultAmount.setCurrencyCode(ISOCurrencyCodeType.valueOf(_performCashIn__return.getDescriptor().getSourceEcd().getCurrencyVault().getAmount().getCurrencyCode().name()));
            _returnDescriptorSourceEcdCurrencyVaultAmount.setCurrencyAmount(_performCashIn__return.getDescriptor().getSourceEcd().getCurrencyVault().getAmount().getCurrencyAmount());
            _returnDescriptorSourceEcdCurrencyVault.setAmount(_returnDescriptorSourceEcdCurrencyVaultAmount);
            _returnDescriptorSourceEcdCurrencyVault.setCurrencyObject(_performCashIn__return.getDescriptor().getSourceEcd().getCurrencyVault().getCurrencyObject());
            _returnDescriptorSourceEcd.setCurrencyVault(_returnDescriptorSourceEcdCurrencyVault);
            _returnDescriptor.setSourceEcd(_returnDescriptorSourceEcd);            
            SubscriberWithCurrencyDescriptor _returnDescriptorDestinationSubscriber = new SubscriberWithCurrencyDescriptor();
            SubscriberDescriptor _returnDescriptorDestinationSubscriberSubscriber = new SubscriberDescriptor();
            EcdDescriptor _returnDescriptorDestinationSubscriberSubscriberEcd = new EcdDescriptor();
            _returnDescriptorDestinationSubscriberSubscriberEcd.setCountryID(ISOCountryCodeType.valueOf(_performCashIn__return.getDescriptor().getDestinationSubscriber().getSubscriber().getEcd().getCountryID().name()));
            _returnDescriptorDestinationSubscriberSubscriberEcd.setEcdID(_performCashIn__return.getDescriptor().getDestinationSubscriber().getSubscriber().getEcd().getEcdID());
            _returnDescriptorDestinationSubscriberSubscriberEcd.setPartitionID(_performCashIn__return.getDescriptor().getDestinationSubscriber().getSubscriber().getEcd().getPartitionID());
            _returnDescriptorDestinationSubscriberSubscriber.setEcd(_returnDescriptorDestinationSubscriberSubscriberEcd);            
            _returnDescriptorDestinationSubscriberSubscriber.setSubscriberID(_performCashIn__return.getDescriptor().getDestinationSubscriber().getSubscriber().getSubscriberID());
            _returnDescriptorDestinationSubscriber.setSubscriber(_returnDescriptorDestinationSubscriberSubscriber);            
            CurrencyVaultDescriptor _returnDescriptorDestinationSubscriberCurrencyVault = new CurrencyVaultDescriptor();
            AmountDescriptor _returnDescriptorDestinationSubscriberCurrencyVaultAmount = new AmountDescriptor();
            _returnDescriptorDestinationSubscriberCurrencyVaultAmount.setCurrencyCode(ISOCurrencyCodeType.valueOf(_performCashIn__return.getDescriptor().getDestinationSubscriber().getCurrencyVault().getAmount().getCurrencyCode().name()));
            _returnDescriptorDestinationSubscriberCurrencyVaultAmount.setCurrencyAmount(_performCashIn__return.getDescriptor().getDestinationSubscriber().getCurrencyVault().getAmount().getCurrencyAmount());
            _returnDescriptorDestinationSubscriberCurrencyVault.setAmount(_returnDescriptorDestinationSubscriberCurrencyVaultAmount);
            _returnDescriptorDestinationSubscriberCurrencyVault.setCurrencyObject(_performCashIn__return.getDescriptor().getDestinationSubscriber().getCurrencyVault().getCurrencyObject());
            _returnDescriptorDestinationSubscriber.setCurrencyVault(_returnDescriptorDestinationSubscriberCurrencyVault);
            _returnDescriptor.setDestinationSubscriber(_returnDescriptorDestinationSubscriber);            
            TransactionDescriptor _returnDescriptorTransaction = new TransactionDescriptor();
            AmountDescriptor _returnDescriptorTransactionAmount = new AmountDescriptor();
            _returnDescriptorTransactionAmount.setCurrencyCode(ISOCurrencyCodeType.valueOf(_performCashIn__return.getDescriptor().getTransaction().getAmount().getCurrencyCode().name()));
            _returnDescriptorTransactionAmount.setCurrencyAmount(_performCashIn__return.getDescriptor().getTransaction().getAmount().getCurrencyAmount());
            _returnDescriptorTransaction.setAmount(_returnDescriptorTransactionAmount);
            _returnDescriptorTransaction.setEcdTransactionId(_performCashIn__return.getDescriptor().getTransaction().getEcdTransactionId());
            _returnDescriptorTransaction.setEcdTransactionTime(_performCashIn__return.getDescriptor().getTransaction().getEcdTransactionTime());
            _returnDescriptorTransaction.setEcdTransactionType(_performCashIn__return.getDescriptor().getTransaction().getEcdTransactionType());
            _returnDescriptorTransaction.setAdditionalEcdTransactionData(_performCashIn__return.getDescriptor().getTransaction().getAdditionalEcdTransactionData());
            _returnDescriptor.setTransaction(_returnDescriptorTransaction);
            _return.setDescriptor(_returnDescriptor);            
            ResultDescriptor _returnResult = new ResultDescriptor();
            _returnResult.setResultMessage(_performCashIn__return.getResult().getResultMessage());
            _returnResult.seteCurrencyTransactionId(_performCashIn__return.getResult().getECurrencyTransactionId());
            _returnResult.seteCurrencyTransactionTime(_performCashIn__return.getResult().getECurrencyTransactionTime());
            _return.setResult(_returnResult);
            return _return;
		} catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        
	}
	
	public CashOutResponse performCashOut(CashOutRequest parameters) throws FaultMessage {		
		try {
			System.setProperty("javax.net.ssl.keyStore", this.getClass().getClassLoader().getResource("keys/ws_client.jks").toURI().getPath());
			System.setProperty("javax.net.ssl.keyStorePassword", "changeit");
			System.setProperty("javax.net.ssl.trustStore", this.getClass().getClassLoader().getResource("keys/cbgui.jts").toURI().getPath());
			System.setProperty("javax.net.ssl.trustStorePassword", "changeit");        	
			
			Properties prop = new Properties();
			InputStream input = this.getClass().getClassLoader().getResourceAsStream("config/config.properties");
			prop.load(input);
			
			TARGET_ADDR = prop.getProperty("TARGET_ADDR");
			TARGET_PORT = prop.getProperty("TARGET_PORT");
			ECM_COUNTRY = prop.getProperty("ECM_COUNTRY");
			ECM_COUNTRY_UPPERCASE = prop.getProperty("ECM_COUNTRY_UPPERCASE");
			ECM_EMINAME = prop.getProperty("ECM_EMINAME");
			ECD_VERSION = prop.getProperty("ECD_VERSION");
			CURRENCY_CODE = prop.getProperty("CURRENCY_CODE");
			WSDL_LOCATION = prop.getProperty("WSDL_LOCATION"); 
			
			SERVICE_NAME = new QName("urn:com:ecmhp", "ecdV" + ECD_VERSION);
			if (TARGET_PORT.equals("80") || TARGET_PORT.equals("8080")) {
			TARGET_URL = "http://";
			} else if (TARGET_PORT.equals("443") || TARGET_PORT.equals("8443")) {
			TARGET_URL = "https://";
			} else {
			System.out.println("Only 80/443 or 8080/8443 ports are supported. Exit...");
			System.exit(0);
			}
			TARGET_URL += TARGET_ADDR + ":" + TARGET_PORT + "/emiIntegrator/services/ecdV" + ECD_VERSION + "/";
			
			ECD_ID = ECM_COUNTRY + "_" + ECM_EMINAME;
			COUNTRY_CODE = com.ecm.test.soap.ISOCountryCodeType.valueOf(ECM_COUNTRY_UPPERCASE);
			CURR_CODE = com.ecm.test.soap.ISOCurrencyCodeType.valueOf(CURRENCY_CODE);              
			
			URL wsdlURL = this.getClass().getClassLoader().getResource(WSDL_LOCATION);
			
			com.ecm.test.soap.EcdV60 ss = new com.ecm.test.soap.EcdV60(wsdlURL, SERVICE_NAME);
			com.ecm.test.soap.EcdV60SOAPPort port = ss.getEcdV60SOAPPort();  
			
			String signProp = this.getClass().getClassLoader().getResource("keys/wss_client.properties").toURI().getPath(); 
			java.util.Map<String, Object> requestContext = ((javax.xml.ws.BindingProvider)port).getRequestContext();
			requestContext.put(org.apache.cxf.message.Message.ENDPOINT_ADDRESS, TARGET_URL);
			requestContext.put("security.signature.properties", signProp);
			requestContext.put("security.callback-handler", "com.ecm.test.soap.PWCBHandler");
			
			com.ecm.test.soap.CashOutRequest _performCashOut_parameters = new com.ecm.test.soap.CashOutRequest();
	        _performCashOut_parameters.setEcdHeader(parameters.ecdHeader);
	        
	        com.ecm.test.soap.CashOutDescriptor _performCashOut_parametersDescriptor = new com.ecm.test.soap.CashOutDescriptor();
	        com.ecm.test.soap.SubscriberWithCurrencyDescriptor _performCashOut_parametersDescriptorSourceSubscriber = new com.ecm.test.soap.SubscriberWithCurrencyDescriptor();
	        com.ecm.test.soap.SubscriberDescriptor _performCashOut_parametersDescriptorSourceSubscriberSubscriber = new com.ecm.test.soap.SubscriberDescriptor();
	        com.ecm.test.soap.EcdDescriptor _performCashOut_parametersDescriptorSourceSubscriberSubscriberEcd = new com.ecm.test.soap.EcdDescriptor();
	        com.ecm.test.soap.ISOCountryCodeType _performCashOut_parametersDescriptorSourceSubscriberSubscriberEcdCountryID = com.ecm.test.soap.ISOCountryCodeType.valueOf(parameters.descriptor.sourceSubscriber.subscriber.ecd.countryID.name());
	        _performCashOut_parametersDescriptorSourceSubscriberSubscriberEcd.setCountryID(_performCashOut_parametersDescriptorSourceSubscriberSubscriberEcdCountryID);
	        _performCashOut_parametersDescriptorSourceSubscriberSubscriberEcd.setEcdID(parameters.descriptor.sourceSubscriber.subscriber.ecd.ecdID);
	        _performCashOut_parametersDescriptorSourceSubscriberSubscriber.setEcd(_performCashOut_parametersDescriptorSourceSubscriberSubscriberEcd);
	        _performCashOut_parametersDescriptorSourceSubscriberSubscriber.setSubscriberID(parameters.descriptor.sourceSubscriber.subscriber.subscriberID);
	        _performCashOut_parametersDescriptorSourceSubscriber.setSubscriber(_performCashOut_parametersDescriptorSourceSubscriberSubscriber);
	        com.ecm.test.soap.CurrencyVaultDescriptor _performCashOut_parametersDescriptorSourceSubscriberCurrencyVault = new com.ecm.test.soap.CurrencyVaultDescriptor();
	        com.ecm.test.soap.AmountDescriptor _performCashOut_parametersDescriptorSourceSubscriberCurrencyVaultAmount = new com.ecm.test.soap.AmountDescriptor();
	        com.ecm.test.soap.ISOCurrencyCodeType _performCashOut_parametersDescriptorSourceSubscriberCurrencyVaultAmountCurrencyCode = com.ecm.test.soap.ISOCurrencyCodeType.valueOf(parameters.descriptor.sourceSubscriber.currencyVault.amount.currencyCode.name());
	        _performCashOut_parametersDescriptorSourceSubscriberCurrencyVaultAmount.setCurrencyCode(_performCashOut_parametersDescriptorSourceSubscriberCurrencyVaultAmountCurrencyCode);
	        _performCashOut_parametersDescriptorSourceSubscriberCurrencyVaultAmount.setCurrencyAmount(parameters.descriptor.sourceSubscriber.currencyVault.amount.currencyAmount);
	        _performCashOut_parametersDescriptorSourceSubscriberCurrencyVault.setAmount(_performCashOut_parametersDescriptorSourceSubscriberCurrencyVaultAmount);
	        _performCashOut_parametersDescriptorSourceSubscriberCurrencyVault.setCurrencyObject(parameters.descriptor.sourceSubscriber.currencyVault.currencyObject);
	        _performCashOut_parametersDescriptorSourceSubscriber.setCurrencyVault(_performCashOut_parametersDescriptorSourceSubscriberCurrencyVault);
	        _performCashOut_parametersDescriptor.setSourceSubscriber(_performCashOut_parametersDescriptorSourceSubscriber);
	        
	        com.ecm.test.soap.EcdWithCurrencyDescriptor _performCashOut_parametersDescriptorDestinationEcd = new com.ecm.test.soap.EcdWithCurrencyDescriptor();
	        com.ecm.test.soap.EcdDescriptor _performCashOut_parametersDescriptorDestinationEcdEcd = new com.ecm.test.soap.EcdDescriptor();
	        com.ecm.test.soap.ISOCountryCodeType _performCashOut_parametersDescriptorDestinationEcdEcdCountryID = com.ecm.test.soap.ISOCountryCodeType.valueOf(parameters.descriptor.destinationEcd.ecd.countryID.name());
	        _performCashOut_parametersDescriptorDestinationEcdEcd.setCountryID(_performCashOut_parametersDescriptorDestinationEcdEcdCountryID);
	        _performCashOut_parametersDescriptorDestinationEcdEcd.setEcdID(parameters.descriptor.destinationEcd.ecd.ecdID);
	        _performCashOut_parametersDescriptorDestinationEcd.setEcd(_performCashOut_parametersDescriptorDestinationEcdEcd);
	        com.ecm.test.soap.CurrencyVaultDescriptor _performCashOut_parametersDescriptorDestinationEcdCurrencyVault = new com.ecm.test.soap.CurrencyVaultDescriptor();
	        com.ecm.test.soap.AmountDescriptor _performCashOut_parametersDescriptorDestinationEcdCurrencyVaultAmount = new com.ecm.test.soap.AmountDescriptor();
	        com.ecm.test.soap.ISOCurrencyCodeType _performCashOut_parametersDescriptorDestinationEcdCurrencyVaultAmountCurrencyCode = com.ecm.test.soap.ISOCurrencyCodeType.valueOf(parameters.descriptor.destinationEcd.currencyVault.amount.currencyCode.name());
	        _performCashOut_parametersDescriptorDestinationEcdCurrencyVaultAmount.setCurrencyCode(_performCashOut_parametersDescriptorDestinationEcdCurrencyVaultAmountCurrencyCode);
	        _performCashOut_parametersDescriptorDestinationEcdCurrencyVaultAmount.setCurrencyAmount(parameters.descriptor.destinationEcd.currencyVault.amount.currencyAmount);
	        _performCashOut_parametersDescriptorDestinationEcdCurrencyVault.setAmount(_performCashOut_parametersDescriptorDestinationEcdCurrencyVaultAmount);
	        com.ecm.test.soap.PossibleVaultTypes _performCashOut_parametersDescriptorDestinationEcdCurrencyVaultVaultType = com.ecm.test.soap.PossibleVaultTypes.ECDMST;
	        _performCashOut_parametersDescriptorDestinationEcdCurrencyVault.setVaultType(_performCashOut_parametersDescriptorDestinationEcdCurrencyVaultVaultType);
	        _performCashOut_parametersDescriptorDestinationEcdCurrencyVault.setCurrencyObject(parameters.descriptor.destinationEcd.currencyVault.currencyObject);
	        _performCashOut_parametersDescriptorDestinationEcd.setCurrencyVault(_performCashOut_parametersDescriptorDestinationEcdCurrencyVault);
	        _performCashOut_parametersDescriptor.setDestinationEcd(_performCashOut_parametersDescriptorDestinationEcd);
	        
	        com.ecm.test.soap.TransactionDescriptor _performCashOut_parametersDescriptorTransaction = new com.ecm.test.soap.TransactionDescriptor();
	        com.ecm.test.soap.AmountDescriptor _performCashOut_parametersDescriptorTransactionAmount = new com.ecm.test.soap.AmountDescriptor();
	        com.ecm.test.soap.ISOCurrencyCodeType _performCashOut_parametersDescriptorTransactionAmountCurrencyCode = com.ecm.test.soap.ISOCurrencyCodeType.valueOf(parameters.descriptor.transaction.amount.currencyCode.name());
	        _performCashOut_parametersDescriptorTransactionAmount.setCurrencyCode(_performCashOut_parametersDescriptorTransactionAmountCurrencyCode);
	        _performCashOut_parametersDescriptorTransactionAmount.setCurrencyAmount(parameters.descriptor.transaction.amount.currencyAmount);
	        _performCashOut_parametersDescriptorTransaction.setAmount(_performCashOut_parametersDescriptorTransactionAmount);
	        _performCashOut_parametersDescriptorTransaction.setEcdTransactionId(parameters.descriptor.transaction.ecdTransactionId);
	        _performCashOut_parametersDescriptorTransaction.setEcdTransactionTime(parameters.descriptor.transaction.ecdTransactionTime);
	        _performCashOut_parametersDescriptorTransaction.setAdditionalEcdTransactionData(parameters.descriptor.transaction.additionalEcdTransactionData);
	        _performCashOut_parametersDescriptor.setTransaction(_performCashOut_parametersDescriptorTransaction);
	        _performCashOut_parameters.setDescriptor(_performCashOut_parametersDescriptor);
	        	        
	        com.ecm.test.soap.CashOutResponse _performCashOut__return = port.performCashOut(_performCashOut_parameters);
	        
			CashOutResponse _return = new CashOutResponse();
            _return.setEcdHeader(_performCashOut__return.getEcdHeader());
            
            CashOutDescriptor _returnDescriptor = new CashOutDescriptor();
            SubscriberWithCurrencyDescriptor _returnDescriptorSourceSubscriber = new SubscriberWithCurrencyDescriptor();
            SubscriberDescriptor _returnDescriptorSourceSubscriberSubscriber = new SubscriberDescriptor();
            EcdDescriptor _returnDescriptorSourceSubscriberSubscriberEcd = new EcdDescriptor();
            _returnDescriptorSourceSubscriberSubscriberEcd.setCountryID(ISOCountryCodeType.valueOf(_performCashOut__return.getDescriptor().getSourceSubscriber().getSubscriber().getEcd().getCountryID().name()));
            _returnDescriptorSourceSubscriberSubscriberEcd.setEcdID(_performCashOut__return.getDescriptor().getSourceSubscriber().getSubscriber().getEcd().getEcdID());
            _returnDescriptorSourceSubscriberSubscriber.setEcd(_returnDescriptorSourceSubscriberSubscriberEcd);
            _returnDescriptorSourceSubscriberSubscriber.setSubscriberID(_performCashOut__return.getDescriptor().getSourceSubscriber().getSubscriber().getSubscriberID());
            _returnDescriptorSourceSubscriber.setSubscriber(_returnDescriptorSourceSubscriberSubscriber);        
            CurrencyVaultDescriptor _returnDescriptorSourceSubscriberCurrencyVault = new CurrencyVaultDescriptor();
            AmountDescriptor _returnDescriptorSourceSubscriberCurrencyVaultAmount = new AmountDescriptor(); 
            _returnDescriptorSourceSubscriberCurrencyVaultAmount.setCurrencyCode(ISOCurrencyCodeType.valueOf(_performCashOut__return.getDescriptor().getSourceSubscriber().getCurrencyVault().getAmount().getCurrencyCode().name()));
            _returnDescriptorSourceSubscriberCurrencyVaultAmount.setCurrencyAmount(_performCashOut__return.getDescriptor().getSourceSubscriber().getCurrencyVault().getAmount().getCurrencyAmount());
            _returnDescriptorSourceSubscriberCurrencyVault.setAmount(_returnDescriptorSourceSubscriberCurrencyVaultAmount);
            _returnDescriptorSourceSubscriberCurrencyVault.setCurrencyObject(_performCashOut__return.getDescriptor().getSourceSubscriber().getCurrencyVault().getCurrencyObject());
            _returnDescriptorSourceSubscriber.setCurrencyVault(_returnDescriptorSourceSubscriberCurrencyVault);            
            _returnDescriptor.setSourceSubscriber(_returnDescriptorSourceSubscriber);
            
            EcdWithCurrencyDescriptor _returnDescriptorDestinationEcd = new EcdWithCurrencyDescriptor();
            EcdDescriptor _returnDescriptorDestinationEcdEcd = new EcdDescriptor(); 
            _returnDescriptorDestinationEcdEcd.setCountryID(ISOCountryCodeType.valueOf(_performCashOut__return.getDescriptor().getSourceSubscriber().getSubscriber().getEcd().getCountryID().name()));
            _returnDescriptorDestinationEcdEcd.setEcdID(_performCashOut__return.getDescriptor().getSourceSubscriber().getSubscriber().getEcd().getEcdID());
            _returnDescriptorDestinationEcd.setEcd(_returnDescriptorDestinationEcdEcd);
            CurrencyVaultDescriptor _returnDescriptorDestinationEcdCurrencyVault = new CurrencyVaultDescriptor();
            AmountDescriptor _returnDescriptorDestinationEcdCurrencyVaultAmount = new AmountDescriptor();
            _returnDescriptorDestinationEcdCurrencyVaultAmount.setCurrencyCode(ISOCurrencyCodeType.valueOf(_performCashOut__return.getDescriptor().getDestinationEcd().getCurrencyVault().getAmount().getCurrencyCode().name()));
            _returnDescriptorDestinationEcdCurrencyVaultAmount.setCurrencyAmount(_performCashOut__return.getDescriptor().getDestinationEcd().getCurrencyVault().getAmount().getCurrencyAmount());
            _returnDescriptorDestinationEcdCurrencyVault.setAmount(_returnDescriptorDestinationEcdCurrencyVaultAmount);
            _returnDescriptorDestinationEcdCurrencyVault.setCurrencyObject(_performCashOut__return.getDescriptor().getDestinationEcd().getCurrencyVault().getCurrencyObject());
            _returnDescriptorDestinationEcd.setCurrencyVault(_returnDescriptorDestinationEcdCurrencyVault);
            _returnDescriptor.setDestinationEcd(_returnDescriptorDestinationEcd);
            
            TransactionDescriptor _returnDescriptorTransaction = new TransactionDescriptor();
            AmountDescriptor _returnDescriptorTransactionAmount = new AmountDescriptor();
            _returnDescriptorTransactionAmount.setCurrencyCode(ISOCurrencyCodeType.valueOf(_performCashOut__return.getDescriptor().getTransaction().getAmount().getCurrencyCode().name()));
            _returnDescriptorTransactionAmount.setCurrencyAmount(_performCashOut__return.getDescriptor().getTransaction().getAmount().getCurrencyAmount());
            _returnDescriptorTransaction.setAmount(_returnDescriptorTransactionAmount);
            _returnDescriptorTransaction.setEcdTransactionId(_performCashOut__return.getDescriptor().getTransaction().getEcdTransactionId());
            _returnDescriptorTransaction.setEcdTransactionTime(_performCashOut__return.getDescriptor().getTransaction().getEcdTransactionTime());
            _returnDescriptorTransaction.setAdditionalEcdTransactionData(_performCashOut__return.getDescriptor().getTransaction().getAdditionalEcdTransactionData());
            _returnDescriptor.setTransaction(_returnDescriptorTransaction);
            
            _return.setDescriptor(_returnDescriptor);
            
            ResultDescriptor _returnResult = new ResultDescriptor();
	        _returnResult.setResultMessage(_performCashOut__return.getResult().getResultMessage());
	        _returnResult.seteCurrencyTransactionId(_performCashOut__return.getResult().getECurrencyTransactionId());
	        _returnResult.seteCurrencyTransactionTime(_performCashOut__return.getResult().getECurrencyTransactionTime());
	        
	        _return.setResult(_returnResult);			
			return _return;
		} catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
	}
	
	public GetSubscriberResponse getSubscriber(GetSubscriberRequest parameters) throws FaultMessage   { 
        try {
        	
        	System.setProperty("javax.net.ssl.keyStore", this.getClass().getClassLoader().getResource("keys/ws_client.jks").toURI().getPath());
        	System.setProperty("javax.net.ssl.keyStorePassword", "changeit");
        	System.setProperty("javax.net.ssl.trustStore", this.getClass().getClassLoader().getResource("keys/cbgui.jts").toURI().getPath());
        	System.setProperty("javax.net.ssl.trustStorePassword", "changeit");
        	        	
        	Properties prop = new Properties();
            InputStream input = this.getClass().getClassLoader().getResourceAsStream("config/config.properties");
    		prop.load(input);	
    		    		
    		TARGET_ADDR = prop.getProperty("TARGET_ADDR");
            TARGET_PORT = prop.getProperty("TARGET_PORT");
            ECM_COUNTRY = prop.getProperty("ECM_COUNTRY");
            ECM_COUNTRY_UPPERCASE = prop.getProperty("ECM_COUNTRY_UPPERCASE");
            ECM_EMINAME = prop.getProperty("ECM_EMINAME");
            ECD_VERSION = prop.getProperty("ECD_VERSION");
            CURRENCY_CODE = prop.getProperty("CURRENCY_CODE");
            WSDL_LOCATION = prop.getProperty("WSDL_LOCATION");

            SERVICE_NAME = new QName("urn:com:ecmhp", "ecdV" + ECD_VERSION);
            if (TARGET_PORT.equals("80") || TARGET_PORT.equals("8080")) {
                TARGET_URL = "http://";
            } else if (TARGET_PORT.equals("443") || TARGET_PORT.equals("8443")) {
                TARGET_URL = "https://";
            } else {
                System.out.println("Only 80/443 or 8080/8443 ports are supported. Exit...");
                System.exit(0);
            }
            TARGET_URL += TARGET_ADDR + ":" + TARGET_PORT + "/emiIntegrator/services/ecdV" + ECD_VERSION + "/";
            
            ECD_ID = ECM_COUNTRY + "_" + ECM_EMINAME;
			COUNTRY_CODE = com.ecm.test.soap.ISOCountryCodeType.valueOf(ECM_COUNTRY_UPPERCASE);
			CURR_CODE = com.ecm.test.soap.ISOCurrencyCodeType.valueOf(CURRENCY_CODE);                
            
            URL wsdlURL = this.getClass().getClassLoader().getResource(WSDL_LOCATION);
                        
            com.ecm.test.soap.EcdV60 ss = new com.ecm.test.soap.EcdV60(wsdlURL, SERVICE_NAME);
            com.ecm.test.soap.EcdV60SOAPPort port = ss.getEcdV60SOAPPort();  
                        
            String signProp = this.getClass().getClassLoader().getResource("keys/wss_client.properties").toURI().getPath(); 
            java.util.Map<String, Object> requestContext = ((javax.xml.ws.BindingProvider)port).getRequestContext();
            requestContext.put(org.apache.cxf.message.Message.ENDPOINT_ADDRESS, TARGET_URL);
            requestContext.put("security.signature.properties", signProp);
            requestContext.put("security.callback-handler", "com.ecm.test.soap.PWCBHandler");
                        
		    com.ecm.test.soap.GetSubscriberRequest _getSubscriber_parameters = new com.ecm.test.soap.GetSubscriberRequest();
		    _getSubscriber_parameters.setEcdHeader(parameters.ecdHeader);
		    com.ecm.test.soap.SubscriberDescriptor _getSubscriber_parametersSubscriber = new com.ecm.test.soap.SubscriberDescriptor();
		    com.ecm.test.soap.EcdDescriptor _getSubscriber_parametersSubscriberEcd = new com.ecm.test.soap.EcdDescriptor();
		    _getSubscriber_parametersSubscriberEcd.setCbID("CbID-529007580");
		    _getSubscriber_parametersSubscriberEcd.setCommercialBankID("CommercialBankID1628986020");
		    com.ecm.test.soap.ISOCountryCodeType _getSubscriber_parametersSubscriberEcdCountryID = com.ecm.test.soap.ISOCountryCodeType.PH;
		    _getSubscriber_parametersSubscriberEcd.setCountryID(_getSubscriber_parametersSubscriberEcdCountryID);
		    _getSubscriber_parametersSubscriberEcd.setEcdID(parameters.subscriber.ecd.ecdID);
		    _getSubscriber_parametersSubscriberEcd.setPartitionID("PartitionID-42582324");
		    _getSubscriber_parametersSubscriber.setEcd(_getSubscriber_parametersSubscriberEcd);
		    _getSubscriber_parametersSubscriber.setSubscriberID(parameters.subscriber.subscriberID);
		    _getSubscriber_parameters.setSubscriber(_getSubscriber_parametersSubscriber);
		     
		    com.ecm.test.soap.GetSubscriberResponse _getSubscriber__return = port.getSubscriber(_getSubscriber_parameters);
                                                                	        	
            GetSubscriberResponse _return = new GetSubscriberResponse();
            _return.setEcdHeader(_getSubscriber__return.getEcdHeader());
            SubscriberWithCurrencyDescriptor _returnSubscriberWithCurrency = new SubscriberWithCurrencyDescriptor();
            SubscriberDescriptor _returnSubscriberWithCurrencySubscriber = new SubscriberDescriptor();
            EcdDescriptor _returnSubscriberWithCurrencySubscriberEcd = new EcdDescriptor();
            ISOCountryCodeType _responseISOCountryCode = ISOCountryCodeType.valueOf(_getSubscriber__return.getSubscriberWithCurrency().getSubscriber().getEcd().getCountryID().name());
            _returnSubscriberWithCurrencySubscriberEcd.setCountryID(_responseISOCountryCode);
            _returnSubscriberWithCurrencySubscriberEcd.setEcdID(parameters.subscriber.ecd.ecdID);
            _returnSubscriberWithCurrencySubscriber.setEcd(_returnSubscriberWithCurrencySubscriberEcd);
            _returnSubscriberWithCurrencySubscriber.setSubscriberID(_getSubscriber__return.getSubscriberWithCurrency().getSubscriber().getSubscriberID());            
            PossibleSubscriberTypes _responseSubsCountryID = PossibleSubscriberTypes.valueOf(_getSubscriber__return.getSubscriberWithCurrency().getSubscriber().getSubscriberType().name());
            PossibleSubscriberStates _responseSubsStates = PossibleSubscriberStates.valueOf(_getSubscriber__return.getSubscriberWithCurrency().getSubscriber().getSubscriberState().name());
            _returnSubscriberWithCurrencySubscriber.setSubscriberType(_responseSubsCountryID);
            _returnSubscriberWithCurrencySubscriber.setSubscriberState(_responseSubsStates);
            _returnSubscriberWithCurrency.setSubscriber(_returnSubscriberWithCurrencySubscriber);            
            CurrencyVaultDescriptor _returnSubscriberWithCurrencyCurrencyVault = new CurrencyVaultDescriptor(); 
            AmountDescriptor _returnSubscriberWithCurrencyCurrencyVaultAmount = new AmountDescriptor();
            ISOCurrencyCodeType _returnSubscriberWithCurrencyCurrencyVaultAmountCurrencyCode = ISOCurrencyCodeType.valueOf(_getSubscriber__return.getSubscriberWithCurrency().getCurrencyVault().getAmount().getCurrencyCode().name());
            _returnSubscriberWithCurrencyCurrencyVaultAmount.setCurrencyCode(_returnSubscriberWithCurrencyCurrencyVaultAmountCurrencyCode);
            _returnSubscriberWithCurrencyCurrencyVaultAmount.setCurrencyAmount(_getSubscriber__return.getSubscriberWithCurrency().getCurrencyVault().getAmount().getCurrencyAmount());
            _returnSubscriberWithCurrencyCurrencyVault.setAmount(_returnSubscriberWithCurrencyCurrencyVaultAmount);
            _returnSubscriberWithCurrencyCurrencyVault.setCurrencyObject(_getSubscriber__return.getSubscriberWithCurrency().getCurrencyVault().getCurrencyObject());            
            _returnSubscriberWithCurrency.setCurrencyVault(_returnSubscriberWithCurrencyCurrencyVault);            
            _return.setSubscriberWithCurrency(_returnSubscriberWithCurrency);
            ResultDescriptor _returnResult = new ResultDescriptor();
            _returnResult.setResultMessage(_getSubscriber__return.getResult().getResultMessage());
            _returnResult.seteCurrencyTransactionId(_getSubscriber__return.getResult().getECurrencyTransactionId());
            _returnResult.seteCurrencyTransactionTime(_getSubscriber__return.getResult().getECurrencyTransactionTime());
            _return.setResult(_returnResult);
            return _return;
        } catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }	
	}
	
	public SetSubscriberResponse setSubscriber(SetSubscriberRequest parameters) throws FaultMessage   { 
        try {
        	
        	File f = new File(this.getClass().getClassLoader().getResource("keys/ws_client.jks").toURI().getPath());
        	System.out.println("KeyStore Path: " + this.getClass().getClassLoader().getResource("keys/ws_client.jks").toURI().getPath());
			System.out.println("KeyStore Exist: " + f.exists());
                	
            SetSubscriberResponse _return = new SetSubscriberResponse();
            _return.setEcdHeader("EcdHeader-878007461");
            SubscriberDescriptor _returnSubscriber = new SubscriberDescriptor();
            EcdDescriptor _returnSubscriberEcd = new EcdDescriptor();
            _returnSubscriberEcd.setCbID("CbID-101985277");
            _returnSubscriberEcd.setCommercialBankID("CommercialBankID422290617");
            ISOCountryCodeType _returnSubscriberEcdCountryID = ISOCountryCodeType.LY;
            _returnSubscriberEcd.setCountryID(_returnSubscriberEcdCountryID);
            _returnSubscriberEcd.setEcdID("EcdID2029227605");
            _returnSubscriberEcd.setPartitionID("PartitionID-2070328");
            _returnSubscriber.setEcd(_returnSubscriberEcd);
            _returnSubscriber.setSubscriberID("SubscriberID996237146");
            PossibleSubscriberTypes _returnSubscriberSubscriberType = PossibleSubscriberTypes.SUB;
            _returnSubscriber.setSubscriberType(_returnSubscriberSubscriberType);
            PossibleSubscriberStates _returnSubscriberSubscriberState = PossibleSubscriberStates.BLOCKED;
            _returnSubscriber.setSubscriberState(_returnSubscriberSubscriberState);
            _returnSubscriber.setEcdSubscriberType("EcdSubscriberType164482436");
            _return.setSubscriber(_returnSubscriber);
            ResultDescriptor _returnResult = new ResultDescriptor();
            _returnResult.setResultMessage("ResultMessage1752049809");
            _returnResult.seteCurrencyTransactionId("ECurrencyTransactionId295445513");
            _returnResult.seteCurrencyTransactionTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar("2018-02-01T11:46:40.783-08:00"));
            _return.setResult(_returnResult);
            return _return;
        } catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
    }
	
	public DeleteSubscriberResponse deleteSubscriber(DeleteSubscriberRequest parameters) throws FaultMessage   { 
        try {
        	System.setProperty("javax.net.ssl.keyStore", this.getClass().getClassLoader().getResource("keys/ws_client.jks").toURI().getPath());
        	System.setProperty("javax.net.ssl.keyStorePassword", "changeit");
        	System.setProperty("javax.net.ssl.trustStore", this.getClass().getClassLoader().getResource("keys/cbgui.jts").toURI().getPath());
        	System.setProperty("javax.net.ssl.trustStorePassword", "changeit");        	
        	
        	Properties prop = new Properties();
            InputStream input = this.getClass().getClassLoader().getResourceAsStream("config/config.properties");
    		prop.load(input);
    		
    		TARGET_ADDR = prop.getProperty("TARGET_ADDR");
            TARGET_PORT = prop.getProperty("TARGET_PORT");
            ECM_COUNTRY = prop.getProperty("ECM_COUNTRY");
            ECM_COUNTRY_UPPERCASE = prop.getProperty("ECM_COUNTRY_UPPERCASE");
            ECM_EMINAME = prop.getProperty("ECM_EMINAME");
            ECD_VERSION = prop.getProperty("ECD_VERSION");
            CURRENCY_CODE = prop.getProperty("CURRENCY_CODE");
            WSDL_LOCATION = prop.getProperty("WSDL_LOCATION"); 

            SERVICE_NAME = new QName("urn:com:ecmhp", "ecdV" + ECD_VERSION);
            if (TARGET_PORT.equals("80") || TARGET_PORT.equals("8080")) {
                TARGET_URL = "http://";
            } else if (TARGET_PORT.equals("443") || TARGET_PORT.equals("8443")) {
                TARGET_URL = "https://";
            } else {
                System.out.println("Only 80/443 or 8080/8443 ports are supported. Exit...");
                System.exit(0);
            }
            TARGET_URL += TARGET_ADDR + ":" + TARGET_PORT + "/emiIntegrator/services/ecdV" + ECD_VERSION + "/";
            
            ECD_ID = ECM_COUNTRY + "_" + ECM_EMINAME;
			COUNTRY_CODE = com.ecm.test.soap.ISOCountryCodeType.valueOf(ECM_COUNTRY_UPPERCASE);
			CURR_CODE = com.ecm.test.soap.ISOCurrencyCodeType.valueOf(CURRENCY_CODE);              
            
            URL wsdlURL = this.getClass().getClassLoader().getResource(WSDL_LOCATION);
                        
            com.ecm.test.soap.EcdV60 ss = new com.ecm.test.soap.EcdV60(wsdlURL, SERVICE_NAME);
            com.ecm.test.soap.EcdV60SOAPPort port = ss.getEcdV60SOAPPort();  
                        
            String signProp = this.getClass().getClassLoader().getResource("keys/wss_client.properties").toURI().getPath(); 
            java.util.Map<String, Object> requestContext = ((javax.xml.ws.BindingProvider)port).getRequestContext();
            requestContext.put(org.apache.cxf.message.Message.ENDPOINT_ADDRESS, TARGET_URL);
            requestContext.put("security.signature.properties", signProp);
            requestContext.put("security.callback-handler", "com.ecm.test.soap.PWCBHandler");
            
            com.ecm.test.soap.DeleteSubscriberRequest _deleteSubscriber_parameters = new com.ecm.test.soap.DeleteSubscriberRequest();
            _deleteSubscriber_parameters.setEcdHeader(parameters.ecdHeader);
            com.ecm.test.soap.SubscriberDescriptor _deleteSubscriber_parametersSubscriber = new com.ecm.test.soap.SubscriberDescriptor();
            com.ecm.test.soap.EcdDescriptor _deleteSubscriber_parametersSubscriberEcd = new com.ecm.test.soap.EcdDescriptor();
            com.ecm.test.soap.ISOCountryCodeType _deleteSubscriber_parametersSubscriberEcdCountryID = com.ecm.test.soap.ISOCountryCodeType.valueOf(parameters.subscriber.ecd.countryID.name());
            _deleteSubscriber_parametersSubscriberEcd.setCountryID(_deleteSubscriber_parametersSubscriberEcdCountryID);
            _deleteSubscriber_parametersSubscriberEcd.setEcdID(parameters.subscriber.ecd.ecdID);
            _deleteSubscriber_parametersSubscriber.setEcd(_deleteSubscriber_parametersSubscriberEcd);
            _deleteSubscriber_parametersSubscriber.setSubscriberID(parameters.subscriber.subscriberID);
            _deleteSubscriber_parameters.setSubscriber(_deleteSubscriber_parametersSubscriber);

            com.ecm.test.soap.DeleteSubscriberResponse _deleteSubscriber__return = port.deleteSubscriber(_deleteSubscriber_parameters);            
            
            DeleteSubscriberResponse _return = new DeleteSubscriberResponse();
            _return.setEcdHeader(_deleteSubscriber__return.getEcdHeader());  
            SubscriberDescriptor _returnSubscriber = new SubscriberDescriptor();
            EcdDescriptor _returnSubscriberEcd = new EcdDescriptor();
            _returnSubscriberEcd.setCbID(_deleteSubscriber__return.getSubscriber().getEcd().getCbID());
            _returnSubscriberEcd.setCountryID(ISOCountryCodeType.valueOf(_deleteSubscriber__return.getSubscriber().getEcd().getCountryID().name()));
            _returnSubscriberEcd.setEcdID(_deleteSubscriber__return.getSubscriber().getEcd().getEcdID());
            _returnSubscriber.setEcd(_returnSubscriberEcd);
            _returnSubscriber.setSubscriberID(_deleteSubscriber__return.getSubscriber().getSubscriberID());
            PossibleSubscriberTypes _returnSubscriberSubscriberType = PossibleSubscriberTypes.valueOf(_deleteSubscriber__return.getSubscriber().getSubscriberType().name());
            _returnSubscriber.setSubscriberType(_returnSubscriberSubscriberType);
            PossibleSubscriberStates _returnSubscriberSubscriberState = PossibleSubscriberStates.valueOf(_deleteSubscriber__return.getSubscriber().getSubscriberState().name());
            _returnSubscriber.setSubscriberState(_returnSubscriberSubscriberState);
            _return.setSubscriber(_returnSubscriber);
            ResultDescriptor _returnResult = new ResultDescriptor();
            _returnResult.setResultMessage(_deleteSubscriber__return.getResult().getResultMessage());
            _returnResult.seteCurrencyTransactionId(_deleteSubscriber__return.getResult().getECurrencyTransactionId());
            _returnResult.seteCurrencyTransactionTime(_deleteSubscriber__return.getResult().getECurrencyTransactionTime());
            _return.setResult(_returnResult);            
            return _return;
        } catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
    }
	
	public SubscriberTransferResponse performSubscriberTransfer(SubscriberTransferRequest parameters) throws FaultMessage   {
		try {
			System.setProperty("javax.net.ssl.keyStore", this.getClass().getClassLoader().getResource("keys/ws_client.jks").toURI().getPath());
        	System.setProperty("javax.net.ssl.keyStorePassword", "changeit");
        	System.setProperty("javax.net.ssl.trustStore", this.getClass().getClassLoader().getResource("keys/cbgui.jts").toURI().getPath());
        	System.setProperty("javax.net.ssl.trustStorePassword", "changeit");        	
        	
        	Properties prop = new Properties();
            InputStream input = this.getClass().getClassLoader().getResourceAsStream("config/config.properties");
    		prop.load(input);
    		
    		TARGET_ADDR = prop.getProperty("TARGET_ADDR");
            TARGET_PORT = prop.getProperty("TARGET_PORT");
            ECM_COUNTRY = prop.getProperty("ECM_COUNTRY");
            ECM_COUNTRY_UPPERCASE = prop.getProperty("ECM_COUNTRY_UPPERCASE");
            ECM_EMINAME = prop.getProperty("ECM_EMINAME");
            ECD_VERSION = prop.getProperty("ECD_VERSION");
            CURRENCY_CODE = prop.getProperty("CURRENCY_CODE");
            WSDL_LOCATION = prop.getProperty("WSDL_LOCATION"); 

            SERVICE_NAME = new QName("urn:com:ecmhp", "ecdV" + ECD_VERSION);
            if (TARGET_PORT.equals("80") || TARGET_PORT.equals("8080")) {
                TARGET_URL = "http://";
            } else if (TARGET_PORT.equals("443") || TARGET_PORT.equals("8443")) {
                TARGET_URL = "https://";
            } else {
                System.out.println("Only 80/443 or 8080/8443 ports are supported. Exit...");
                System.exit(0);
            }
            TARGET_URL += TARGET_ADDR + ":" + TARGET_PORT + "/emiIntegrator/services/ecdV" + ECD_VERSION + "/";
            
            ECD_ID = ECM_COUNTRY + "_" + ECM_EMINAME;
			COUNTRY_CODE = com.ecm.test.soap.ISOCountryCodeType.valueOf(ECM_COUNTRY_UPPERCASE);
			CURR_CODE = com.ecm.test.soap.ISOCurrencyCodeType.valueOf(CURRENCY_CODE);                 
            
            URL wsdlURL = this.getClass().getClassLoader().getResource(WSDL_LOCATION);
                        
            com.ecm.test.soap.EcdV60 ss = new com.ecm.test.soap.EcdV60(wsdlURL, SERVICE_NAME);
            com.ecm.test.soap.EcdV60SOAPPort port = ss.getEcdV60SOAPPort();  
                        
            String signProp = this.getClass().getClassLoader().getResource("keys/wss_client.properties").toURI().getPath(); 
            java.util.Map<String, Object> requestContext = ((javax.xml.ws.BindingProvider)port).getRequestContext();
            requestContext.put(org.apache.cxf.message.Message.ENDPOINT_ADDRESS, TARGET_URL);
            requestContext.put("security.signature.properties", signProp);
            requestContext.put("security.callback-handler", "com.ecm.test.soap.PWCBHandler");
            
            com.ecm.test.soap.SubscriberTransferRequest _performSubscriberTransfer_parameters = new com.ecm.test.soap.SubscriberTransferRequest();
		    _performSubscriberTransfer_parameters.setEcdHeader(parameters.ecdHeader);
		    
		    com.ecm.test.soap.SubscriberTransferDescriptor _performSubscriberTransfer_parametersDescriptor = new com.ecm.test.soap.SubscriberTransferDescriptor();
		    com.ecm.test.soap.SubscriberWithCurrencyDescriptor _performSubscriberTransfer_parametersDescriptorSourceSubscriber = new com.ecm.test.soap.SubscriberWithCurrencyDescriptor();
		    com.ecm.test.soap.SubscriberDescriptor _performSubscriberTransfer_parametersDescriptorSourceSubscriberSubscriber = new com.ecm.test.soap.SubscriberDescriptor();
		    com.ecm.test.soap.EcdDescriptor _performSubscriberTransfer_parametersDescriptorSourceSubscriberSubscriberEcd = new com.ecm.test.soap.EcdDescriptor();
		    com.ecm.test.soap.ISOCountryCodeType _performSubscriberTransfer_parametersDescriptorSourceSubscriberSubscriberEcdCountryID = com.ecm.test.soap.ISOCountryCodeType.valueOf(parameters.descriptor.sourceSubscriber.subscriber.ecd.countryID.name());
		    _performSubscriberTransfer_parametersDescriptorSourceSubscriberSubscriberEcd.setCountryID(_performSubscriberTransfer_parametersDescriptorSourceSubscriberSubscriberEcdCountryID);
		    _performSubscriberTransfer_parametersDescriptorSourceSubscriberSubscriberEcd.setEcdID(parameters.descriptor.sourceSubscriber.subscriber.ecd.ecdID);
		    _performSubscriberTransfer_parametersDescriptorSourceSubscriberSubscriber.setEcd(_performSubscriberTransfer_parametersDescriptorSourceSubscriberSubscriberEcd);
		    _performSubscriberTransfer_parametersDescriptorSourceSubscriberSubscriber.setSubscriberID(parameters.descriptor.sourceSubscriber.subscriber.subscriberID);
		    _performSubscriberTransfer_parametersDescriptorSourceSubscriber.setSubscriber(_performSubscriberTransfer_parametersDescriptorSourceSubscriberSubscriber);
		    com.ecm.test.soap.CurrencyVaultDescriptor _performSubscriberTransfer_parametersDescriptorSourceSubscriberCurrencyVault = new com.ecm.test.soap.CurrencyVaultDescriptor();
		    com.ecm.test.soap.AmountDescriptor _performSubscriberTransfer_parametersDescriptorSourceSubscriberCurrencyVaultAmount = new com.ecm.test.soap.AmountDescriptor();
		    com.ecm.test.soap.ISOCurrencyCodeType _performSubscriberTransfer_parametersDescriptorSourceSubscriberCurrencyVaultAmountCurrencyCode = com.ecm.test.soap.ISOCurrencyCodeType.valueOf(parameters.descriptor.sourceSubscriber.currencyVault.amount.currencyCode.name());
		    _performSubscriberTransfer_parametersDescriptorSourceSubscriberCurrencyVaultAmount.setCurrencyCode(_performSubscriberTransfer_parametersDescriptorSourceSubscriberCurrencyVaultAmountCurrencyCode);
		    _performSubscriberTransfer_parametersDescriptorSourceSubscriberCurrencyVaultAmount.setCurrencyAmount(parameters.descriptor.sourceSubscriber.currencyVault.amount.currencyAmount);
		    _performSubscriberTransfer_parametersDescriptorSourceSubscriberCurrencyVault.setAmount(_performSubscriberTransfer_parametersDescriptorSourceSubscriberCurrencyVaultAmount);
		    _performSubscriberTransfer_parametersDescriptorSourceSubscriberCurrencyVault.setCurrencyObject(parameters.descriptor.sourceSubscriber.currencyVault.currencyObject);
		    _performSubscriberTransfer_parametersDescriptorSourceSubscriber.setCurrencyVault(_performSubscriberTransfer_parametersDescriptorSourceSubscriberCurrencyVault);
		    _performSubscriberTransfer_parametersDescriptor.setSourceSubscriber(_performSubscriberTransfer_parametersDescriptorSourceSubscriber);
		      
		    com.ecm.test.soap.SubscriberWithCurrencyDescriptor _performSubscriberTransfer_parametersDescriptorDestinationSubscriber = new com.ecm.test.soap.SubscriberWithCurrencyDescriptor();
		    com.ecm.test.soap.SubscriberDescriptor _performSubscriberTransfer_parametersDescriptorDestinationSubscriberSubscriber = new com.ecm.test.soap.SubscriberDescriptor();
		    com.ecm.test.soap.EcdDescriptor _performSubscriberTransfer_parametersDescriptorDestinationSubscriberSubscriberEcd = new com.ecm.test.soap.EcdDescriptor();
		    com.ecm.test.soap.ISOCountryCodeType _performSubscriberTransfer_parametersDescriptorDestinationSubscriberSubscriberEcdCountryID = com.ecm.test.soap.ISOCountryCodeType.valueOf(parameters.descriptor.destinationSubscriber.subscriber.ecd.countryID.name());
		    _performSubscriberTransfer_parametersDescriptorDestinationSubscriberSubscriberEcd.setCountryID(_performSubscriberTransfer_parametersDescriptorDestinationSubscriberSubscriberEcdCountryID);
		    _performSubscriberTransfer_parametersDescriptorDestinationSubscriberSubscriberEcd.setEcdID(parameters.descriptor.destinationSubscriber.subscriber.ecd.ecdID);
		    _performSubscriberTransfer_parametersDescriptorDestinationSubscriberSubscriber.setEcd(_performSubscriberTransfer_parametersDescriptorDestinationSubscriberSubscriberEcd);
		    _performSubscriberTransfer_parametersDescriptorDestinationSubscriberSubscriber.setSubscriberID(parameters.descriptor.destinationSubscriber.subscriber.subscriberID);
		    _performSubscriberTransfer_parametersDescriptorDestinationSubscriber.setSubscriber(_performSubscriberTransfer_parametersDescriptorDestinationSubscriberSubscriber);
		    com.ecm.test.soap.CurrencyVaultDescriptor _performSubscriberTransfer_parametersDescriptorDestinationSubscriberCurrencyVault = new com.ecm.test.soap.CurrencyVaultDescriptor();
		    com.ecm.test.soap.AmountDescriptor _performSubscriberTransfer_parametersDescriptorDestinationSubscriberCurrencyVaultAmount = new com.ecm.test.soap.AmountDescriptor();
		    com.ecm.test.soap.ISOCurrencyCodeType _performSubscriberTransfer_parametersDescriptorDestinationSubscriberCurrencyVaultAmountCurrencyCode = com.ecm.test.soap.ISOCurrencyCodeType.valueOf(parameters.descriptor.destinationSubscriber.currencyVault.amount.currencyCode.name());
		    _performSubscriberTransfer_parametersDescriptorDestinationSubscriberCurrencyVaultAmount.setCurrencyCode(_performSubscriberTransfer_parametersDescriptorDestinationSubscriberCurrencyVaultAmountCurrencyCode);
		    _performSubscriberTransfer_parametersDescriptorDestinationSubscriberCurrencyVaultAmount.setCurrencyAmount(parameters.descriptor.destinationSubscriber.currencyVault.amount.currencyAmount);
		    _performSubscriberTransfer_parametersDescriptorDestinationSubscriberCurrencyVault.setAmount(_performSubscriberTransfer_parametersDescriptorDestinationSubscriberCurrencyVaultAmount);
		    _performSubscriberTransfer_parametersDescriptorDestinationSubscriberCurrencyVault.setCurrencyObject(parameters.descriptor.destinationSubscriber.currencyVault.currencyObject);
		    _performSubscriberTransfer_parametersDescriptorDestinationSubscriber.setCurrencyVault(_performSubscriberTransfer_parametersDescriptorDestinationSubscriberCurrencyVault);
		    _performSubscriberTransfer_parametersDescriptor.setDestinationSubscriber(_performSubscriberTransfer_parametersDescriptorDestinationSubscriber);
		      
		    com.ecm.test.soap.TransactionDescriptor _performSubscriberTransfer_parametersDescriptorTransaction = new com.ecm.test.soap.TransactionDescriptor();
		    com.ecm.test.soap.AmountDescriptor _performSubscriberTransfer_parametersDescriptorTransactionAmount = new com.ecm.test.soap.AmountDescriptor();
		    com.ecm.test.soap.ISOCurrencyCodeType _performSubscriberTransfer_parametersDescriptorTransactionAmountCurrencyCode = com.ecm.test.soap.ISOCurrencyCodeType.valueOf(parameters.descriptor.transaction.amount.currencyCode.name());
		    _performSubscriberTransfer_parametersDescriptorTransactionAmount.setCurrencyCode(_performSubscriberTransfer_parametersDescriptorTransactionAmountCurrencyCode);
		    _performSubscriberTransfer_parametersDescriptorTransactionAmount.setCurrencyAmount(parameters.descriptor.transaction.amount.currencyAmount);
		    _performSubscriberTransfer_parametersDescriptorTransaction.setAmount(_performSubscriberTransfer_parametersDescriptorTransactionAmount);
		    _performSubscriberTransfer_parametersDescriptorTransaction.setEcdTransactionId(parameters.descriptor.transaction.ecdTransactionId);
		    _performSubscriberTransfer_parametersDescriptorTransaction.setEcdTransactionTime(parameters.descriptor.transaction.ecdTransactionTime);
		    _performSubscriberTransfer_parametersDescriptorTransaction.setAdditionalEcdTransactionData(parameters.descriptor.transaction.additionalEcdTransactionData);
		    _performSubscriberTransfer_parametersDescriptor.setTransaction(_performSubscriberTransfer_parametersDescriptorTransaction);
		    _performSubscriberTransfer_parameters.setDescriptor(_performSubscriberTransfer_parametersDescriptor);
		      
            com.ecm.test.soap.SubscriberTransferResponse _performSubscriberTransfer__return = port.performSubscriberTransfer(_performSubscriberTransfer_parameters);						
		
			SubscriberTransferResponse _return = new SubscriberTransferResponse();
			_return.setEcdHeader(_performSubscriberTransfer__return.getEcdHeader());
			
			SubscriberTransferDescriptor _returnDescriptor = new SubscriberTransferDescriptor();
			
            SubscriberWithCurrencyDescriptor _returnDescriptorSourceSubscriber = new SubscriberWithCurrencyDescriptor();
            SubscriberDescriptor _returnDescriptorSourceSubscriberSubscriber = new SubscriberDescriptor();
            EcdDescriptor _returnDescriptorSourceSubscriberSubscriberEcd = new EcdDescriptor();
            ISOCountryCodeType _returnDescriptorSourceSubscriberSubscriberEcdCountryID = ISOCountryCodeType.valueOf(_performSubscriberTransfer__return.getDescriptor().getSourceSubscriber().getSubscriber().getEcd().getCountryID().name());
            _returnDescriptorSourceSubscriberSubscriberEcd.setCountryID(_returnDescriptorSourceSubscriberSubscriberEcdCountryID);
            _returnDescriptorSourceSubscriberSubscriberEcd.setEcdID(_performSubscriberTransfer__return.getDescriptor().getSourceSubscriber().getSubscriber().getEcd().getEcdID());
            _returnDescriptorSourceSubscriberSubscriber.setEcd(_returnDescriptorSourceSubscriberSubscriberEcd);
            _returnDescriptorSourceSubscriberSubscriber.setSubscriberID(_performSubscriberTransfer__return.getDescriptor().getSourceSubscriber().getSubscriber().getSubscriberID());
            _returnDescriptorSourceSubscriber.setSubscriber(_returnDescriptorSourceSubscriberSubscriber);
            CurrencyVaultDescriptor _returnDescriptorSourceSubscriberCurrencyVault = new CurrencyVaultDescriptor();
            AmountDescriptor _returnDescriptorSourceSubscriberCurrencyVaultAmount = new AmountDescriptor();
            ISOCurrencyCodeType _returnDescriptorSourceSubscriberCurrencyVaultAmountCurrencyCode = ISOCurrencyCodeType.valueOf(_performSubscriberTransfer__return.getDescriptor().getSourceSubscriber().getCurrencyVault().getAmount().getCurrencyCode().name());
            _returnDescriptorSourceSubscriberCurrencyVaultAmount.setCurrencyCode(_returnDescriptorSourceSubscriberCurrencyVaultAmountCurrencyCode);
            _returnDescriptorSourceSubscriberCurrencyVaultAmount.setCurrencyAmount(_performSubscriberTransfer__return.getDescriptor().getSourceSubscriber().getCurrencyVault().getAmount().getCurrencyAmount());
            _returnDescriptorSourceSubscriberCurrencyVault.setAmount(_returnDescriptorSourceSubscriberCurrencyVaultAmount);
            _returnDescriptorSourceSubscriberCurrencyVault.setCurrencyObject(_performSubscriberTransfer__return.getDescriptor().getSourceSubscriber().getCurrencyVault().getCurrencyObject());
            _returnDescriptorSourceSubscriber.setCurrencyVault(_returnDescriptorSourceSubscriberCurrencyVault);
            _returnDescriptor.setSourceSubscriber(_returnDescriptorSourceSubscriber);
            
            SubscriberWithCurrencyDescriptor _returnDescriptorDestinationSubscriber = new SubscriberWithCurrencyDescriptor();
            SubscriberDescriptor _returnDescriptorDestinationSubscriberSubscriber = new SubscriberDescriptor();
            EcdDescriptor _returnDescriptorDestinationSubscriberSubscriberEcd = new EcdDescriptor();
            ISOCountryCodeType _returnDescriptorDestinationSubscriberSubscriberEcdCountryID = ISOCountryCodeType.valueOf(_performSubscriberTransfer__return.getDescriptor().getDestinationSubscriber().getSubscriber().getEcd().getCountryID().name());
            _returnDescriptorDestinationSubscriberSubscriberEcd.setCountryID(_returnDescriptorDestinationSubscriberSubscriberEcdCountryID);
            _returnDescriptorSourceSubscriberSubscriberEcd.setEcdID(_performSubscriberTransfer__return.getDescriptor().getDestinationSubscriber().getSubscriber().getEcd().getEcdID());
            _returnDescriptorSourceSubscriberSubscriber.setEcd(_returnDescriptorSourceSubscriberSubscriberEcd);
            _returnDescriptorDestinationSubscriberSubscriber.setSubscriberID(_performSubscriberTransfer__return.getDescriptor().getDestinationSubscriber().getSubscriber().getSubscriberID());
            _returnDescriptorDestinationSubscriber.setSubscriber(_returnDescriptorDestinationSubscriberSubscriber);
            CurrencyVaultDescriptor _returnDescriptorDestinationSubscriberCurrencyVault = new CurrencyVaultDescriptor();
            AmountDescriptor _returnDescriptorDestinationSubscriberCurrencyVaultAmount = new AmountDescriptor();
            ISOCurrencyCodeType _returnDescriptorDestinationSubscriberCurrencyVaultAmountCurrencyCode = ISOCurrencyCodeType.valueOf(_performSubscriberTransfer__return.getDescriptor().getDestinationSubscriber().getCurrencyVault().getAmount().getCurrencyCode().name());
            _returnDescriptorDestinationSubscriberCurrencyVaultAmount.setCurrencyCode(_returnDescriptorDestinationSubscriberCurrencyVaultAmountCurrencyCode);
            _returnDescriptorDestinationSubscriberCurrencyVaultAmount.setCurrencyAmount(_performSubscriberTransfer__return.getDescriptor().getDestinationSubscriber().getCurrencyVault().getAmount().getCurrencyAmount());
            _returnDescriptorDestinationSubscriberCurrencyVault.setAmount(_returnDescriptorDestinationSubscriberCurrencyVaultAmount);
            _returnDescriptorDestinationSubscriberCurrencyVault.setCurrencyObject(_performSubscriberTransfer__return.getDescriptor().getDestinationSubscriber().getCurrencyVault().getCurrencyObject());
            _returnDescriptorDestinationSubscriber.setCurrencyVault(_returnDescriptorDestinationSubscriberCurrencyVault);
            _returnDescriptor.setDestinationSubscriber(_returnDescriptorDestinationSubscriber);
            
            TransactionDescriptor _returnDescriptorTransaction = new TransactionDescriptor();
            AmountDescriptor _returnDescriptorTransactionAmount = new AmountDescriptor();
            ISOCurrencyCodeType _returnDescriptorTransactionAmountCurrencyCode = ISOCurrencyCodeType.valueOf(_performSubscriberTransfer__return.getDescriptor().getTransaction().getAmount().getCurrencyCode().name());
            _returnDescriptorTransactionAmount.setCurrencyCode(_returnDescriptorTransactionAmountCurrencyCode);
            _returnDescriptorTransactionAmount.setCurrencyAmount(_performSubscriberTransfer__return.getDescriptor().getTransaction().getAmount().getCurrencyAmount());
            _returnDescriptorTransaction.setAmount(_returnDescriptorTransactionAmount);
            _returnDescriptorTransaction.setEcdTransactionId(_performSubscriberTransfer__return.getDescriptor().getTransaction().getEcdTransactionId());
            _returnDescriptorTransaction.setEcdTransactionTime(_performSubscriberTransfer__return.getDescriptor().getTransaction().getEcdTransactionTime());  
            _returnDescriptorTransaction.setEcdTransactionType(_performSubscriberTransfer__return.getDescriptor().getTransaction().getEcdTransactionType());
            _returnDescriptorTransaction.setAdditionalEcdTransactionData(_performSubscriberTransfer__return.getDescriptor().getTransaction().getAdditionalEcdTransactionData());
            _returnDescriptor.setTransaction(_returnDescriptorTransaction);
            
            _return.setDescriptor(_returnDescriptor);
            
            ResultDescriptor _returnResult = new ResultDescriptor();
            _returnResult.setResultMessage(_performSubscriberTransfer__return.getResult().getResultMessage());
            _returnResult.seteCurrencyTransactionId(_performSubscriberTransfer__return.getResult().getECurrencyTransactionId());
            _returnResult.seteCurrencyTransactionTime(_performSubscriberTransfer__return.getResult().getECurrencyTransactionTime());
            _return.setResult(_returnResult);
			
			return _return;
		
		} catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
	}
	
}
