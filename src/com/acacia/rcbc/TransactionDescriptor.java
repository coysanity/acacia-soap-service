package com.acacia.rcbc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transactionDescriptor", propOrder = {
    "amount",
    "ecdTransactionId",
    "ecdTransactionTime",
    "additionalEcdTransactionData",
    "sourceLocation",
    "destinationLocation",
    "cid",
    "liqId",
    "ecdTransactionType"
})
public class TransactionDescriptor {
	
	@XmlElement(required = true)
    protected AmountDescriptor amount;
    @XmlElement(required = true)
    protected String ecdTransactionId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar ecdTransactionTime;
    protected String additionalEcdTransactionData;
    protected LocationDescriptor sourceLocation;
    protected LocationDescriptor destinationLocation;
    protected String cid;
    protected String liqId;
    protected String ecdTransactionType;
    
	public AmountDescriptor getAmount() {
		return amount;
	}
	
	public void setAmount(AmountDescriptor amount) {
		this.amount = amount;
	}
	
	public String getEcdTransactionId() {
		return ecdTransactionId;
	}
	
	public void setEcdTransactionId(String ecdTransactionId) {
		this.ecdTransactionId = ecdTransactionId;
	}
	
	public XMLGregorianCalendar getEcdTransactionTime() {
		return ecdTransactionTime;
	}
	
	public void setEcdTransactionTime(XMLGregorianCalendar ecdTransactionTime) {
		this.ecdTransactionTime = ecdTransactionTime;
	}
	
	public String getAdditionalEcdTransactionData() {
		return additionalEcdTransactionData;
	}
	public void setAdditionalEcdTransactionData(String additionalEcdTransactionData) {
		this.additionalEcdTransactionData = additionalEcdTransactionData;
	}
	
	public LocationDescriptor getSourceLocation() {
		return sourceLocation;
	}
	
	public void setSourceLocation(LocationDescriptor sourceLocation) {
		this.sourceLocation = sourceLocation;
	}
	
	public LocationDescriptor getDestinationLocation() {
		return destinationLocation;
	}
	
	public void setDestinationLocation(LocationDescriptor destinationLocation) {
		this.destinationLocation = destinationLocation;
	}
	
	public String getCid() {
		return cid;
	}
	
	public void setCid(String cid) {
		this.cid = cid;
	}
	
	public String getLiqId() {
		return liqId;
	}
	
	public void setLiqId(String liqId) {
		this.liqId = liqId;
	}
	
	public String getEcdTransactionType() {
		return ecdTransactionType;
	}
	
	public void setEcdTransactionType(String ecdTransactionType) {
		this.ecdTransactionType = ecdTransactionType;
	}
    

}
