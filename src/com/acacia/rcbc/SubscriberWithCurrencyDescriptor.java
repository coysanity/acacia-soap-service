package com.acacia.rcbc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "subscriberWithCurrencyDescriptor", propOrder = {
    "subscriber",
    "currencyVault"
})
public class SubscriberWithCurrencyDescriptor {
	
	@XmlElement(required = true)
    protected SubscriberDescriptor subscriber;
    protected CurrencyVaultDescriptor currencyVault;
    
	public SubscriberDescriptor getSubscriber() {
		return subscriber;
	}
	
	public void setSubscriber(SubscriberDescriptor subscriber) {
		this.subscriber = subscriber;
	}
	
	public CurrencyVaultDescriptor getCurrencyVault() {
		return currencyVault;
	}
	
	public void setCurrencyVault(CurrencyVaultDescriptor currencyVault) {
		this.currencyVault = currencyVault;
	}
    
    
	
	

}
