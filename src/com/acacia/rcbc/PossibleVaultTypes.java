package com.acacia.rcbc;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


@XmlType(name = "possibleVaultTypes")
@XmlEnum
public enum PossibleVaultTypes {
	
	ECDMST,
    LIQ,
    BRSG,
    SUB,
    MER,
    AGN,
    ESC;

    public String value() {
        return name();
    }

    public static PossibleVaultTypes fromValue(String v) {
        return valueOf(v);
    }

}
