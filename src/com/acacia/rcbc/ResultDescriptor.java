package com.acacia.rcbc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "resultDescriptor", propOrder = {
    "resultMessage",
    "eCurrencyTransactionId",
    "eCurrencyTransactionTime"
})
public class ResultDescriptor {
		
	protected String resultMessage;
    @XmlElement(required = true)
    protected String eCurrencyTransactionId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar eCurrencyTransactionTime;
    
	public String getResultMessage() {
		return resultMessage;
	}
	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}
	public String geteCurrencyTransactionId() {
		return eCurrencyTransactionId;
	}
	public void seteCurrencyTransactionId(String eCurrencyTransactionId) {
		this.eCurrencyTransactionId = eCurrencyTransactionId;
	}
	public XMLGregorianCalendar geteCurrencyTransactionTime() {
		return eCurrencyTransactionTime;
	}
	public void seteCurrencyTransactionTime(XMLGregorianCalendar eCurrencyTransactionTime) {
		this.eCurrencyTransactionTime = eCurrencyTransactionTime;
	}
    
    

}
