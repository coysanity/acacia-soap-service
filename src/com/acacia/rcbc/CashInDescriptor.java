package com.acacia.rcbc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cashInDescriptor", propOrder = {
    "sourceEcd",
    "destinationSubscriber",
    "transaction"
})
public class CashInDescriptor {
	
	@XmlElement(required = true)
    protected EcdWithCurrencyDescriptor sourceEcd;
    @XmlElement(required = true)
    protected SubscriberWithCurrencyDescriptor destinationSubscriber;
    @XmlElement(required = true)
    protected TransactionDescriptor transaction;
    
	public EcdWithCurrencyDescriptor getSourceEcd() {
		return sourceEcd;
	}
	
	public void setSourceEcd(EcdWithCurrencyDescriptor sourceEcd) {
		this.sourceEcd = sourceEcd;
	}
	
	public SubscriberWithCurrencyDescriptor getDestinationSubscriber() {
		return destinationSubscriber;
	}
	
	public void setDestinationSubscriber(SubscriberWithCurrencyDescriptor destinationSubscriber) {
		this.destinationSubscriber = destinationSubscriber;
	}

	public TransactionDescriptor getTransaction() {
		return transaction;
	}
	
	public void setTransaction(TransactionDescriptor transaction) {
		this.transaction = transaction;
	}
    
    
	

}
