package com.acacia.rcbc;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "amountDescriptor", propOrder = {
    "currencyCode",
    "currencyAmount"
})
public class AmountDescriptor {
	
	@XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected ISOCurrencyCodeType currencyCode;
    @XmlElement(required = true)
    protected BigDecimal currencyAmount;
    
	public ISOCurrencyCodeType getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(ISOCurrencyCodeType currencyCode) {
		this.currencyCode = currencyCode;
	}
	public BigDecimal getCurrencyAmount() {
		return currencyAmount;
	}
	public void setCurrencyAmount(BigDecimal currencyAmount) {
		this.currencyAmount = currencyAmount;
	}
    

}
