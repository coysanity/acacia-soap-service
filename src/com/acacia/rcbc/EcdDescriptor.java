package com.acacia.rcbc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ecdDescriptor", propOrder = {
    "cbID",
    "commercialBankID",
    "countryID",
    "ecdID",
    "partitionID"
})
public class EcdDescriptor {
	
	protected String cbID;
    protected String commercialBankID;
    @XmlSchemaType(name = "NMTOKEN")
    protected ISOCountryCodeType countryID;
    @XmlElement(required = true)
    protected String ecdID;
    protected String partitionID;
    
    public String getCbID() {
        return cbID;
    }
    
    public void setCbID(String value) {
        this.cbID = value;
    }
    
    public String getCommercialBankID() {
        return commercialBankID;
    }
    
    public void setCommercialBankID(String value) {
        this.commercialBankID = value;
    }
    
    public ISOCountryCodeType getCountryID() {
        return countryID;
    }
    
    public void setCountryID(ISOCountryCodeType value) {
        this.countryID = value;
    }
    
    public String getEcdID() {
        return ecdID;
    }
    
    public void setEcdID(String value) {
        this.ecdID = value;
    }
    
    public String getPartitionID() {
        return partitionID;
    }
    
    public void setPartitionID(String value) {
        this.partitionID = value;
    }

}
