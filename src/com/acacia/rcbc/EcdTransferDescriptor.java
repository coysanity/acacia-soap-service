package com.acacia.rcbc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ecdTransferDescriptor", propOrder = {
    "sourceEcd",
    "destinationEcd",
    "transaction"
})
public class EcdTransferDescriptor {

	@XmlElement(required = true)
    protected EcdWithCurrencyDescriptor sourceEcd;
    @XmlElement(required = true)
    protected EcdWithCurrencyDescriptor destinationEcd;
    @XmlElement(required = true)
    protected TransactionDescriptor transaction;
    
	public EcdWithCurrencyDescriptor getSourceEcd() {
		return sourceEcd;
	}
	
	public void setSourceEcd(EcdWithCurrencyDescriptor sourceEcd) {
		this.sourceEcd = sourceEcd;
	}
	
	public EcdWithCurrencyDescriptor getDestinationEcd() {
		return destinationEcd;
	}
	
	public void setDestinationEcd(EcdWithCurrencyDescriptor destinationEcd) {
		this.destinationEcd = destinationEcd;
	}
	
	public TransactionDescriptor getTransaction() {
		return transaction;
	}
	
	public void setTransaction(TransactionDescriptor transaction) {
		this.transaction = transaction;
	}
       	
}
