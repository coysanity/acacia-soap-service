package com.acacia.rcbc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addSubscriberRequest", propOrder = {
    "ecdHeader",
    "subscriber"
})
public class AddSubscriberRequest {
	
	@XmlElement(required = true, defaultValue = "")
    protected String ecdHeader;
    @XmlElement(required = true)
    protected SubscriberDescriptor subscriber;
    
    
    public String getEcdHeader() {
        return ecdHeader;
    }
    
    public void setEcdHeader(String value) {
        this.ecdHeader = value;
    }
    
    public SubscriberDescriptor getSubscriber() {
        return subscriber;
    }
    
    public void setSubscriber(SubscriberDescriptor value) {
        this.subscriber = value;
    }
	
}
