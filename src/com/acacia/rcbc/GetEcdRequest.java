package com.acacia.rcbc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getEcdRequest", propOrder = {
    "ecdHeader",
    "ecd",
    "vaultType"
})
public class GetEcdRequest {
	
	@XmlElement(required = true, defaultValue = "")
    protected String ecdHeader;
    @XmlElement(required = true)
    protected EcdDescriptor ecd;
    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected PossibleEcdVaultTypes vaultType;
    
	public String getEcdHeader() {
		return ecdHeader;
	}
	
	public void setEcdHeader(String ecdHeader) {
		this.ecdHeader = ecdHeader;
	}
	
	public EcdDescriptor getEcd() {
		return ecd;
	}
	
	public void setEcd(EcdDescriptor ecd) {
		this.ecd = ecd;
	}
	
	public PossibleEcdVaultTypes getVaultType() {
		return vaultType;
	}
	
	public void setVaultType(PossibleEcdVaultTypes vaultType) {
		this.vaultType = vaultType;
	}

}
