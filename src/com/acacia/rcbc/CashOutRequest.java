package com.acacia.rcbc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cashOutRequest", propOrder = {
    "ecdHeader",
    "descriptor"
})
public class CashOutRequest {
	
	@XmlElement(required = true, defaultValue = "")
    protected String ecdHeader;
    @XmlElement(required = true)
    protected CashOutDescriptor descriptor;
    
	public String getEcdHeader() {
		return ecdHeader;
	}
	
	public void setEcdHeader(String ecdHeader) {
		this.ecdHeader = ecdHeader;
	}
	
	public CashOutDescriptor getDescriptor() {
		return descriptor;
	}
	
	public void setDescriptor(CashOutDescriptor descriptor) {
		this.descriptor = descriptor;
	}
    
    

}
