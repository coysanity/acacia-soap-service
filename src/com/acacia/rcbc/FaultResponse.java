package com.acacia.rcbc;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "faultResponse", propOrder = {
    "ecdHeader",
    "faultItem",
    "eventTime"
})
public class FaultResponse {
	
	@XmlElement(required = true)
    protected String ecdHeader;
    @XmlElement(required = true)
    protected List<FaultDescriptor> faultItem;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar eventTime;
    
    public String getEcdHeader() {
        return ecdHeader;
    }
    
    public void setEcdHeader(String value) {
        this.ecdHeader = value;
    }
    
    

}
