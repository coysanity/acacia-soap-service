package com.acacia.rcbc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getSubscriberResponse", propOrder = {
    "ecdHeader",
    "subscriberWithCurrency",
    "result"
})
public class GetSubscriberResponse {

	@XmlElement(required = true)
    protected String ecdHeader;
    @XmlElement(required = true)
    protected SubscriberWithCurrencyDescriptor subscriberWithCurrency;
    @XmlElement(required = true)
    protected ResultDescriptor result;
    
	public String getEcdHeader() {
		return ecdHeader;
	}
	
	public void setEcdHeader(String ecdHeader) {
		this.ecdHeader = ecdHeader;
	}
	
	public SubscriberWithCurrencyDescriptor getSubscriberWithCurrency() {
		return subscriberWithCurrency;
	}
	
	public void setSubscriberWithCurrency(SubscriberWithCurrencyDescriptor subscriberWithCurrency) {
		this.subscriberWithCurrency = subscriberWithCurrency;
	}
	
	public ResultDescriptor getResult() {
		return result;
	}
	
	public void setResult(ResultDescriptor result) {
		this.result = result;
	}
    
	
}
