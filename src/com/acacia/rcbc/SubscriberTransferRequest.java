package com.acacia.rcbc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "subscriberTransferRequest", propOrder = {
    "ecdHeader",
    "descriptor"
})
public class SubscriberTransferRequest {
	
	@XmlElement(required = true, defaultValue = "")
    protected String ecdHeader;
    @XmlElement(required = true)
    protected SubscriberTransferDescriptor descriptor;
    
	public String getEcdHeader() {
		return ecdHeader;
	}
	
	public void setEcdHeader(String ecdHeader) {
		this.ecdHeader = ecdHeader;
	}
	
	public SubscriberTransferDescriptor getDescriptor() {
		return descriptor;
	}
	
	public void setDescriptor(SubscriberTransferDescriptor descriptor) {
		this.descriptor = descriptor;
	}
    
}
