package com.acacia.rcbc;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "possibleSubscriberStates")
@XmlEnum
public enum PossibleSubscriberStates {
	
	@XmlEnumValue("active")
    ACTIVE("active"),
    @XmlEnumValue("suspended")
    SUSPENDED("suspended"),
    @XmlEnumValue("blocked")
    BLOCKED("blocked");
    private final String value;
    
    PossibleSubscriberStates(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PossibleSubscriberStates fromValue(String v) {
        for (PossibleSubscriberStates c: PossibleSubscriberStates.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
