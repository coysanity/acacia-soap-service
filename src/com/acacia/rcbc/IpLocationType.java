package com.acacia.rcbc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ipLocationType", propOrder = {
    "ipType",
    "ip"
})
public class IpLocationType {
	
	@XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected PossibleIpTypes ipType;
    @XmlElement(required = true)
    protected String ip;
    
	public PossibleIpTypes getIpType() {
		return ipType;
	}
	
	public void setIpType(PossibleIpTypes ipType) {
		this.ipType = ipType;
	}
	
	public String getIp() {
		return ip;
	}
	
	public void setIp(String ip) {
		this.ip = ip;
	}
    
    

}
