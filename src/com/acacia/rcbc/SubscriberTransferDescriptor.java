package com.acacia.rcbc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "subscriberTransferDescriptor", propOrder = {
    "sourceSubscriber",
    "destinationSubscriber",
    "transaction"
})
public class SubscriberTransferDescriptor {
	
	@XmlElement(required = true)
    protected SubscriberWithCurrencyDescriptor sourceSubscriber;
    @XmlElement(required = true)
    protected SubscriberWithCurrencyDescriptor destinationSubscriber;
    @XmlElement(required = true)
    protected TransactionDescriptor transaction;
    
	public SubscriberWithCurrencyDescriptor getSourceSubscriber() {
		return sourceSubscriber;
	}
	
	public void setSourceSubscriber(SubscriberWithCurrencyDescriptor sourceSubscriber) {
		this.sourceSubscriber = sourceSubscriber;
	}
	
	public SubscriberWithCurrencyDescriptor getDestinationSubscriber() {
		return destinationSubscriber;
	}
	
	public void setDestinationSubscriber(SubscriberWithCurrencyDescriptor destinationSubscriber) {
		this.destinationSubscriber = destinationSubscriber;
	}
	
	public TransactionDescriptor getTransaction() {
		return transaction;
	}
	
	public void setTransaction(TransactionDescriptor transaction) {
		this.transaction = transaction;
	}
    
}
