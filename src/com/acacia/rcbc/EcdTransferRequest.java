package com.acacia.rcbc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ecdTransferRequest", propOrder = {
    "ecdHeader",
    "descriptor"
})
public class EcdTransferRequest {
	
	@XmlElement(required = true, defaultValue = "")
    protected String ecdHeader;
    @XmlElement(required = true)
    protected EcdTransferDescriptor descriptor;
    
    public String getEcdHeader() {
        return ecdHeader;
    }
    
    public void setEcdHeader(String value) {
        this.ecdHeader = value;
    }
    
    public EcdTransferDescriptor getDescriptor() {
        return descriptor;
    }

    public void setDescriptor(EcdTransferDescriptor value) {
        this.descriptor = value;
    }

}
