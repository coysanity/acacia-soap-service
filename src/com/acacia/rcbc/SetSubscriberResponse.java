package com.acacia.rcbc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "setSubscriberResponse", propOrder = {
    "ecdHeader",
    "subscriber",
    "result"
})
public class SetSubscriberResponse {

	@XmlElement(required = true)
    protected String ecdHeader;
    @XmlElement(required = true)
    protected SubscriberDescriptor subscriber;
    @XmlElement(required = true)
    protected ResultDescriptor result;
    
	public String getEcdHeader() {
		return ecdHeader;
	}
	
	public void setEcdHeader(String ecdHeader) {
		this.ecdHeader = ecdHeader;
	}
	
	public SubscriberDescriptor getSubscriber() {
		return subscriber;
	}
	
	public void setSubscriber(SubscriberDescriptor subscriber) {
		this.subscriber = subscriber;
	}
	
	public ResultDescriptor getResult() {
		return result;
	}
	
	public void setResult(ResultDescriptor result) {
		this.result = result;
	}
    
}
