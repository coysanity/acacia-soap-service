
package com.ecm.test.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * 
 * 				=====
 * 				transactionDescriptor:: is an 'abstract' encapsulation of data pertinent to a specific eCurrency transaction. This entity will never exist on its
 * 				own but rather is a 'partial' included in various transaction types. ecdTransactionId is an arbitrary string allowing the ECD to uniquely identify
 * 				this particular transaction. The ID will be persisted by eCMS in the historical transaction stream for audit and possible future retrieval.
 * 				ecdTransactionTime is the time of the transaction request as perceived by the ECD system. It is used by eCMS for audit purposes only. Resolution
 * 				should be a millisecond. Multiple transactions can have the same ecdTransactionTime additionalEcdTransactionData is an arbitrary string guaranteed to be
 * 				returned in the transaction response from the eCMS. It is not persisted by eCMS.
 * 				cid: correlation id for xECD transactions
 * 				liqId: liquidity provider id for xECD transactions
 * 				====
 * 			
 * 
 * <p>Java class for transactionDescriptor complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="transactionDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="amount" type="{urn:com:ecmhp}amountDescriptor"/&gt;
 *         &lt;element name="ecdTransactionId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ecdTransactionTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="additionalEcdTransactionData" type="{urn:com:ecmhp}ecdStringMaxLength32" minOccurs="0"/&gt;
 *         &lt;element name="sourceLocation" type="{urn:com:ecmhp}locationDescriptor" minOccurs="0"/&gt;
 *         &lt;element name="destinationLocation" type="{urn:com:ecmhp}locationDescriptor" minOccurs="0"/&gt;
 *         &lt;element name="cid" type="{urn:com:ecmhp}uuid" minOccurs="0"/&gt;
 *         &lt;element name="liqId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ecdTransactionType" type="{urn:com:ecmhp}ecdStringMaxLength32" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transactionDescriptor", propOrder = {
    "amount",
    "ecdTransactionId",
    "ecdTransactionTime",
    "additionalEcdTransactionData",
    "sourceLocation",
    "destinationLocation",
    "cid",
    "liqId",
    "ecdTransactionType"
})
public class TransactionDescriptor {

    @XmlElement(required = true)
    protected AmountDescriptor amount;
    @XmlElement(required = true)
    protected String ecdTransactionId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar ecdTransactionTime;
    protected String additionalEcdTransactionData;
    protected LocationDescriptor sourceLocation;
    protected LocationDescriptor destinationLocation;
    protected String cid;
    protected String liqId;
    protected String ecdTransactionType;

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link AmountDescriptor }
     *     
     */
    public AmountDescriptor getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountDescriptor }
     *     
     */
    public void setAmount(AmountDescriptor value) {
        this.amount = value;
    }

    /**
     * Gets the value of the ecdTransactionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEcdTransactionId() {
        return ecdTransactionId;
    }

    /**
     * Sets the value of the ecdTransactionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEcdTransactionId(String value) {
        this.ecdTransactionId = value;
    }

    /**
     * Gets the value of the ecdTransactionTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEcdTransactionTime() {
        return ecdTransactionTime;
    }

    /**
     * Sets the value of the ecdTransactionTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEcdTransactionTime(XMLGregorianCalendar value) {
        this.ecdTransactionTime = value;
    }

    /**
     * Gets the value of the additionalEcdTransactionData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdditionalEcdTransactionData() {
        return additionalEcdTransactionData;
    }

    /**
     * Sets the value of the additionalEcdTransactionData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdditionalEcdTransactionData(String value) {
        this.additionalEcdTransactionData = value;
    }

    /**
     * Gets the value of the sourceLocation property.
     * 
     * @return
     *     possible object is
     *     {@link LocationDescriptor }
     *     
     */
    public LocationDescriptor getSourceLocation() {
        return sourceLocation;
    }

    /**
     * Sets the value of the sourceLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationDescriptor }
     *     
     */
    public void setSourceLocation(LocationDescriptor value) {
        this.sourceLocation = value;
    }

    /**
     * Gets the value of the destinationLocation property.
     * 
     * @return
     *     possible object is
     *     {@link LocationDescriptor }
     *     
     */
    public LocationDescriptor getDestinationLocation() {
        return destinationLocation;
    }

    /**
     * Sets the value of the destinationLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationDescriptor }
     *     
     */
    public void setDestinationLocation(LocationDescriptor value) {
        this.destinationLocation = value;
    }

    /**
     * Gets the value of the cid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCid() {
        return cid;
    }

    /**
     * Sets the value of the cid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCid(String value) {
        this.cid = value;
    }

    /**
     * Gets the value of the liqId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLiqId() {
        return liqId;
    }

    /**
     * Sets the value of the liqId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLiqId(String value) {
        this.liqId = value;
    }

    /**
     * Gets the value of the ecdTransactionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEcdTransactionType() {
        return ecdTransactionType;
    }

    /**
     * Sets the value of the ecdTransactionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEcdTransactionType(String value) {
        this.ecdTransactionType = value;
    }

}
