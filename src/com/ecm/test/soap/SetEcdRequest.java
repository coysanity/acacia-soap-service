
package com.ecm.test.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for setEcdRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="setEcdRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="header" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="prevEcdWithCurrency" type="{urn:com:ecmhp}ecdWithCurrencyDescriptor"/&gt;
 *         &lt;element name="newEcdWithCurrency" type="{urn:com:ecmhp}ecdWithCurrencyDescriptor"/&gt;
 *         &lt;element name="transaction" type="{urn:com:ecmhp}transactionDescriptor"/&gt;
 *         &lt;element name="cause" type="{urn:com:ecmhp}possibleNotificationCauses"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "setEcdRequest", propOrder = {
    "header",
    "prevEcdWithCurrency",
    "newEcdWithCurrency",
    "transaction",
    "cause"
})
public class SetEcdRequest {

    protected String header;
    @XmlElement(required = true)
    protected EcdWithCurrencyDescriptor prevEcdWithCurrency;
    @XmlElement(required = true)
    protected EcdWithCurrencyDescriptor newEcdWithCurrency;
    @XmlElement(required = true)
    protected TransactionDescriptor transaction;
    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected PossibleNotificationCauses cause;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHeader(String value) {
        this.header = value;
    }

    /**
     * Gets the value of the prevEcdWithCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link EcdWithCurrencyDescriptor }
     *     
     */
    public EcdWithCurrencyDescriptor getPrevEcdWithCurrency() {
        return prevEcdWithCurrency;
    }

    /**
     * Sets the value of the prevEcdWithCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link EcdWithCurrencyDescriptor }
     *     
     */
    public void setPrevEcdWithCurrency(EcdWithCurrencyDescriptor value) {
        this.prevEcdWithCurrency = value;
    }

    /**
     * Gets the value of the newEcdWithCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link EcdWithCurrencyDescriptor }
     *     
     */
    public EcdWithCurrencyDescriptor getNewEcdWithCurrency() {
        return newEcdWithCurrency;
    }

    /**
     * Sets the value of the newEcdWithCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link EcdWithCurrencyDescriptor }
     *     
     */
    public void setNewEcdWithCurrency(EcdWithCurrencyDescriptor value) {
        this.newEcdWithCurrency = value;
    }

    /**
     * Gets the value of the transaction property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionDescriptor }
     *     
     */
    public TransactionDescriptor getTransaction() {
        return transaction;
    }

    /**
     * Sets the value of the transaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionDescriptor }
     *     
     */
    public void setTransaction(TransactionDescriptor value) {
        this.transaction = value;
    }

    /**
     * Gets the value of the cause property.
     * 
     * @return
     *     possible object is
     *     {@link PossibleNotificationCauses }
     *     
     */
    public PossibleNotificationCauses getCause() {
        return cause;
    }

    /**
     * Sets the value of the cause property.
     * 
     * @param value
     *     allowed object is
     *     {@link PossibleNotificationCauses }
     *     
     */
    public void setCause(PossibleNotificationCauses value) {
        this.cause = value;
    }

}
