
package com.ecm.test.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				=====
 * 				ecdWithCurrencyDescriptor:: Ecd with their eCurrency vault. The amount of currency
 * 				available for ECD to distribute to the subscribers.
 * 				====
 * 			
 * 
 * <p>Java class for ecdWithCurrencyDescriptor complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ecdWithCurrencyDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ecd" type="{urn:com:ecmhp}ecdDescriptor"/&gt;
 *         &lt;element name="currencyVault" type="{urn:com:ecmhp}currencyVaultDescriptor" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ecdWithCurrencyDescriptor", propOrder = {
    "ecd",
    "currencyVault"
})
public class EcdWithCurrencyDescriptor {

    @XmlElement(required = true)
    protected EcdDescriptor ecd;
    protected CurrencyVaultDescriptor currencyVault;

    /**
     * Gets the value of the ecd property.
     * 
     * @return
     *     possible object is
     *     {@link EcdDescriptor }
     *     
     */
    public EcdDescriptor getEcd() {
        return ecd;
    }

    /**
     * Sets the value of the ecd property.
     * 
     * @param value
     *     allowed object is
     *     {@link EcdDescriptor }
     *     
     */
    public void setEcd(EcdDescriptor value) {
        this.ecd = value;
    }

    /**
     * Gets the value of the currencyVault property.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyVaultDescriptor }
     *     
     */
    public CurrencyVaultDescriptor getCurrencyVault() {
        return currencyVault;
    }

    /**
     * Sets the value of the currencyVault property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyVaultDescriptor }
     *     
     */
    public void setCurrencyVault(CurrencyVaultDescriptor value) {
        this.currencyVault = value;
    }

}
