
package com.ecm.test.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for transferGroupRequestEntry complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="transferGroupRequestEntry"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="groupEntryType" type="{urn:com:ecmhp}possibleTransferGroupEntryTypes"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="subscriberTransferRequest" type="{urn:com:ecmhp}subscriberTransferRequest"/&gt;
 *           &lt;element name="cashInRequest" type="{urn:com:ecmhp}cashInRequest"/&gt;
 *           &lt;element name="cashOutRequest" type="{urn:com:ecmhp}cashOutRequest"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transferGroupRequestEntry", propOrder = {
    "groupEntryType",
    "subscriberTransferRequest",
    "cashInRequest",
    "cashOutRequest"
})
public class TransferGroupRequestEntry {

    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected PossibleTransferGroupEntryTypes groupEntryType;
    protected SubscriberTransferRequest subscriberTransferRequest;
    protected CashInRequest cashInRequest;
    protected CashOutRequest cashOutRequest;

    /**
     * Gets the value of the groupEntryType property.
     * 
     * @return
     *     possible object is
     *     {@link PossibleTransferGroupEntryTypes }
     *     
     */
    public PossibleTransferGroupEntryTypes getGroupEntryType() {
        return groupEntryType;
    }

    /**
     * Sets the value of the groupEntryType property.
     * 
     * @param value
     *     allowed object is
     *     {@link PossibleTransferGroupEntryTypes }
     *     
     */
    public void setGroupEntryType(PossibleTransferGroupEntryTypes value) {
        this.groupEntryType = value;
    }

    /**
     * Gets the value of the subscriberTransferRequest property.
     * 
     * @return
     *     possible object is
     *     {@link SubscriberTransferRequest }
     *     
     */
    public SubscriberTransferRequest getSubscriberTransferRequest() {
        return subscriberTransferRequest;
    }

    /**
     * Sets the value of the subscriberTransferRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubscriberTransferRequest }
     *     
     */
    public void setSubscriberTransferRequest(SubscriberTransferRequest value) {
        this.subscriberTransferRequest = value;
    }

    /**
     * Gets the value of the cashInRequest property.
     * 
     * @return
     *     possible object is
     *     {@link CashInRequest }
     *     
     */
    public CashInRequest getCashInRequest() {
        return cashInRequest;
    }

    /**
     * Sets the value of the cashInRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link CashInRequest }
     *     
     */
    public void setCashInRequest(CashInRequest value) {
        this.cashInRequest = value;
    }

    /**
     * Gets the value of the cashOutRequest property.
     * 
     * @return
     *     possible object is
     *     {@link CashOutRequest }
     *     
     */
    public CashOutRequest getCashOutRequest() {
        return cashOutRequest;
    }

    /**
     * Sets the value of the cashOutRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link CashOutRequest }
     *     
     */
    public void setCashOutRequest(CashOutRequest value) {
        this.cashOutRequest = value;
    }

}
