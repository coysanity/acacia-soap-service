
package com.ecm.test.soap;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for transferGroupResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="transferGroupResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ecdHeader" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ecdGroupId" type="{urn:com:ecmhp}ecdStringMaxLength32" minOccurs="0"/&gt;
 *         &lt;element name="eCurrencyGroupId" type="{urn:com:ecmhp}uuid"/&gt;
 *         &lt;element name="responseTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="groupResponseEntry" type="{urn:com:ecmhp}transferGroupResponseEntry" maxOccurs="10"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transferGroupResponse", propOrder = {
    "ecdHeader",
    "ecdGroupId",
    "eCurrencyGroupId",
    "responseTime",
    "groupResponseEntry"
})
public class TransferGroupResponse {

    protected String ecdHeader;
    protected String ecdGroupId;
    @XmlElement(required = true)
    protected String eCurrencyGroupId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar responseTime;
    @XmlElement(required = true)
    protected List<TransferGroupResponseEntry> groupResponseEntry;

    /**
     * Gets the value of the ecdHeader property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEcdHeader() {
        return ecdHeader;
    }

    /**
     * Sets the value of the ecdHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEcdHeader(String value) {
        this.ecdHeader = value;
    }

    /**
     * Gets the value of the ecdGroupId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEcdGroupId() {
        return ecdGroupId;
    }

    /**
     * Sets the value of the ecdGroupId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEcdGroupId(String value) {
        this.ecdGroupId = value;
    }

    /**
     * Gets the value of the eCurrencyGroupId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getECurrencyGroupId() {
        return eCurrencyGroupId;
    }

    /**
     * Sets the value of the eCurrencyGroupId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setECurrencyGroupId(String value) {
        this.eCurrencyGroupId = value;
    }

    /**
     * Gets the value of the responseTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getResponseTime() {
        return responseTime;
    }

    /**
     * Sets the value of the responseTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setResponseTime(XMLGregorianCalendar value) {
        this.responseTime = value;
    }

    /**
     * Gets the value of the groupResponseEntry property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the groupResponseEntry property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGroupResponseEntry().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TransferGroupResponseEntry }
     * 
     * 
     */
    public List<TransferGroupResponseEntry> getGroupResponseEntry() {
        if (groupResponseEntry == null) {
            groupResponseEntry = new ArrayList<TransferGroupResponseEntry>();
        }
        return this.groupResponseEntry;
    }

}
