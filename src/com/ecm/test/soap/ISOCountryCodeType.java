
package com.ecm.test.soap;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ISOCountryCodeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ISOCountryCodeType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="AD"/&gt;
 *     &lt;enumeration value="AE"/&gt;
 *     &lt;enumeration value="AF"/&gt;
 *     &lt;enumeration value="AG"/&gt;
 *     &lt;enumeration value="AI"/&gt;
 *     &lt;enumeration value="AL"/&gt;
 *     &lt;enumeration value="AM"/&gt;
 *     &lt;enumeration value="AN"/&gt;
 *     &lt;enumeration value="AO"/&gt;
 *     &lt;enumeration value="AQ"/&gt;
 *     &lt;enumeration value="AR"/&gt;
 *     &lt;enumeration value="AS"/&gt;
 *     &lt;enumeration value="AT"/&gt;
 *     &lt;enumeration value="AU"/&gt;
 *     &lt;enumeration value="AW"/&gt;
 *     &lt;enumeration value="AX"/&gt;
 *     &lt;enumeration value="AZ"/&gt;
 *     &lt;enumeration value="BA"/&gt;
 *     &lt;enumeration value="BB"/&gt;
 *     &lt;enumeration value="BD"/&gt;
 *     &lt;enumeration value="BE"/&gt;
 *     &lt;enumeration value="BF"/&gt;
 *     &lt;enumeration value="BG"/&gt;
 *     &lt;enumeration value="BH"/&gt;
 *     &lt;enumeration value="BI"/&gt;
 *     &lt;enumeration value="BJ"/&gt;
 *     &lt;enumeration value="BL"/&gt;
 *     &lt;enumeration value="BM"/&gt;
 *     &lt;enumeration value="BN"/&gt;
 *     &lt;enumeration value="BO"/&gt;
 *     &lt;enumeration value="BR"/&gt;
 *     &lt;enumeration value="BS"/&gt;
 *     &lt;enumeration value="BT"/&gt;
 *     &lt;enumeration value="BV"/&gt;
 *     &lt;enumeration value="BW"/&gt;
 *     &lt;enumeration value="BY"/&gt;
 *     &lt;enumeration value="BZ"/&gt;
 *     &lt;enumeration value="CA"/&gt;
 *     &lt;enumeration value="CC"/&gt;
 *     &lt;enumeration value="CD"/&gt;
 *     &lt;enumeration value="CF"/&gt;
 *     &lt;enumeration value="CG"/&gt;
 *     &lt;enumeration value="CH"/&gt;
 *     &lt;enumeration value="CI"/&gt;
 *     &lt;enumeration value="CK"/&gt;
 *     &lt;enumeration value="CL"/&gt;
 *     &lt;enumeration value="CM"/&gt;
 *     &lt;enumeration value="CN"/&gt;
 *     &lt;enumeration value="CO"/&gt;
 *     &lt;enumeration value="CR"/&gt;
 *     &lt;enumeration value="CU"/&gt;
 *     &lt;enumeration value="CV"/&gt;
 *     &lt;enumeration value="CX"/&gt;
 *     &lt;enumeration value="CY"/&gt;
 *     &lt;enumeration value="CZ"/&gt;
 *     &lt;enumeration value="DE"/&gt;
 *     &lt;enumeration value="DJ"/&gt;
 *     &lt;enumeration value="DK"/&gt;
 *     &lt;enumeration value="DM"/&gt;
 *     &lt;enumeration value="DO"/&gt;
 *     &lt;enumeration value="DZ"/&gt;
 *     &lt;enumeration value="EC"/&gt;
 *     &lt;enumeration value="EE"/&gt;
 *     &lt;enumeration value="EG"/&gt;
 *     &lt;enumeration value="EH"/&gt;
 *     &lt;enumeration value="ER"/&gt;
 *     &lt;enumeration value="ES"/&gt;
 *     &lt;enumeration value="ET"/&gt;
 *     &lt;enumeration value="FI"/&gt;
 *     &lt;enumeration value="FJ"/&gt;
 *     &lt;enumeration value="FK"/&gt;
 *     &lt;enumeration value="FM"/&gt;
 *     &lt;enumeration value="FO"/&gt;
 *     &lt;enumeration value="FR"/&gt;
 *     &lt;enumeration value="GA"/&gt;
 *     &lt;enumeration value="GB"/&gt;
 *     &lt;enumeration value="GD"/&gt;
 *     &lt;enumeration value="GE"/&gt;
 *     &lt;enumeration value="GF"/&gt;
 *     &lt;enumeration value="GG"/&gt;
 *     &lt;enumeration value="GH"/&gt;
 *     &lt;enumeration value="GI"/&gt;
 *     &lt;enumeration value="GL"/&gt;
 *     &lt;enumeration value="GM"/&gt;
 *     &lt;enumeration value="GN"/&gt;
 *     &lt;enumeration value="GP"/&gt;
 *     &lt;enumeration value="GQ"/&gt;
 *     &lt;enumeration value="GR"/&gt;
 *     &lt;enumeration value="GS"/&gt;
 *     &lt;enumeration value="GT"/&gt;
 *     &lt;enumeration value="GU"/&gt;
 *     &lt;enumeration value="GW"/&gt;
 *     &lt;enumeration value="GY"/&gt;
 *     &lt;enumeration value="HK"/&gt;
 *     &lt;enumeration value="HM"/&gt;
 *     &lt;enumeration value="HN"/&gt;
 *     &lt;enumeration value="HR"/&gt;
 *     &lt;enumeration value="HT"/&gt;
 *     &lt;enumeration value="HU"/&gt;
 *     &lt;enumeration value="ID"/&gt;
 *     &lt;enumeration value="IE"/&gt;
 *     &lt;enumeration value="IL"/&gt;
 *     &lt;enumeration value="IM"/&gt;
 *     &lt;enumeration value="IN"/&gt;
 *     &lt;enumeration value="IO"/&gt;
 *     &lt;enumeration value="IQ"/&gt;
 *     &lt;enumeration value="IR"/&gt;
 *     &lt;enumeration value="IS"/&gt;
 *     &lt;enumeration value="IT"/&gt;
 *     &lt;enumeration value="JE"/&gt;
 *     &lt;enumeration value="JM"/&gt;
 *     &lt;enumeration value="JO"/&gt;
 *     &lt;enumeration value="JP"/&gt;
 *     &lt;enumeration value="KE"/&gt;
 *     &lt;enumeration value="KG"/&gt;
 *     &lt;enumeration value="KH"/&gt;
 *     &lt;enumeration value="KI"/&gt;
 *     &lt;enumeration value="KM"/&gt;
 *     &lt;enumeration value="KN"/&gt;
 *     &lt;enumeration value="KP"/&gt;
 *     &lt;enumeration value="KR"/&gt;
 *     &lt;enumeration value="KW"/&gt;
 *     &lt;enumeration value="KY"/&gt;
 *     &lt;enumeration value="KZ"/&gt;
 *     &lt;enumeration value="LA"/&gt;
 *     &lt;enumeration value="LB"/&gt;
 *     &lt;enumeration value="LC"/&gt;
 *     &lt;enumeration value="LI"/&gt;
 *     &lt;enumeration value="LK"/&gt;
 *     &lt;enumeration value="LR"/&gt;
 *     &lt;enumeration value="LS"/&gt;
 *     &lt;enumeration value="LT"/&gt;
 *     &lt;enumeration value="LU"/&gt;
 *     &lt;enumeration value="LV"/&gt;
 *     &lt;enumeration value="LY"/&gt;
 *     &lt;enumeration value="MA"/&gt;
 *     &lt;enumeration value="MC"/&gt;
 *     &lt;enumeration value="MD"/&gt;
 *     &lt;enumeration value="ME"/&gt;
 *     &lt;enumeration value="MF"/&gt;
 *     &lt;enumeration value="MG"/&gt;
 *     &lt;enumeration value="MH"/&gt;
 *     &lt;enumeration value="MK"/&gt;
 *     &lt;enumeration value="ML"/&gt;
 *     &lt;enumeration value="MM"/&gt;
 *     &lt;enumeration value="MN"/&gt;
 *     &lt;enumeration value="MO"/&gt;
 *     &lt;enumeration value="MP"/&gt;
 *     &lt;enumeration value="MQ"/&gt;
 *     &lt;enumeration value="MR"/&gt;
 *     &lt;enumeration value="MS"/&gt;
 *     &lt;enumeration value="MT"/&gt;
 *     &lt;enumeration value="MU"/&gt;
 *     &lt;enumeration value="MV"/&gt;
 *     &lt;enumeration value="MW"/&gt;
 *     &lt;enumeration value="MX"/&gt;
 *     &lt;enumeration value="MY"/&gt;
 *     &lt;enumeration value="MZ"/&gt;
 *     &lt;enumeration value="NA"/&gt;
 *     &lt;enumeration value="NC"/&gt;
 *     &lt;enumeration value="NE"/&gt;
 *     &lt;enumeration value="NF"/&gt;
 *     &lt;enumeration value="NG"/&gt;
 *     &lt;enumeration value="NI"/&gt;
 *     &lt;enumeration value="NL"/&gt;
 *     &lt;enumeration value="NO"/&gt;
 *     &lt;enumeration value="NP"/&gt;
 *     &lt;enumeration value="NR"/&gt;
 *     &lt;enumeration value="NU"/&gt;
 *     &lt;enumeration value="NZ"/&gt;
 *     &lt;enumeration value="OM"/&gt;
 *     &lt;enumeration value="PA"/&gt;
 *     &lt;enumeration value="PE"/&gt;
 *     &lt;enumeration value="PF"/&gt;
 *     &lt;enumeration value="PG"/&gt;
 *     &lt;enumeration value="PH"/&gt;
 *     &lt;enumeration value="PK"/&gt;
 *     &lt;enumeration value="PL"/&gt;
 *     &lt;enumeration value="PM"/&gt;
 *     &lt;enumeration value="PN"/&gt;
 *     &lt;enumeration value="PR"/&gt;
 *     &lt;enumeration value="PS"/&gt;
 *     &lt;enumeration value="PT"/&gt;
 *     &lt;enumeration value="PW"/&gt;
 *     &lt;enumeration value="PY"/&gt;
 *     &lt;enumeration value="QA"/&gt;
 *     &lt;enumeration value="RE"/&gt;
 *     &lt;enumeration value="RO"/&gt;
 *     &lt;enumeration value="RS"/&gt;
 *     &lt;enumeration value="RU"/&gt;
 *     &lt;enumeration value="RW"/&gt;
 *     &lt;enumeration value="SA"/&gt;
 *     &lt;enumeration value="SB"/&gt;
 *     &lt;enumeration value="SC"/&gt;
 *     &lt;enumeration value="SD"/&gt;
 *     &lt;enumeration value="SE"/&gt;
 *     &lt;enumeration value="SG"/&gt;
 *     &lt;enumeration value="SH"/&gt;
 *     &lt;enumeration value="SI"/&gt;
 *     &lt;enumeration value="SJ"/&gt;
 *     &lt;enumeration value="SK"/&gt;
 *     &lt;enumeration value="SL"/&gt;
 *     &lt;enumeration value="SM"/&gt;
 *     &lt;enumeration value="SN"/&gt;
 *     &lt;enumeration value="SO"/&gt;
 *     &lt;enumeration value="SR"/&gt;
 *     &lt;enumeration value="ST"/&gt;
 *     &lt;enumeration value="SV"/&gt;
 *     &lt;enumeration value="SY"/&gt;
 *     &lt;enumeration value="SZ"/&gt;
 *     &lt;enumeration value="TC"/&gt;
 *     &lt;enumeration value="TD"/&gt;
 *     &lt;enumeration value="TF"/&gt;
 *     &lt;enumeration value="TG"/&gt;
 *     &lt;enumeration value="TH"/&gt;
 *     &lt;enumeration value="TJ"/&gt;
 *     &lt;enumeration value="TK"/&gt;
 *     &lt;enumeration value="TL"/&gt;
 *     &lt;enumeration value="TM"/&gt;
 *     &lt;enumeration value="TN"/&gt;
 *     &lt;enumeration value="TO"/&gt;
 *     &lt;enumeration value="TR"/&gt;
 *     &lt;enumeration value="TT"/&gt;
 *     &lt;enumeration value="TV"/&gt;
 *     &lt;enumeration value="TW"/&gt;
 *     &lt;enumeration value="TZ"/&gt;
 *     &lt;enumeration value="UA"/&gt;
 *     &lt;enumeration value="UG"/&gt;
 *     &lt;enumeration value="UM"/&gt;
 *     &lt;enumeration value="US"/&gt;
 *     &lt;enumeration value="UY"/&gt;
 *     &lt;enumeration value="UZ"/&gt;
 *     &lt;enumeration value="VA"/&gt;
 *     &lt;enumeration value="VC"/&gt;
 *     &lt;enumeration value="VE"/&gt;
 *     &lt;enumeration value="VG"/&gt;
 *     &lt;enumeration value="VI"/&gt;
 *     &lt;enumeration value="VN"/&gt;
 *     &lt;enumeration value="VU"/&gt;
 *     &lt;enumeration value="WF"/&gt;
 *     &lt;enumeration value="WS"/&gt;
 *     &lt;enumeration value="YE"/&gt;
 *     &lt;enumeration value="YT"/&gt;
 *     &lt;enumeration value="ZA"/&gt;
 *     &lt;enumeration value="ZM"/&gt;
 *     &lt;enumeration value="ZW"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ISOCountryCodeType")
@XmlEnum
public enum ISOCountryCodeType {


    /**
     * ANDORRA
     * 
     */
    AD,

    /**
     * UNITED ARAB EMIRATES
     * 
     */
    AE,

    /**
     * AFGHANISTAN
     * 
     */
    AF,

    /**
     * ANTIGUA AND BARBUDA
     * 
     */
    AG,

    /**
     * ANGUILLA
     * 
     */
    AI,

    /**
     * ALBANIA
     * 
     */
    AL,

    /**
     * ARMENIA
     * 
     */
    AM,

    /**
     * NETHERLANDS ANTILLES
     * 
     */
    AN,

    /**
     * ANGOLA
     * 
     */
    AO,

    /**
     * ANTARCTICA
     * 
     */
    AQ,

    /**
     * ARGENTINA
     * 
     */
    AR,

    /**
     * AMERICAN SAMOA
     * 
     */
    AS,

    /**
     * AUSTRIA
     * 
     */
    AT,

    /**
     * AUSTRALIA
     * 
     */
    AU,

    /**
     * ARUBA
     * 
     */
    AW,

    /**
     * ÅLAND ISLANDS
     * 
     */
    AX,

    /**
     * AZERBAIJAN
     * 
     */
    AZ,

    /**
     * BOSNIA AND HERZEGOVINA
     * 
     */
    BA,

    /**
     * BARBADOS
     * 
     */
    BB,

    /**
     * BANGLADESH
     * 
     */
    BD,

    /**
     * BELGIUM
     * 
     */
    BE,

    /**
     * BURKINA FASO
     * 
     */
    BF,

    /**
     * BULGARIA
     * 
     */
    BG,

    /**
     * BAHRAIN
     * 
     */
    BH,

    /**
     * BURUNDI
     * 
     */
    BI,

    /**
     * BENIN
     * 
     */
    BJ,

    /**
     * SAINT BARTH�LEMY
     * 
     */
    BL,

    /**
     * BERMUDA
     * 
     */
    BM,

    /**
     * BRUNEI DARUSSALAM
     * 
     */
    BN,

    /**
     * BOLIVIA
     * 
     */
    BO,

    /**
     * BRAZIL
     * 
     */
    BR,

    /**
     * BAHAMAS
     * 
     */
    BS,

    /**
     * BHUTAN
     * 
     */
    BT,

    /**
     * BOUVET ISLAND
     * 
     */
    BV,

    /**
     * BOTSWANA
     * 
     */
    BW,

    /**
     * BELARUS
     * 
     */
    BY,

    /**
     * BELIZE
     * 
     */
    BZ,

    /**
     * CANADA
     * 
     */
    CA,

    /**
     * COCOS (KEELING) ISLANDS
     * 
     */
    CC,

    /**
     * CONGO, THE DEMOCRATIC REPUBLIC OF THE
     * 
     */
    CD,

    /**
     * CENTRAL AFRICAN REPUBLIC
     * 
     */
    CF,

    /**
     * CONGO
     * 
     */
    CG,

    /**
     * SWITZERLAND
     * 
     */
    CH,

    /**
     * COTE D'IVOIRE
     * 
     */
    CI,

    /**
     * COOK ISLANDS
     * 
     */
    CK,

    /**
     * CHILE
     * 
     */
    CL,

    /**
     * CAMEROON
     * 
     */
    CM,

    /**
     * CHINA
     * 
     */
    CN,

    /**
     * COLOMBIA
     * 
     */
    CO,

    /**
     * COSTA RICA
     * 
     */
    CR,

    /**
     * CUBA
     * 
     */
    CU,

    /**
     * CAPE VERDE
     * 
     */
    CV,

    /**
     * CHRISTMAS ISLAND
     * 
     */
    CX,

    /**
     * CYPRUS
     * 
     */
    CY,

    /**
     * CZECH REPUBLIC
     * 
     */
    CZ,

    /**
     * GERMANY
     * 
     */
    DE,

    /**
     * DJIBOUTI
     * 
     */
    DJ,

    /**
     * DENMARK
     * 
     */
    DK,

    /**
     * DOMINICA
     * 
     */
    DM,

    /**
     * DOMINICAN REPUBLIC
     * 
     */
    DO,

    /**
     * ALGERIA
     * 
     */
    DZ,

    /**
     * ECUADOR
     * 
     */
    EC,

    /**
     * ESTONIA
     * 
     */
    EE,

    /**
     * EGYPT
     * 
     */
    EG,

    /**
     * WESTERN SAHARA
     * 
     */
    EH,

    /**
     * ERITREA
     * 
     */
    ER,

    /**
     * SPAIN
     * 
     */
    ES,

    /**
     * ETHIOPIA
     * 
     */
    ET,

    /**
     * FINLAND
     * 
     */
    FI,

    /**
     * FIJI
     * 
     */
    FJ,

    /**
     * FALKLAND ISLANDS (MALVINAS)
     * 
     */
    FK,

    /**
     * MICRONESIA, FEDERATED STATES OF
     * 
     */
    FM,

    /**
     * FAROE ISLANDS
     * 
     */
    FO,

    /**
     * FRANCE
     * 
     */
    FR,

    /**
     * GABON
     * 
     */
    GA,

    /**
     * UNITED KINGDOM
     * 
     */
    GB,

    /**
     * GRENADA
     * 
     */
    GD,

    /**
     * GEORGIA
     * 
     */
    GE,

    /**
     * FRENCH GUIANA
     * 
     */
    GF,

    /**
     * GUERNSEY
     * 
     */
    GG,

    /**
     * GHANA
     * 
     */
    GH,

    /**
     * GIBRALTAR
     * 
     */
    GI,

    /**
     * GREENLAND
     * 
     */
    GL,

    /**
     * GAMBIA
     * 
     */
    GM,

    /**
     * GUINEA
     * 
     */
    GN,

    /**
     * GUADELOUPE
     * 
     */
    GP,

    /**
     * EQUATORIAL GUINEA
     * 
     */
    GQ,

    /**
     * GREECE
     * 
     */
    GR,

    /**
     * SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS
     * 
     */
    GS,

    /**
     * GUATEMALA
     * 
     */
    GT,

    /**
     * GUAM
     * 
     */
    GU,

    /**
     * GUINEA-BISSAU
     * 
     */
    GW,

    /**
     * GUYANA
     * 
     */
    GY,

    /**
     * HONG KONG
     * 
     */
    HK,

    /**
     * HEARD ISLAND AND MCDONALD ISLANDS
     * 
     */
    HM,

    /**
     * HONDURAS
     * 
     */
    HN,

    /**
     * CROATIA
     * 
     */
    HR,

    /**
     * HAITI
     * 
     */
    HT,

    /**
     * HUNGARY
     * 
     */
    HU,

    /**
     * INDONESIA
     * 
     */
    ID,

    /**
     * IRELAND
     * 
     */
    IE,

    /**
     * ISRAEL
     * 
     */
    IL,

    /**
     * ISLE OF MAN
     * 
     */
    IM,

    /**
     * INDIA
     * 
     */
    IN,

    /**
     * BRITISH INDIAN OCEAN TERRITORY
     * 
     */
    IO,

    /**
     * IRAQ
     * 
     */
    IQ,

    /**
     * IRAN, ISLAMIC REPUBLIC OF
     * 
     */
    IR,

    /**
     * ICELAND
     * 
     */
    IS,

    /**
     * ITALY
     * 
     */
    IT,

    /**
     * JERSEY
     * 
     */
    JE,

    /**
     * JAMAICA
     * 
     */
    JM,

    /**
     * JORDAN
     * 
     */
    JO,

    /**
     * JAPAN
     * 
     */
    JP,

    /**
     * KENYA
     * 
     */
    KE,

    /**
     * KYRGYZSTAN
     * 
     */
    KG,

    /**
     * CAMBODIA
     * 
     */
    KH,

    /**
     * KIRIBATI
     * 
     */
    KI,

    /**
     * COMOROS
     * 
     */
    KM,

    /**
     * SAINT KITTS AND NEVIS
     * 
     */
    KN,

    /**
     * KOREA, DEMOCRATIC PEOPLE'S REPUBLIC OF
     * 
     */
    KP,

    /**
     * KOREA, REPUBLIC OF
     * 
     */
    KR,

    /**
     * KUWAIT
     * 
     */
    KW,

    /**
     * CAYMAN ISLANDS
     * 
     */
    KY,

    /**
     * KAZAKHSTAN
     * 
     */
    KZ,

    /**
     * LAO PEOPLE'S DEMOCRATIC REPUBLIC
     * 
     */
    LA,

    /**
     * LEBANON
     * 
     */
    LB,

    /**
     * SAINT LUCIA
     * 
     */
    LC,

    /**
     * LIECHTENSTEIN
     * 
     */
    LI,

    /**
     * SRI LANKA
     * 
     */
    LK,

    /**
     * LIBERIA
     * 
     */
    LR,

    /**
     * LESOTHO
     * 
     */
    LS,

    /**
     * LITHUANIA
     * 
     */
    LT,

    /**
     * LUXEMBOURG
     * 
     */
    LU,

    /**
     * LATVIA
     * 
     */
    LV,

    /**
     * LIBYAN ARAB JAMAHIRIYA
     * 
     */
    LY,

    /**
     * MOROCCO
     * 
     */
    MA,

    /**
     * MONACO
     * 
     */
    MC,

    /**
     * MOLDOVA, REPUBLIC OF
     * 
     */
    MD,

    /**
     * MONTENEGRO
     * 
     */
    ME,

    /**
     * SAINT MARTIN (FRENCH PART)
     * 
     */
    MF,

    /**
     * MADAGASCAR
     * 
     */
    MG,

    /**
     * MARSHALL ISLANDS
     * 
     */
    MH,

    /**
     * MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF
     * 
     */
    MK,

    /**
     * MALI
     * 
     */
    ML,

    /**
     * MYANMAR
     * 
     */
    MM,

    /**
     * MONGOLIA
     * 
     */
    MN,

    /**
     * MACAO
     * 
     */
    MO,

    /**
     * NORTHERN MARIANA ISLANDS
     * 
     */
    MP,

    /**
     * MARTINIQUE
     * 
     */
    MQ,

    /**
     * MAURITANIA
     * 
     */
    MR,

    /**
     * MONTSERRAT
     * 
     */
    MS,

    /**
     * MALTA
     * 
     */
    MT,

    /**
     * MAURITIUS
     * 
     */
    MU,

    /**
     * MALDIVES
     * 
     */
    MV,

    /**
     * MALAWI
     * 
     */
    MW,

    /**
     * MEXICO
     * 
     */
    MX,

    /**
     * MALAYSIA
     * 
     */
    MY,

    /**
     * MOZAMBIQUE
     * 
     */
    MZ,

    /**
     * NAMIBIA
     * 
     */
    NA,

    /**
     * NEW CALEDONIA
     * 
     */
    NC,

    /**
     * NIGER
     * 
     */
    NE,

    /**
     * NORFOLK ISLAND
     * 
     */
    NF,

    /**
     * NIGERIA
     * 
     */
    NG,

    /**
     * NICARAGUA
     * 
     */
    NI,

    /**
     * NETHERLANDS
     * 
     */
    NL,

    /**
     * NORWAY
     * 
     */
    NO,

    /**
     * NEPAL
     * 
     */
    NP,

    /**
     * NAURU
     * 
     */
    NR,

    /**
     * NIUE
     * 
     */
    NU,

    /**
     * NEW ZEALAND
     * 
     */
    NZ,

    /**
     * OMAN
     * 
     */
    OM,

    /**
     * PANAMA
     * 
     */
    PA,

    /**
     * PERU
     * 
     */
    PE,

    /**
     * FRENCH POLYNESIA
     * 
     */
    PF,

    /**
     * PAPUA NEW GUINEA
     * 
     */
    PG,

    /**
     * PHILIPPINES
     * 
     */
    PH,

    /**
     * PAKISTAN
     * 
     */
    PK,

    /**
     * POLAND
     * 
     */
    PL,

    /**
     * SAINT PIERRE AND MIQUELON
     * 
     */
    PM,

    /**
     * PITCAIRN
     * 
     */
    PN,

    /**
     * PUERTO RICO
     * 
     */
    PR,

    /**
     * PALESTINIAN TERRITORY, OCCUPIED
     * 
     */
    PS,

    /**
     * PORTUGAL
     * 
     */
    PT,

    /**
     * PALAU
     * 
     */
    PW,

    /**
     * PARAGUAY
     * 
     */
    PY,

    /**
     * QATAR
     * 
     */
    QA,

    /**
     * REUNION
     * 
     */
    RE,

    /**
     * ROMANIA
     * 
     */
    RO,

    /**
     * SERBIA
     * 
     */
    RS,

    /**
     * RUSSIAN FEDERATION
     * 
     */
    RU,

    /**
     * RWANDA
     * 
     */
    RW,

    /**
     * SAUDI ARABIA
     * 
     */
    SA,

    /**
     * SOLOMON ISLANDS
     * 
     */
    SB,

    /**
     * SEYCHELLES
     * 
     */
    SC,

    /**
     * SUDAN
     * 
     */
    SD,

    /**
     * SWEDEN
     * 
     */
    SE,

    /**
     * SINGAPORE
     * 
     */
    SG,

    /**
     * SAINT HELENA
     * 
     */
    SH,

    /**
     * SLOVENIA
     * 
     */
    SI,

    /**
     * SVALBARD AND JAN MAYEN
     * 
     */
    SJ,

    /**
     * SLOVAKIA
     * 
     */
    SK,

    /**
     * SIERRA LEONE
     * 
     */
    SL,

    /**
     * SAN MARINO
     * 
     */
    SM,

    /**
     * SENEGAL
     * 
     */
    SN,

    /**
     * SOMALIA
     * 
     */
    SO,

    /**
     * SURINAME
     * 
     */
    SR,

    /**
     * SAO TOME AND PRINCIPE
     * 
     */
    ST,

    /**
     * EL SALVADOR
     * 
     */
    SV,

    /**
     * SYRIAN ARAB REPUBLIC
     * 
     */
    SY,

    /**
     * SWAZILAND
     * 
     */
    SZ,

    /**
     * TURKS AND CAICOS ISLANDS
     * 
     */
    TC,

    /**
     * CHAD
     * 
     */
    TD,

    /**
     * FRENCH SOUTHERN TERRITORIES
     * 
     */
    TF,

    /**
     * TOGO
     * 
     */
    TG,

    /**
     * THAILAND
     * 
     */
    TH,

    /**
     * TAJIKISTAN
     * 
     */
    TJ,

    /**
     * TOKELAU
     * 
     */
    TK,

    /**
     * TIMOR-LESTE
     * 
     */
    TL,

    /**
     * TURKMENISTAN
     * 
     */
    TM,

    /**
     * TUNISIA
     * 
     */
    TN,

    /**
     * TONGA
     * 
     */
    TO,

    /**
     * TURKEY
     * 
     */
    TR,

    /**
     * TRINIDAD AND TOBAGO
     * 
     */
    TT,

    /**
     * TUVALU
     * 
     */
    TV,

    /**
     * TAIWAN, PROVINCE OF CHINA
     * 
     */
    TW,

    /**
     * TANZANIA, UNITED REPUBLIC OF
     * 
     */
    TZ,

    /**
     * UKRAINE
     * 
     */
    UA,

    /**
     * UGANDA
     * 
     */
    UG,

    /**
     * UNITED STATES MINOR OUTLYING ISLANDS
     * 
     */
    UM,

    /**
     * UNITED STATES
     * 
     */
    US,

    /**
     * URUGUAY
     * 
     */
    UY,

    /**
     * UZBEKISTAN
     * 
     */
    UZ,

    /**
     * HOLY SEE (VATICAN CITY STATE)
     * 
     */
    VA,

    /**
     * SAINT VINCENT AND THE GRENADINES
     * 
     */
    VC,

    /**
     * VENEZUELA, PLURINATIONAL STATE OF
     * 
     */
    VE,

    /**
     * VIRGIN ISLANDS, BRITISH
     * 
     */
    VG,

    /**
     * VIRGIN ISLANDS, U.S.
     * 
     */
    VI,

    /**
     * VIET NAM
     * 
     */
    VN,

    /**
     * VANUATU
     * 
     */
    VU,

    /**
     * WALLIS AND FUTUNA
     * 
     */
    WF,

    /**
     * SAMOA
     * 
     */
    WS,

    /**
     * YEMEN
     * 
     */
    YE,

    /**
     * MAYOTTE
     * 
     */
    YT,

    /**
     * SOUTH AFRICA
     * 
     */
    ZA,

    /**
     * ZAMBIA
     * 
     */
    ZM,

    /**
     * ZIMBABWE
     * 
     */
    ZW;

    public String value() {
        return name();
    }

    public static ISOCountryCodeType fromValue(String v) {
        return valueOf(v);
    }

}
