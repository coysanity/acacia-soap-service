
package com.ecm.test.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				=====
 * 				ecdDescriptor:: Identifies a tpu stack operating within the eCurrency system supervised by the specific Central Bank The only mandatory part of
 * 				the descriptor is the ecdID as issued by the Central Bank. For most applications ecdDescriptor is equivalent to ecdId and simply identifies the stack
 * 				installed at the eCD. However, additional
 * 				elements are provided for situations in which:
 * 				- eCD operations are partitioned across multiple instances of stacks for administrative, redundancy or performance reasons
 * 				- eCD is authorized by a Commercial Bank rather than directly by the Central Bank
 * 				- eCD authorized to operate in multiple countries wants to perform an FX transaction
 * 				- a central bank manages currencies of multiple countries in a monetary union
 * 				- multiple countries operate on the same currency (euro, dollar), not necessarily under authority of the same central bank
 * 				====
 * 			
 * 
 * <p>Java class for ecdDescriptor complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ecdDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="cbID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="commercialBankID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="countryID" type="{urn:com:ecmhp}ISOCountryCodeType" minOccurs="0"/&gt;
 *         &lt;element name="ecdID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="partitionID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ecdDescriptor", propOrder = {
    "cbID",
    "commercialBankID",
    "countryID",
    "ecdID",
    "partitionID"
})
public class EcdDescriptor {

    protected String cbID;
    protected String commercialBankID;
    @XmlSchemaType(name = "NMTOKEN")
    protected ISOCountryCodeType countryID;
    @XmlElement(required = true)
    protected String ecdID;
    protected String partitionID;

    /**
     * Gets the value of the cbID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCbID() {
        return cbID;
    }

    /**
     * Sets the value of the cbID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCbID(String value) {
        this.cbID = value;
    }

    /**
     * Gets the value of the commercialBankID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommercialBankID() {
        return commercialBankID;
    }

    /**
     * Sets the value of the commercialBankID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommercialBankID(String value) {
        this.commercialBankID = value;
    }

    /**
     * Gets the value of the countryID property.
     * 
     * @return
     *     possible object is
     *     {@link ISOCountryCodeType }
     *     
     */
    public ISOCountryCodeType getCountryID() {
        return countryID;
    }

    /**
     * Sets the value of the countryID property.
     * 
     * @param value
     *     allowed object is
     *     {@link ISOCountryCodeType }
     *     
     */
    public void setCountryID(ISOCountryCodeType value) {
        this.countryID = value;
    }

    /**
     * Gets the value of the ecdID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEcdID() {
        return ecdID;
    }

    /**
     * Sets the value of the ecdID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEcdID(String value) {
        this.ecdID = value;
    }

    /**
     * Gets the value of the partitionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartitionID() {
        return partitionID;
    }

    /**
     * Sets the value of the partitionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartitionID(String value) {
        this.partitionID = value;
    }

}
