
package com.ecm.test.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for setSubscriberResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="setSubscriberResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ecdHeader" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="subscriber" type="{urn:com:ecmhp}subscriberDescriptor"/&gt;
 *         &lt;element name="result" type="{urn:com:ecmhp}resultDescriptor"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "setSubscriberResponse", propOrder = {
    "ecdHeader",
    "subscriber",
    "result"
})
public class SetSubscriberResponse {

    @XmlElement(required = true)
    protected String ecdHeader;
    @XmlElement(required = true)
    protected SubscriberDescriptor subscriber;
    @XmlElement(required = true)
    protected ResultDescriptor result;

    /**
     * Gets the value of the ecdHeader property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEcdHeader() {
        return ecdHeader;
    }

    /**
     * Sets the value of the ecdHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEcdHeader(String value) {
        this.ecdHeader = value;
    }

    /**
     * Gets the value of the subscriber property.
     * 
     * @return
     *     possible object is
     *     {@link SubscriberDescriptor }
     *     
     */
    public SubscriberDescriptor getSubscriber() {
        return subscriber;
    }

    /**
     * Sets the value of the subscriber property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubscriberDescriptor }
     *     
     */
    public void setSubscriber(SubscriberDescriptor value) {
        this.subscriber = value;
    }

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link ResultDescriptor }
     *     
     */
    public ResultDescriptor getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultDescriptor }
     *     
     */
    public void setResult(ResultDescriptor value) {
        this.result = value;
    }

}
