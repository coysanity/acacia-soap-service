
package com.ecm.test.soap;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for possibleVaultTypes.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="possibleVaultTypes"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="ECDMST"/&gt;
 *     &lt;enumeration value="LIQ"/&gt;
 *     &lt;enumeration value="BRSG"/&gt;
 *     &lt;enumeration value="SUB"/&gt;
 *     &lt;enumeration value="MER"/&gt;
 *     &lt;enumeration value="AGN"/&gt;
 *     &lt;enumeration value="ESC"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "possibleVaultTypes")
@XmlEnum
public enum PossibleVaultTypes {

    ECDMST,
    LIQ,
    BRSG,
    SUB,
    MER,
    AGN,
    ESC;

    public String value() {
        return name();
    }

    public static PossibleVaultTypes fromValue(String v) {
        return valueOf(v);
    }

}
