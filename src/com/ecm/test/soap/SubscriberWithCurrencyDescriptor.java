
package com.ecm.test.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 	            =====
 * 				subscriberWithCurrencyDescriptor:: Subscriber with their eCurrency vault.
 * 	            ====
 * 			
 * 
 * <p>Java class for subscriberWithCurrencyDescriptor complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="subscriberWithCurrencyDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="subscriber" type="{urn:com:ecmhp}subscriberDescriptor"/&gt;
 *         &lt;element name="currencyVault" type="{urn:com:ecmhp}currencyVaultDescriptor" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "subscriberWithCurrencyDescriptor", propOrder = {
    "subscriber",
    "currencyVault"
})
public class SubscriberWithCurrencyDescriptor {

    @XmlElement(required = true)
    protected SubscriberDescriptor subscriber;
    protected CurrencyVaultDescriptor currencyVault;

    /**
     * Gets the value of the subscriber property.
     * 
     * @return
     *     possible object is
     *     {@link SubscriberDescriptor }
     *     
     */
    public SubscriberDescriptor getSubscriber() {
        return subscriber;
    }

    /**
     * Sets the value of the subscriber property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubscriberDescriptor }
     *     
     */
    public void setSubscriber(SubscriberDescriptor value) {
        this.subscriber = value;
    }

    /**
     * Gets the value of the currencyVault property.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyVaultDescriptor }
     *     
     */
    public CurrencyVaultDescriptor getCurrencyVault() {
        return currencyVault;
    }

    /**
     * Sets the value of the currencyVault property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyVaultDescriptor }
     *     
     */
    public void setCurrencyVault(CurrencyVaultDescriptor value) {
        this.currencyVault = value;
    }

}
