
package com.ecm.test.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *                 =====
 *                 subscriberTransferDescriptor:: describes a concrete transaction. It identifies its source, destination
 *                 and the amount of currency 'exchanging hands'
 *                 =====
 *             
 * 
 * <p>Java class for ecdTransferDescriptor complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ecdTransferDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="sourceEcd" type="{urn:com:ecmhp}ecdWithCurrencyDescriptor"/&gt;
 *         &lt;element name="destinationEcd" type="{urn:com:ecmhp}ecdWithCurrencyDescriptor"/&gt;
 *         &lt;element name="transaction" type="{urn:com:ecmhp}transactionDescriptor"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ecdTransferDescriptor", propOrder = {
    "sourceEcd",
    "destinationEcd",
    "transaction"
})
public class EcdTransferDescriptor {

    @XmlElement(required = true)
    protected EcdWithCurrencyDescriptor sourceEcd;
    @XmlElement(required = true)
    protected EcdWithCurrencyDescriptor destinationEcd;
    @XmlElement(required = true)
    protected TransactionDescriptor transaction;

    /**
     * Gets the value of the sourceEcd property.
     * 
     * @return
     *     possible object is
     *     {@link EcdWithCurrencyDescriptor }
     *     
     */
    public EcdWithCurrencyDescriptor getSourceEcd() {
        return sourceEcd;
    }

    /**
     * Sets the value of the sourceEcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link EcdWithCurrencyDescriptor }
     *     
     */
    public void setSourceEcd(EcdWithCurrencyDescriptor value) {
        this.sourceEcd = value;
    }

    /**
     * Gets the value of the destinationEcd property.
     * 
     * @return
     *     possible object is
     *     {@link EcdWithCurrencyDescriptor }
     *     
     */
    public EcdWithCurrencyDescriptor getDestinationEcd() {
        return destinationEcd;
    }

    /**
     * Sets the value of the destinationEcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link EcdWithCurrencyDescriptor }
     *     
     */
    public void setDestinationEcd(EcdWithCurrencyDescriptor value) {
        this.destinationEcd = value;
    }

    /**
     * Gets the value of the transaction property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionDescriptor }
     *     
     */
    public TransactionDescriptor getTransaction() {
        return transaction;
    }

    /**
     * Sets the value of the transaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionDescriptor }
     *     
     */
    public void setTransaction(TransactionDescriptor value) {
        this.transaction = value;
    }

}
