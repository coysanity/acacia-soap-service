
package com.ecm.test.soap;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for possibleTransferGroupEntryTypes.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="possibleTransferGroupEntryTypes"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="subscriberTransfer"/&gt;
 *     &lt;enumeration value="cashIn"/&gt;
 *     &lt;enumeration value="cashOut"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "possibleTransferGroupEntryTypes")
@XmlEnum
public enum PossibleTransferGroupEntryTypes {

    @XmlEnumValue("subscriberTransfer")
    SUBSCRIBER_TRANSFER("subscriberTransfer"),
    @XmlEnumValue("cashIn")
    CASH_IN("cashIn"),
    @XmlEnumValue("cashOut")
    CASH_OUT("cashOut");
    private final String value;

    PossibleTransferGroupEntryTypes(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PossibleTransferGroupEntryTypes fromValue(String v) {
        for (PossibleTransferGroupEntryTypes c: PossibleTransferGroupEntryTypes.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
