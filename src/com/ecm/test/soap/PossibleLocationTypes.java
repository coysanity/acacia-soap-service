
package com.ecm.test.soap;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for possibleLocationTypes.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="possibleLocationTypes"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="geoCoordinates"/&gt;
 *     &lt;enumeration value="postalCode"/&gt;
 *     &lt;enumeration value="ipAddress"/&gt;
 *     &lt;enumeration value="other"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "possibleLocationTypes")
@XmlEnum
public enum PossibleLocationTypes {

    @XmlEnumValue("geoCoordinates")
    GEO_COORDINATES("geoCoordinates"),
    @XmlEnumValue("postalCode")
    POSTAL_CODE("postalCode"),
    @XmlEnumValue("ipAddress")
    IP_ADDRESS("ipAddress"),
    @XmlEnumValue("other")
    OTHER("other");
    private final String value;

    PossibleLocationTypes(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PossibleLocationTypes fromValue(String v) {
        for (PossibleLocationTypes c: PossibleLocationTypes.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
