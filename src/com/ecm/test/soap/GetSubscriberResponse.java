
package com.ecm.test.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getSubscriberResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getSubscriberResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ecdHeader" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="subscriberWithCurrency" type="{urn:com:ecmhp}subscriberWithCurrencyDescriptor"/&gt;
 *         &lt;element name="result" type="{urn:com:ecmhp}resultDescriptor"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getSubscriberResponse", propOrder = {
    "ecdHeader",
    "subscriberWithCurrency",
    "result"
})
public class GetSubscriberResponse {

    @XmlElement(required = true)
    protected String ecdHeader;
    @XmlElement(required = true)
    protected SubscriberWithCurrencyDescriptor subscriberWithCurrency;
    @XmlElement(required = true)
    protected ResultDescriptor result;

    /**
     * Gets the value of the ecdHeader property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEcdHeader() {
        return ecdHeader;
    }

    /**
     * Sets the value of the ecdHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEcdHeader(String value) {
        this.ecdHeader = value;
    }

    /**
     * Gets the value of the subscriberWithCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link SubscriberWithCurrencyDescriptor }
     *     
     */
    public SubscriberWithCurrencyDescriptor getSubscriberWithCurrency() {
        return subscriberWithCurrency;
    }

    /**
     * Sets the value of the subscriberWithCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubscriberWithCurrencyDescriptor }
     *     
     */
    public void setSubscriberWithCurrency(SubscriberWithCurrencyDescriptor value) {
        this.subscriberWithCurrency = value;
    }

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link ResultDescriptor }
     *     
     */
    public ResultDescriptor getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultDescriptor }
     *     
     */
    public void setResult(ResultDescriptor value) {
        this.result = value;
    }

}
