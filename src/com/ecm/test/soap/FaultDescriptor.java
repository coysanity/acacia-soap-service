
package com.ecm.test.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				=====
 * 				faultDescriptor:: describes a 'negative' outcome of the operation. Faults can be caused by a variety of factors, from simple 'insufficient funds'
 * 				to eCMS processing error. faultCode indicates the actual cause as enumerated above and is intended for
 * 				programmatic processing by the ECD resultMessage is an arbitrary string and is intended for
 * 				human consumption or analysis by log file analysis and correlation tools
 * 				=====
 * 			
 * 
 * <p>Java class for faultDescriptor complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="faultDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="faultCode" type="{urn:com:ecmhp}possibleFaults"/&gt;
 *         &lt;element name="groupEntryNo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="groupEntryFaultCode" type="{urn:com:ecmhp}possibleFaults" minOccurs="0"/&gt;
 *         &lt;element name="resultMessage" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "faultDescriptor", propOrder = {
    "faultCode",
    "groupEntryNo",
    "groupEntryFaultCode",
    "resultMessage"
})
public class FaultDescriptor {

    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected PossibleFaults faultCode;
    protected Integer groupEntryNo;
    @XmlSchemaType(name = "NMTOKEN")
    protected PossibleFaults groupEntryFaultCode;
    @XmlElement(required = true)
    protected String resultMessage;

    /**
     * Gets the value of the faultCode property.
     * 
     * @return
     *     possible object is
     *     {@link PossibleFaults }
     *     
     */
    public PossibleFaults getFaultCode() {
        return faultCode;
    }

    /**
     * Sets the value of the faultCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link PossibleFaults }
     *     
     */
    public void setFaultCode(PossibleFaults value) {
        this.faultCode = value;
    }

    /**
     * Gets the value of the groupEntryNo property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getGroupEntryNo() {
        return groupEntryNo;
    }

    /**
     * Sets the value of the groupEntryNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setGroupEntryNo(Integer value) {
        this.groupEntryNo = value;
    }

    /**
     * Gets the value of the groupEntryFaultCode property.
     * 
     * @return
     *     possible object is
     *     {@link PossibleFaults }
     *     
     */
    public PossibleFaults getGroupEntryFaultCode() {
        return groupEntryFaultCode;
    }

    /**
     * Sets the value of the groupEntryFaultCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link PossibleFaults }
     *     
     */
    public void setGroupEntryFaultCode(PossibleFaults value) {
        this.groupEntryFaultCode = value;
    }

    /**
     * Gets the value of the resultMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResultMessage() {
        return resultMessage;
    }

    /**
     * Sets the value of the resultMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResultMessage(String value) {
        this.resultMessage = value;
    }

}
