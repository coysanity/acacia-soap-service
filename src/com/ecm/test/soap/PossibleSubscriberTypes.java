
package com.ecm.test.soap;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for possibleSubscriberTypes.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="possibleSubscriberTypes"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="SUB"/&gt;
 *     &lt;enumeration value="MER"/&gt;
 *     &lt;enumeration value="AGN"/&gt;
 *     &lt;enumeration value="ESC"/&gt;
 *     &lt;enumeration value="S"/&gt;
 *     &lt;enumeration value="M"/&gt;
 *     &lt;enumeration value="A"/&gt;
 *     &lt;enumeration value="E"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "possibleSubscriberTypes")
@XmlEnum
public enum PossibleSubscriberTypes {

    SUB,
    MER,
    AGN,
    ESC,
    S,
    M,
    A,
    E;

    public String value() {
        return name();
    }

    public static PossibleSubscriberTypes fromValue(String v) {
        return valueOf(v);
    }

}
