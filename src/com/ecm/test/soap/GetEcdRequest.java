
package com.ecm.test.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getEcdRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getEcdRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ecdHeader" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ecd" type="{urn:com:ecmhp}ecdDescriptor"/&gt;
 *         &lt;element name="vaultType" type="{urn:com:ecmhp}possibleEcdVaultTypes"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getEcdRequest", propOrder = {
    "ecdHeader",
    "ecd",
    "vaultType"
})
public class GetEcdRequest {

    @XmlElement(required = true, defaultValue = "")
    protected String ecdHeader;
    @XmlElement(required = true)
    protected EcdDescriptor ecd;
    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected PossibleEcdVaultTypes vaultType;

    /**
     * Gets the value of the ecdHeader property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEcdHeader() {
        return ecdHeader;
    }

    /**
     * Sets the value of the ecdHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEcdHeader(String value) {
        this.ecdHeader = value;
    }

    /**
     * Gets the value of the ecd property.
     * 
     * @return
     *     possible object is
     *     {@link EcdDescriptor }
     *     
     */
    public EcdDescriptor getEcd() {
        return ecd;
    }

    /**
     * Sets the value of the ecd property.
     * 
     * @param value
     *     allowed object is
     *     {@link EcdDescriptor }
     *     
     */
    public void setEcd(EcdDescriptor value) {
        this.ecd = value;
    }

    /**
     * Gets the value of the vaultType property.
     * 
     * @return
     *     possible object is
     *     {@link PossibleEcdVaultTypes }
     *     
     */
    public PossibleEcdVaultTypes getVaultType() {
        return vaultType;
    }

    /**
     * Sets the value of the vaultType property.
     * 
     * @param value
     *     allowed object is
     *     {@link PossibleEcdVaultTypes }
     *     
     */
    public void setVaultType(PossibleEcdVaultTypes value) {
        this.vaultType = value;
    }

}
