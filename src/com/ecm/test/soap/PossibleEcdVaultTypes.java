
package com.ecm.test.soap;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for possibleEcdVaultTypes.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="possibleEcdVaultTypes"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="ECDMST"/&gt;
 *     &lt;enumeration value="BRSG"/&gt;
 *     &lt;enumeration value="LIQ"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "possibleEcdVaultTypes")
@XmlEnum
public enum PossibleEcdVaultTypes {

    ECDMST,
    BRSG,
    LIQ;

    public String value() {
        return name();
    }

    public static PossibleEcdVaultTypes fromValue(String v) {
        return valueOf(v);
    }

}
