
package com.ecm.test.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				=====
 * 				currencyVaultDescriptor:: is an encapsulation of the actual eCurrency cryptogram with the amount in plaintext extracted for use and convenience of
 * 				systems outside of eCMS any mismatch between plaintext value and the cryptogram makes the currencyVaultDescriptor invalid. eCurrency entities are uniquely
 * 				identifiable (just like bank notes) and so are independent of any 'belong' association to actual subscribers. The vaultType does not describe a type of subscriber.
 * 				It is an intrinsic property of the eCurrency units.
 * 				====
 * 			
 * 
 * <p>Java class for currencyVaultDescriptor complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="currencyVaultDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="amount" type="{urn:com:ecmhp}amountDescriptor"/&gt;
 *         &lt;element name="vaultType" type="{urn:com:ecmhp}possibleVaultTypes" minOccurs="0"/&gt;
 *         &lt;element name="currencyObject" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "currencyVaultDescriptor", propOrder = {
    "amount",
    "vaultType",
    "currencyObject"
})
public class CurrencyVaultDescriptor {

    @XmlElement(required = true)
    protected AmountDescriptor amount;
    @XmlSchemaType(name = "NMTOKEN")
    protected PossibleVaultTypes vaultType;
    @XmlElement(required = true)
    protected String currencyObject;

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link AmountDescriptor }
     *     
     */
    public AmountDescriptor getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountDescriptor }
     *     
     */
    public void setAmount(AmountDescriptor value) {
        this.amount = value;
    }

    /**
     * Gets the value of the vaultType property.
     * 
     * @return
     *     possible object is
     *     {@link PossibleVaultTypes }
     *     
     */
    public PossibleVaultTypes getVaultType() {
        return vaultType;
    }

    /**
     * Sets the value of the vaultType property.
     * 
     * @param value
     *     allowed object is
     *     {@link PossibleVaultTypes }
     *     
     */
    public void setVaultType(PossibleVaultTypes value) {
        this.vaultType = value;
    }

    /**
     * Gets the value of the currencyObject property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyObject() {
        return currencyObject;
    }

    /**
     * Sets the value of the currencyObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyObject(String value) {
        this.currencyObject = value;
    }

}
