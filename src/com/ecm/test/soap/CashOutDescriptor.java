
package com.ecm.test.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				=====
 * 				cashOutDescriptor:: describes a concrete transaction. It identifies its source, destination
 * 				and the amount of currency 'exchanging hands'. ALL transactions which result in conversion of eCurrency into any other form of money or
 * 				currency, such as coin, bank notes, or promissory notes such as loans,
 * 				are treated as cash-out. All cash-out amounts come from a subscriber and move to the ECD master vault.
 * 				=====
 * 			
 * 
 * <p>Java class for cashOutDescriptor complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cashOutDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="sourceSubscriber" type="{urn:com:ecmhp}subscriberWithCurrencyDescriptor"/&gt;
 *         &lt;element name="destinationEcd" type="{urn:com:ecmhp}ecdWithCurrencyDescriptor"/&gt;
 *         &lt;element name="transaction" type="{urn:com:ecmhp}transactionDescriptor"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cashOutDescriptor", propOrder = {
    "sourceSubscriber",
    "destinationEcd",
    "transaction"
})
public class CashOutDescriptor {

    @XmlElement(required = true)
    protected SubscriberWithCurrencyDescriptor sourceSubscriber;
    @XmlElement(required = true)
    protected EcdWithCurrencyDescriptor destinationEcd;
    @XmlElement(required = true)
    protected TransactionDescriptor transaction;

    /**
     * Gets the value of the sourceSubscriber property.
     * 
     * @return
     *     possible object is
     *     {@link SubscriberWithCurrencyDescriptor }
     *     
     */
    public SubscriberWithCurrencyDescriptor getSourceSubscriber() {
        return sourceSubscriber;
    }

    /**
     * Sets the value of the sourceSubscriber property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubscriberWithCurrencyDescriptor }
     *     
     */
    public void setSourceSubscriber(SubscriberWithCurrencyDescriptor value) {
        this.sourceSubscriber = value;
    }

    /**
     * Gets the value of the destinationEcd property.
     * 
     * @return
     *     possible object is
     *     {@link EcdWithCurrencyDescriptor }
     *     
     */
    public EcdWithCurrencyDescriptor getDestinationEcd() {
        return destinationEcd;
    }

    /**
     * Sets the value of the destinationEcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link EcdWithCurrencyDescriptor }
     *     
     */
    public void setDestinationEcd(EcdWithCurrencyDescriptor value) {
        this.destinationEcd = value;
    }

    /**
     * Gets the value of the transaction property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionDescriptor }
     *     
     */
    public TransactionDescriptor getTransaction() {
        return transaction;
    }

    /**
     * Sets the value of the transaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionDescriptor }
     *     
     */
    public void setTransaction(TransactionDescriptor value) {
        this.transaction = value;
    }

}
