
package com.ecm.test.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				=====
 * 				locationDescriptor:: specifies location of an event (in this case transaction). Few different types of location types can be given.
 * 				====
 * 			
 * 
 * <p>Java class for locationDescriptor complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="locationDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="locationType" type="{urn:com:ecmhp}possibleLocationTypes"/&gt;
 *         &lt;element name="locationGeo" type="{urn:com:ecmhp}geoLocationType" minOccurs="0"/&gt;
 *         &lt;element name="locationIp" type="{urn:com:ecmhp}ipLocationType" minOccurs="0"/&gt;
 *         &lt;element name="locationPostal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="locationOther" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "locationDescriptor", propOrder = {
    "locationType",
    "locationGeo",
    "locationIp",
    "locationPostal",
    "locationOther"
})
public class LocationDescriptor {

    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected PossibleLocationTypes locationType;
    protected GeoLocationType locationGeo;
    protected IpLocationType locationIp;
    protected String locationPostal;
    protected String locationOther;

    /**
     * Gets the value of the locationType property.
     * 
     * @return
     *     possible object is
     *     {@link PossibleLocationTypes }
     *     
     */
    public PossibleLocationTypes getLocationType() {
        return locationType;
    }

    /**
     * Sets the value of the locationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link PossibleLocationTypes }
     *     
     */
    public void setLocationType(PossibleLocationTypes value) {
        this.locationType = value;
    }

    /**
     * Gets the value of the locationGeo property.
     * 
     * @return
     *     possible object is
     *     {@link GeoLocationType }
     *     
     */
    public GeoLocationType getLocationGeo() {
        return locationGeo;
    }

    /**
     * Sets the value of the locationGeo property.
     * 
     * @param value
     *     allowed object is
     *     {@link GeoLocationType }
     *     
     */
    public void setLocationGeo(GeoLocationType value) {
        this.locationGeo = value;
    }

    /**
     * Gets the value of the locationIp property.
     * 
     * @return
     *     possible object is
     *     {@link IpLocationType }
     *     
     */
    public IpLocationType getLocationIp() {
        return locationIp;
    }

    /**
     * Sets the value of the locationIp property.
     * 
     * @param value
     *     allowed object is
     *     {@link IpLocationType }
     *     
     */
    public void setLocationIp(IpLocationType value) {
        this.locationIp = value;
    }

    /**
     * Gets the value of the locationPostal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationPostal() {
        return locationPostal;
    }

    /**
     * Sets the value of the locationPostal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationPostal(String value) {
        this.locationPostal = value;
    }

    /**
     * Gets the value of the locationOther property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationOther() {
        return locationOther;
    }

    /**
     * Sets the value of the locationOther property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationOther(String value) {
        this.locationOther = value;
    }

}
