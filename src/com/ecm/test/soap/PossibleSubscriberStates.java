
package com.ecm.test.soap;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for possibleSubscriberStates.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="possibleSubscriberStates"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="active"/&gt;
 *     &lt;enumeration value="suspended"/&gt;
 *     &lt;enumeration value="blocked"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "possibleSubscriberStates")
@XmlEnum
public enum PossibleSubscriberStates {

    @XmlEnumValue("active")
    ACTIVE("active"),
    @XmlEnumValue("suspended")
    SUSPENDED("suspended"),
    @XmlEnumValue("blocked")
    BLOCKED("blocked");
    private final String value;

    PossibleSubscriberStates(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PossibleSubscriberStates fromValue(String v) {
        for (PossibleSubscriberStates c: PossibleSubscriberStates.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
