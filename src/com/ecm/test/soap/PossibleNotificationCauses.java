
package com.ecm.test.soap;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for possibleNotificationCauses.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="possibleNotificationCauses"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="ADD_CURRENCY"/&gt;
 *     &lt;enumeration value="REMOVE_CURRENCY"/&gt;
 *     &lt;enumeration value="BRASSAGE"/&gt;
 *     &lt;enumeration value="REPATRIATION"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "possibleNotificationCauses")
@XmlEnum
public enum PossibleNotificationCauses {

    ADD_CURRENCY,
    REMOVE_CURRENCY,
    BRASSAGE,
    REPATRIATION;

    public String value() {
        return name();
    }

    public static PossibleNotificationCauses fromValue(String v) {
        return valueOf(v);
    }

}
