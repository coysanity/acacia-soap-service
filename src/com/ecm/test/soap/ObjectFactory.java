
package com.ecm.test.soap;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.ecmhp package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetEcdRequest_QNAME = new QName("urn:com:ecmhp", "getEcdRequest");
    private final static QName _GetEcdResponse_QNAME = new QName("urn:com:ecmhp", "getEcdResponse");
    private final static QName _GetSubscriberRequest_QNAME = new QName("urn:com:ecmhp", "getSubscriberRequest");
    private final static QName _GetSubscriberResponse_QNAME = new QName("urn:com:ecmhp", "getSubscriberResponse");
    private final static QName _SetSubscriberRequest_QNAME = new QName("urn:com:ecmhp", "setSubscriberRequest");
    private final static QName _SetSubscriberResponse_QNAME = new QName("urn:com:ecmhp", "setSubscriberResponse");
    private final static QName _AddSubscriberRequest_QNAME = new QName("urn:com:ecmhp", "addSubscriberRequest");
    private final static QName _AddSubscriberResponse_QNAME = new QName("urn:com:ecmhp", "addSubscriberResponse");
    private final static QName _EcdTransferRequest_QNAME = new QName("urn:com:ecmhp", "ecdTransferRequest");
    private final static QName _EcdTransferResponse_QNAME = new QName("urn:com:ecmhp", "ecdTransferResponse");
    private final static QName _SubscriberTransferRequest_QNAME = new QName("urn:com:ecmhp", "subscriberTransferRequest");
    private final static QName _SubscriberTransferResponse_QNAME = new QName("urn:com:ecmhp", "subscriberTransferResponse");
    private final static QName _CashInRequest_QNAME = new QName("urn:com:ecmhp", "cashInRequest");
    private final static QName _CashInResponse_QNAME = new QName("urn:com:ecmhp", "cashInResponse");
    private final static QName _CashOutRequest_QNAME = new QName("urn:com:ecmhp", "cashOutRequest");
    private final static QName _CashOutResponse_QNAME = new QName("urn:com:ecmhp", "cashOutResponse");
    private final static QName _DeleteSubscriberRequest_QNAME = new QName("urn:com:ecmhp", "deleteSubscriberRequest");
    private final static QName _DeleteSubscriberResponse_QNAME = new QName("urn:com:ecmhp", "deleteSubscriberResponse");
    private final static QName _TransferGroupRequest_QNAME = new QName("urn:com:ecmhp", "transferGroupRequest");
    private final static QName _TransferGroupResponse_QNAME = new QName("urn:com:ecmhp", "transferGroupResponse");
    private final static QName _FaultResponse_QNAME = new QName("urn:com:ecmhp", "faultResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.ecmhp
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetEcdRequest }
     * 
     */
    public GetEcdRequest createGetEcdRequest() {
        return new GetEcdRequest();
    }

    /**
     * Create an instance of {@link GetEcdResponse }
     * 
     */
    public GetEcdResponse createGetEcdResponse() {
        return new GetEcdResponse();
    }

    /**
     * Create an instance of {@link GetSubscriberRequest }
     * 
     */
    public GetSubscriberRequest createGetSubscriberRequest() {
        return new GetSubscriberRequest();
    }

    /**
     * Create an instance of {@link GetSubscriberResponse }
     * 
     */
    public GetSubscriberResponse createGetSubscriberResponse() {
        return new GetSubscriberResponse();
    }

    /**
     * Create an instance of {@link SetSubscriberRequest }
     * 
     */
    public SetSubscriberRequest createSetSubscriberRequest() {
        return new SetSubscriberRequest();
    }

    /**
     * Create an instance of {@link SetSubscriberResponse }
     * 
     */
    public SetSubscriberResponse createSetSubscriberResponse() {
        return new SetSubscriberResponse();
    }

    /**
     * Create an instance of {@link AddSubscriberRequest }
     * 
     */
    public AddSubscriberRequest createAddSubscriberRequest() {
        return new AddSubscriberRequest();
    }

    /**
     * Create an instance of {@link AddSubscriberResponse }
     * 
     */
    public AddSubscriberResponse createAddSubscriberResponse() {
        return new AddSubscriberResponse();
    }

    /**
     * Create an instance of {@link EcdTransferRequest }
     * 
     */
    public EcdTransferRequest createEcdTransferRequest() {
        return new EcdTransferRequest();
    }

    /**
     * Create an instance of {@link EcdTransferResponse }
     * 
     */
    public EcdTransferResponse createEcdTransferResponse() {
        return new EcdTransferResponse();
    }

    /**
     * Create an instance of {@link SubscriberTransferRequest }
     * 
     */
    public SubscriberTransferRequest createSubscriberTransferRequest() {
        return new SubscriberTransferRequest();
    }

    /**
     * Create an instance of {@link SubscriberTransferResponse }
     * 
     */
    public SubscriberTransferResponse createSubscriberTransferResponse() {
        return new SubscriberTransferResponse();
    }

    /**
     * Create an instance of {@link CashInRequest }
     * 
     */
    public CashInRequest createCashInRequest() {
        return new CashInRequest();
    }

    /**
     * Create an instance of {@link CashInResponse }
     * 
     */
    public CashInResponse createCashInResponse() {
        return new CashInResponse();
    }

    /**
     * Create an instance of {@link CashOutRequest }
     * 
     */
    public CashOutRequest createCashOutRequest() {
        return new CashOutRequest();
    }

    /**
     * Create an instance of {@link CashOutResponse }
     * 
     */
    public CashOutResponse createCashOutResponse() {
        return new CashOutResponse();
    }

    /**
     * Create an instance of {@link DeleteSubscriberRequest }
     * 
     */
    public DeleteSubscriberRequest createDeleteSubscriberRequest() {
        return new DeleteSubscriberRequest();
    }

    /**
     * Create an instance of {@link DeleteSubscriberResponse }
     * 
     */
    public DeleteSubscriberResponse createDeleteSubscriberResponse() {
        return new DeleteSubscriberResponse();
    }

    /**
     * Create an instance of {@link TransferGroupRequest }
     * 
     */
    public TransferGroupRequest createTransferGroupRequest() {
        return new TransferGroupRequest();
    }

    /**
     * Create an instance of {@link TransferGroupResponse }
     * 
     */
    public TransferGroupResponse createTransferGroupResponse() {
        return new TransferGroupResponse();
    }

    /**
     * Create an instance of {@link FaultResponse }
     * 
     */
    public FaultResponse createFaultResponse() {
        return new FaultResponse();
    }

    /**
     * Create an instance of {@link FaultDescriptor }
     * 
     */
    public FaultDescriptor createFaultDescriptor() {
        return new FaultDescriptor();
    }

    /**
     * Create an instance of {@link GeoLocationType }
     * 
     */
    public GeoLocationType createGeoLocationType() {
        return new GeoLocationType();
    }

    /**
     * Create an instance of {@link IpLocationType }
     * 
     */
    public IpLocationType createIpLocationType() {
        return new IpLocationType();
    }

    /**
     * Create an instance of {@link EcdDescriptor }
     * 
     */
    public EcdDescriptor createEcdDescriptor() {
        return new EcdDescriptor();
    }

    /**
     * Create an instance of {@link SubscriberDescriptor }
     * 
     */
    public SubscriberDescriptor createSubscriberDescriptor() {
        return new SubscriberDescriptor();
    }

    /**
     * Create an instance of {@link AmountDescriptor }
     * 
     */
    public AmountDescriptor createAmountDescriptor() {
        return new AmountDescriptor();
    }

    /**
     * Create an instance of {@link CurrencyVaultDescriptor }
     * 
     */
    public CurrencyVaultDescriptor createCurrencyVaultDescriptor() {
        return new CurrencyVaultDescriptor();
    }

    /**
     * Create an instance of {@link SubscriberWithCurrencyDescriptor }
     * 
     */
    public SubscriberWithCurrencyDescriptor createSubscriberWithCurrencyDescriptor() {
        return new SubscriberWithCurrencyDescriptor();
    }

    /**
     * Create an instance of {@link EcdWithCurrencyDescriptor }
     * 
     */
    public EcdWithCurrencyDescriptor createEcdWithCurrencyDescriptor() {
        return new EcdWithCurrencyDescriptor();
    }

    /**
     * Create an instance of {@link LocationDescriptor }
     * 
     */
    public LocationDescriptor createLocationDescriptor() {
        return new LocationDescriptor();
    }

    /**
     * Create an instance of {@link EcdTransferDescriptor }
     * 
     */
    public EcdTransferDescriptor createEcdTransferDescriptor() {
        return new EcdTransferDescriptor();
    }

    /**
     * Create an instance of {@link SubscriberTransferDescriptor }
     * 
     */
    public SubscriberTransferDescriptor createSubscriberTransferDescriptor() {
        return new SubscriberTransferDescriptor();
    }

    /**
     * Create an instance of {@link CashInDescriptor }
     * 
     */
    public CashInDescriptor createCashInDescriptor() {
        return new CashInDescriptor();
    }

    /**
     * Create an instance of {@link CashOutDescriptor }
     * 
     */
    public CashOutDescriptor createCashOutDescriptor() {
        return new CashOutDescriptor();
    }

    /**
     * Create an instance of {@link TransactionDescriptor }
     * 
     */
    public TransactionDescriptor createTransactionDescriptor() {
        return new TransactionDescriptor();
    }

    /**
     * Create an instance of {@link ResultDescriptor }
     * 
     */
    public ResultDescriptor createResultDescriptor() {
        return new ResultDescriptor();
    }

    /**
     * Create an instance of {@link SetEcdRequest }
     * 
     */
    public SetEcdRequest createSetEcdRequest() {
        return new SetEcdRequest();
    }

    /**
     * Create an instance of {@link SetEcdResponse }
     * 
     */
    public SetEcdResponse createSetEcdResponse() {
        return new SetEcdResponse();
    }

    /**
     * Create an instance of {@link TransferGroupRequestEntry }
     * 
     */
    public TransferGroupRequestEntry createTransferGroupRequestEntry() {
        return new TransferGroupRequestEntry();
    }

    /**
     * Create an instance of {@link TransferGroupResponseEntry }
     * 
     */
    public TransferGroupResponseEntry createTransferGroupResponseEntry() {
        return new TransferGroupResponseEntry();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEcdRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:ecmhp", name = "getEcdRequest")
    public JAXBElement<GetEcdRequest> createGetEcdRequest(GetEcdRequest value) {
        return new JAXBElement<GetEcdRequest>(_GetEcdRequest_QNAME, GetEcdRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEcdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:ecmhp", name = "getEcdResponse")
    public JAXBElement<GetEcdResponse> createGetEcdResponse(GetEcdResponse value) {
        return new JAXBElement<GetEcdResponse>(_GetEcdResponse_QNAME, GetEcdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSubscriberRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:ecmhp", name = "getSubscriberRequest")
    public JAXBElement<GetSubscriberRequest> createGetSubscriberRequest(GetSubscriberRequest value) {
        return new JAXBElement<GetSubscriberRequest>(_GetSubscriberRequest_QNAME, GetSubscriberRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSubscriberResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:ecmhp", name = "getSubscriberResponse")
    public JAXBElement<GetSubscriberResponse> createGetSubscriberResponse(GetSubscriberResponse value) {
        return new JAXBElement<GetSubscriberResponse>(_GetSubscriberResponse_QNAME, GetSubscriberResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetSubscriberRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:ecmhp", name = "setSubscriberRequest")
    public JAXBElement<SetSubscriberRequest> createSetSubscriberRequest(SetSubscriberRequest value) {
        return new JAXBElement<SetSubscriberRequest>(_SetSubscriberRequest_QNAME, SetSubscriberRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetSubscriberResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:ecmhp", name = "setSubscriberResponse")
    public JAXBElement<SetSubscriberResponse> createSetSubscriberResponse(SetSubscriberResponse value) {
        return new JAXBElement<SetSubscriberResponse>(_SetSubscriberResponse_QNAME, SetSubscriberResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddSubscriberRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:ecmhp", name = "addSubscriberRequest")
    public JAXBElement<AddSubscriberRequest> createAddSubscriberRequest(AddSubscriberRequest value) {
        return new JAXBElement<AddSubscriberRequest>(_AddSubscriberRequest_QNAME, AddSubscriberRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddSubscriberResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:ecmhp", name = "addSubscriberResponse")
    public JAXBElement<AddSubscriberResponse> createAddSubscriberResponse(AddSubscriberResponse value) {
        return new JAXBElement<AddSubscriberResponse>(_AddSubscriberResponse_QNAME, AddSubscriberResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EcdTransferRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:ecmhp", name = "ecdTransferRequest")
    public JAXBElement<EcdTransferRequest> createEcdTransferRequest(EcdTransferRequest value) {
        return new JAXBElement<EcdTransferRequest>(_EcdTransferRequest_QNAME, EcdTransferRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EcdTransferResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:ecmhp", name = "ecdTransferResponse")
    public JAXBElement<EcdTransferResponse> createEcdTransferResponse(EcdTransferResponse value) {
        return new JAXBElement<EcdTransferResponse>(_EcdTransferResponse_QNAME, EcdTransferResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubscriberTransferRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:ecmhp", name = "subscriberTransferRequest")
    public JAXBElement<SubscriberTransferRequest> createSubscriberTransferRequest(SubscriberTransferRequest value) {
        return new JAXBElement<SubscriberTransferRequest>(_SubscriberTransferRequest_QNAME, SubscriberTransferRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubscriberTransferResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:ecmhp", name = "subscriberTransferResponse")
    public JAXBElement<SubscriberTransferResponse> createSubscriberTransferResponse(SubscriberTransferResponse value) {
        return new JAXBElement<SubscriberTransferResponse>(_SubscriberTransferResponse_QNAME, SubscriberTransferResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CashInRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:ecmhp", name = "cashInRequest")
    public JAXBElement<CashInRequest> createCashInRequest(CashInRequest value) {
        return new JAXBElement<CashInRequest>(_CashInRequest_QNAME, CashInRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CashInResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:ecmhp", name = "cashInResponse")
    public JAXBElement<CashInResponse> createCashInResponse(CashInResponse value) {
        return new JAXBElement<CashInResponse>(_CashInResponse_QNAME, CashInResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CashOutRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:ecmhp", name = "cashOutRequest")
    public JAXBElement<CashOutRequest> createCashOutRequest(CashOutRequest value) {
        return new JAXBElement<CashOutRequest>(_CashOutRequest_QNAME, CashOutRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CashOutResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:ecmhp", name = "cashOutResponse")
    public JAXBElement<CashOutResponse> createCashOutResponse(CashOutResponse value) {
        return new JAXBElement<CashOutResponse>(_CashOutResponse_QNAME, CashOutResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteSubscriberRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:ecmhp", name = "deleteSubscriberRequest")
    public JAXBElement<DeleteSubscriberRequest> createDeleteSubscriberRequest(DeleteSubscriberRequest value) {
        return new JAXBElement<DeleteSubscriberRequest>(_DeleteSubscriberRequest_QNAME, DeleteSubscriberRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteSubscriberResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:ecmhp", name = "deleteSubscriberResponse")
    public JAXBElement<DeleteSubscriberResponse> createDeleteSubscriberResponse(DeleteSubscriberResponse value) {
        return new JAXBElement<DeleteSubscriberResponse>(_DeleteSubscriberResponse_QNAME, DeleteSubscriberResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransferGroupRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:ecmhp", name = "transferGroupRequest")
    public JAXBElement<TransferGroupRequest> createTransferGroupRequest(TransferGroupRequest value) {
        return new JAXBElement<TransferGroupRequest>(_TransferGroupRequest_QNAME, TransferGroupRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransferGroupResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:ecmhp", name = "transferGroupResponse")
    public JAXBElement<TransferGroupResponse> createTransferGroupResponse(TransferGroupResponse value) {
        return new JAXBElement<TransferGroupResponse>(_TransferGroupResponse_QNAME, TransferGroupResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FaultResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:ecmhp", name = "faultResponse")
    public JAXBElement<FaultResponse> createFaultResponse(FaultResponse value) {
        return new JAXBElement<FaultResponse>(_FaultResponse_QNAME, FaultResponse.class, null, value);
    }

}
