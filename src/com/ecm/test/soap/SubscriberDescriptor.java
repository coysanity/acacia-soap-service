
package com.ecm.test.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				=====
 * 				subscriberDescriptor:: Identifies a subscriber of the payment system. The descriptor must refer to a unique subscriber
 * 				through the combination of the ecdDescriptor and the subscriberId. The subscriberId can be optionally obfuscated either by the ECD
 * 				prior to sending to the eCMS or can be obfuscated by eCMS as a first step of processing. This serves to maintain anonymity
 * 				of the subscribers within the eCMS. subscriberType is not currently used by the eCMS and is provided for internal ECD use.
 * 				subscriberState is also provided for internal use by the eCD. Only subscribers in 'active' subscriberState will be allowed 
 * 				to participate in transactions. eCMS does not modify this state internally.it can be modified by the ECD through setSubscriber operation.
 * 				====
 * 			
 * 
 * <p>Java class for subscriberDescriptor complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="subscriberDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ecd" type="{urn:com:ecmhp}ecdDescriptor"/&gt;
 *         &lt;element name="subscriberID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="subscriberType" type="{urn:com:ecmhp}possibleSubscriberTypes" minOccurs="0"/&gt;
 *         &lt;element name="subscriberState" type="{urn:com:ecmhp}possibleSubscriberStates" minOccurs="0"/&gt;
 *         &lt;element name="ecdSubscriberType" type="{urn:com:ecmhp}ecdStringMaxLength32" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "subscriberDescriptor", propOrder = {
    "ecd",
    "subscriberID",
    "subscriberType",
    "subscriberState",
    "ecdSubscriberType"
})
public class SubscriberDescriptor {

    @XmlElement(required = true)
    protected EcdDescriptor ecd;
    @XmlElement(required = true)
    protected String subscriberID;
    @XmlSchemaType(name = "NMTOKEN")
    protected PossibleSubscriberTypes subscriberType;
    @XmlSchemaType(name = "NMTOKEN")
    protected PossibleSubscriberStates subscriberState;
    protected String ecdSubscriberType;

    /**
     * Gets the value of the ecd property.
     * 
     * @return
     *     possible object is
     *     {@link EcdDescriptor }
     *     
     */
    public EcdDescriptor getEcd() {
        return ecd;
    }

    /**
     * Sets the value of the ecd property.
     * 
     * @param value
     *     allowed object is
     *     {@link EcdDescriptor }
     *     
     */
    public void setEcd(EcdDescriptor value) {
        this.ecd = value;
    }

    /**
     * Gets the value of the subscriberID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriberID() {
        return subscriberID;
    }

    /**
     * Sets the value of the subscriberID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriberID(String value) {
        this.subscriberID = value;
    }

    /**
     * Gets the value of the subscriberType property.
     * 
     * @return
     *     possible object is
     *     {@link PossibleSubscriberTypes }
     *     
     */
    public PossibleSubscriberTypes getSubscriberType() {
        return subscriberType;
    }

    /**
     * Sets the value of the subscriberType property.
     * 
     * @param value
     *     allowed object is
     *     {@link PossibleSubscriberTypes }
     *     
     */
    public void setSubscriberType(PossibleSubscriberTypes value) {
        this.subscriberType = value;
    }

    /**
     * Gets the value of the subscriberState property.
     * 
     * @return
     *     possible object is
     *     {@link PossibleSubscriberStates }
     *     
     */
    public PossibleSubscriberStates getSubscriberState() {
        return subscriberState;
    }

    /**
     * Sets the value of the subscriberState property.
     * 
     * @param value
     *     allowed object is
     *     {@link PossibleSubscriberStates }
     *     
     */
    public void setSubscriberState(PossibleSubscriberStates value) {
        this.subscriberState = value;
    }

    /**
     * Gets the value of the ecdSubscriberType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEcdSubscriberType() {
        return ecdSubscriberType;
    }

    /**
     * Sets the value of the ecdSubscriberType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEcdSubscriberType(String value) {
        this.ecdSubscriberType = value;
    }

}
