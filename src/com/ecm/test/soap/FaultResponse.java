
package com.ecm.test.soap;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for faultResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="faultResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ecdHeader" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="faultItem" type="{urn:com:ecmhp}faultDescriptor" maxOccurs="unbounded"/&gt;
 *         &lt;element name="eventTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "faultResponse", propOrder = {
    "ecdHeader",
    "faultItem",
    "eventTime"
})
public class FaultResponse {

    @XmlElement(required = true)
    protected String ecdHeader;
    @XmlElement(required = true)
    protected List<FaultDescriptor> faultItem;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar eventTime;

    /**
     * Gets the value of the ecdHeader property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEcdHeader() {
        return ecdHeader;
    }

    /**
     * Sets the value of the ecdHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEcdHeader(String value) {
        this.ecdHeader = value;
    }

    /**
     * Gets the value of the faultItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the faultItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFaultItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FaultDescriptor }
     * 
     * 
     */
    public List<FaultDescriptor> getFaultItem() {
        if (faultItem == null) {
            faultItem = new ArrayList<FaultDescriptor>();
        }
        return this.faultItem;
    }

    /**
     * Gets the value of the eventTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEventTime() {
        return eventTime;
    }

    /**
     * Sets the value of the eventTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEventTime(XMLGregorianCalendar value) {
        this.eventTime = value;
    }

}
