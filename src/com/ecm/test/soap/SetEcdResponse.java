
package com.ecm.test.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for setEcdResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="setEcdResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="header" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ecdWithCurrency" type="{urn:com:ecmhp}ecdWithCurrencyDescriptor" minOccurs="0"/&gt;
 *         &lt;element name="result" type="{urn:com:ecmhp}resultDescriptor"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "setEcdResponse", propOrder = {
    "header",
    "ecdWithCurrency",
    "result"
})
public class SetEcdResponse {

    protected String header;
    protected EcdWithCurrencyDescriptor ecdWithCurrency;
    @XmlElement(required = true)
    protected ResultDescriptor result;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHeader(String value) {
        this.header = value;
    }

    /**
     * Gets the value of the ecdWithCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link EcdWithCurrencyDescriptor }
     *     
     */
    public EcdWithCurrencyDescriptor getEcdWithCurrency() {
        return ecdWithCurrency;
    }

    /**
     * Sets the value of the ecdWithCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link EcdWithCurrencyDescriptor }
     *     
     */
    public void setEcdWithCurrency(EcdWithCurrencyDescriptor value) {
        this.ecdWithCurrency = value;
    }

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link ResultDescriptor }
     *     
     */
    public ResultDescriptor getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultDescriptor }
     *     
     */
    public void setResult(ResultDescriptor value) {
        this.result = value;
    }

}
