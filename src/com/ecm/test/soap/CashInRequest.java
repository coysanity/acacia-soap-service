
package com.ecm.test.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cashInRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cashInRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ecdHeader" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="descriptor" type="{urn:com:ecmhp}cashInDescriptor"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cashInRequest", propOrder = {
    "ecdHeader",
    "descriptor"
})
public class CashInRequest {

    @XmlElement(required = true, defaultValue = "")
    protected String ecdHeader;
    @XmlElement(required = true)
    protected CashInDescriptor descriptor;

    /**
     * Gets the value of the ecdHeader property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEcdHeader() {
        return ecdHeader;
    }

    /**
     * Sets the value of the ecdHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEcdHeader(String value) {
        this.ecdHeader = value;
    }

    /**
     * Gets the value of the descriptor property.
     * 
     * @return
     *     possible object is
     *     {@link CashInDescriptor }
     *     
     */
    public CashInDescriptor getDescriptor() {
        return descriptor;
    }

    /**
     * Sets the value of the descriptor property.
     * 
     * @param value
     *     allowed object is
     *     {@link CashInDescriptor }
     *     
     */
    public void setDescriptor(CashInDescriptor value) {
        this.descriptor = value;
    }

}
