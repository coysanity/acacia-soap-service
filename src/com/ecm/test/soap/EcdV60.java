package com.ecm.test.soap;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 3.2.0
 * 2018-02-01T11:46:40.817-08:00
 * Generated source version: 3.2.0
 * 
 */
@WebServiceClient(name = "ecdV60", 
                  wsdlLocation = "file:/Users/dyin/workspace/ecm/ecdcommon/resources/ecdIntegrationV60.wsdl",
                  targetNamespace = "urn:com:ecmhp") 
public class EcdV60 extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("urn:com:ecmhp", "ecdV60");
    public final static QName EcdV60SOAPPort = new QName("urn:com:ecmhp", "ecdV60SOAPPort");
    static {
        URL url = null;
        try {
            url = new URL("file:/Users/dyin/workspace/ecm/ecdcommon/resources/ecdIntegrationV60.wsdl");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(EcdV60.class.getName())
                .log(java.util.logging.Level.INFO, 
                     "Can not initialize the default wsdl from {0}", "file:/Users/dyin/workspace/ecm/ecdcommon/resources/ecdIntegrationV60.wsdl");
        }
        WSDL_LOCATION = url;
    }

    public EcdV60(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public EcdV60(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public EcdV60() {
        super(WSDL_LOCATION, SERVICE);
    }
    
    public EcdV60(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    public EcdV60(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    public EcdV60(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }    




    /**
     *
     * @return
     *     returns EcdV60SOAPPort
     */
    @WebEndpoint(name = "ecdV60SOAPPort")
    public EcdV60SOAPPort getEcdV60SOAPPort() {
        return super.getPort(EcdV60SOAPPort, EcdV60SOAPPort.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns EcdV60SOAPPort
     */
    @WebEndpoint(name = "ecdV60SOAPPort")
    public EcdV60SOAPPort getEcdV60SOAPPort(WebServiceFeature... features) {
        return super.getPort(EcdV60SOAPPort, EcdV60SOAPPort.class, features);
    }

}
