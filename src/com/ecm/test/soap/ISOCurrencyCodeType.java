
package com.ecm.test.soap;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ISOCurrencyCodeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ISOCurrencyCodeType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="AED"/&gt;
 *     &lt;enumeration value="AFN"/&gt;
 *     &lt;enumeration value="ALL"/&gt;
 *     &lt;enumeration value="AMD"/&gt;
 *     &lt;enumeration value="ANG"/&gt;
 *     &lt;enumeration value="AOA"/&gt;
 *     &lt;enumeration value="ARS"/&gt;
 *     &lt;enumeration value="AUD"/&gt;
 *     &lt;enumeration value="AWG"/&gt;
 *     &lt;enumeration value="AZN"/&gt;
 *     &lt;enumeration value="BAM"/&gt;
 *     &lt;enumeration value="BBD"/&gt;
 *     &lt;enumeration value="BDT"/&gt;
 *     &lt;enumeration value="BGN"/&gt;
 *     &lt;enumeration value="BHD"/&gt;
 *     &lt;enumeration value="BIF"/&gt;
 *     &lt;enumeration value="BMD"/&gt;
 *     &lt;enumeration value="BND"/&gt;
 *     &lt;enumeration value="BOB"/&gt;
 *     &lt;enumeration value="BRL"/&gt;
 *     &lt;enumeration value="BSD"/&gt;
 *     &lt;enumeration value="BTN"/&gt;
 *     &lt;enumeration value="BWP"/&gt;
 *     &lt;enumeration value="BYR"/&gt;
 *     &lt;enumeration value="BZD"/&gt;
 *     &lt;enumeration value="CAD"/&gt;
 *     &lt;enumeration value="CDF"/&gt;
 *     &lt;enumeration value="CHF"/&gt;
 *     &lt;enumeration value="CLP"/&gt;
 *     &lt;enumeration value="CNY"/&gt;
 *     &lt;enumeration value="COP"/&gt;
 *     &lt;enumeration value="CRC"/&gt;
 *     &lt;enumeration value="CUP"/&gt;
 *     &lt;enumeration value="CVE"/&gt;
 *     &lt;enumeration value="CZK"/&gt;
 *     &lt;enumeration value="DJF"/&gt;
 *     &lt;enumeration value="DKK"/&gt;
 *     &lt;enumeration value="DOP"/&gt;
 *     &lt;enumeration value="DZD"/&gt;
 *     &lt;enumeration value="EEK"/&gt;
 *     &lt;enumeration value="EGP"/&gt;
 *     &lt;enumeration value="ERN"/&gt;
 *     &lt;enumeration value="ETB"/&gt;
 *     &lt;enumeration value="EUR"/&gt;
 *     &lt;enumeration value="FJD"/&gt;
 *     &lt;enumeration value="FKP"/&gt;
 *     &lt;enumeration value="GBP"/&gt;
 *     &lt;enumeration value="GEL"/&gt;
 *     &lt;enumeration value="GHS"/&gt;
 *     &lt;enumeration value="GIP"/&gt;
 *     &lt;enumeration value="GMD"/&gt;
 *     &lt;enumeration value="GNF"/&gt;
 *     &lt;enumeration value="GTQ"/&gt;
 *     &lt;enumeration value="GYD"/&gt;
 *     &lt;enumeration value="GWP"/&gt;
 *     &lt;enumeration value="HKD"/&gt;
 *     &lt;enumeration value="HNL"/&gt;
 *     &lt;enumeration value="HRK"/&gt;
 *     &lt;enumeration value="HTG"/&gt;
 *     &lt;enumeration value="HUF"/&gt;
 *     &lt;enumeration value="IDR"/&gt;
 *     &lt;enumeration value="ILS"/&gt;
 *     &lt;enumeration value="INR"/&gt;
 *     &lt;enumeration value="IQD"/&gt;
 *     &lt;enumeration value="IRR"/&gt;
 *     &lt;enumeration value="ISK"/&gt;
 *     &lt;enumeration value="JMD"/&gt;
 *     &lt;enumeration value="JOD"/&gt;
 *     &lt;enumeration value="JPY"/&gt;
 *     &lt;enumeration value="KES"/&gt;
 *     &lt;enumeration value="KGS"/&gt;
 *     &lt;enumeration value="KHR"/&gt;
 *     &lt;enumeration value="KMF"/&gt;
 *     &lt;enumeration value="KPW"/&gt;
 *     &lt;enumeration value="KRW"/&gt;
 *     &lt;enumeration value="KWD"/&gt;
 *     &lt;enumeration value="KYD"/&gt;
 *     &lt;enumeration value="KZT"/&gt;
 *     &lt;enumeration value="LAK"/&gt;
 *     &lt;enumeration value="LBP"/&gt;
 *     &lt;enumeration value="LKR"/&gt;
 *     &lt;enumeration value="LRD"/&gt;
 *     &lt;enumeration value="LSL"/&gt;
 *     &lt;enumeration value="LTL"/&gt;
 *     &lt;enumeration value="LVL"/&gt;
 *     &lt;enumeration value="LYD"/&gt;
 *     &lt;enumeration value="MAD"/&gt;
 *     &lt;enumeration value="MDL"/&gt;
 *     &lt;enumeration value="MGA"/&gt;
 *     &lt;enumeration value="MKD"/&gt;
 *     &lt;enumeration value="MMK"/&gt;
 *     &lt;enumeration value="MNT"/&gt;
 *     &lt;enumeration value="MOP"/&gt;
 *     &lt;enumeration value="MRO"/&gt;
 *     &lt;enumeration value="MUR"/&gt;
 *     &lt;enumeration value="MVR"/&gt;
 *     &lt;enumeration value="MWK"/&gt;
 *     &lt;enumeration value="MXN"/&gt;
 *     &lt;enumeration value="MYR"/&gt;
 *     &lt;enumeration value="MZN"/&gt;
 *     &lt;enumeration value="NAD"/&gt;
 *     &lt;enumeration value="NGN"/&gt;
 *     &lt;enumeration value="NIO"/&gt;
 *     &lt;enumeration value="NOK"/&gt;
 *     &lt;enumeration value="NPR"/&gt;
 *     &lt;enumeration value="NZD"/&gt;
 *     &lt;enumeration value="OMR"/&gt;
 *     &lt;enumeration value="PAB"/&gt;
 *     &lt;enumeration value="PEN"/&gt;
 *     &lt;enumeration value="PGK"/&gt;
 *     &lt;enumeration value="PHP"/&gt;
 *     &lt;enumeration value="PKR"/&gt;
 *     &lt;enumeration value="PLN"/&gt;
 *     &lt;enumeration value="PYG"/&gt;
 *     &lt;enumeration value="QAR"/&gt;
 *     &lt;enumeration value="RON"/&gt;
 *     &lt;enumeration value="RSD"/&gt;
 *     &lt;enumeration value="RUB"/&gt;
 *     &lt;enumeration value="RWF"/&gt;
 *     &lt;enumeration value="SAR"/&gt;
 *     &lt;enumeration value="SBD"/&gt;
 *     &lt;enumeration value="SCR"/&gt;
 *     &lt;enumeration value="SDG"/&gt;
 *     &lt;enumeration value="SEK"/&gt;
 *     &lt;enumeration value="SGD"/&gt;
 *     &lt;enumeration value="SHP"/&gt;
 *     &lt;enumeration value="SKK"/&gt;
 *     &lt;enumeration value="SLL"/&gt;
 *     &lt;enumeration value="SOS"/&gt;
 *     &lt;enumeration value="SRD"/&gt;
 *     &lt;enumeration value="STD"/&gt;
 *     &lt;enumeration value="SVC"/&gt;
 *     &lt;enumeration value="SYP"/&gt;
 *     &lt;enumeration value="SZL"/&gt;
 *     &lt;enumeration value="THB"/&gt;
 *     &lt;enumeration value="TJS"/&gt;
 *     &lt;enumeration value="TMM"/&gt;
 *     &lt;enumeration value="TND"/&gt;
 *     &lt;enumeration value="TOP"/&gt;
 *     &lt;enumeration value="TRY"/&gt;
 *     &lt;enumeration value="TTD"/&gt;
 *     &lt;enumeration value="TWD"/&gt;
 *     &lt;enumeration value="TZS"/&gt;
 *     &lt;enumeration value="UAH"/&gt;
 *     &lt;enumeration value="UGX"/&gt;
 *     &lt;enumeration value="USD"/&gt;
 *     &lt;enumeration value="UYU"/&gt;
 *     &lt;enumeration value="UZS"/&gt;
 *     &lt;enumeration value="VEF"/&gt;
 *     &lt;enumeration value="VND"/&gt;
 *     &lt;enumeration value="VUV"/&gt;
 *     &lt;enumeration value="WST"/&gt;
 *     &lt;enumeration value="XAF"/&gt;
 *     &lt;enumeration value="XAG"/&gt;
 *     &lt;enumeration value="XAU"/&gt;
 *     &lt;enumeration value="XCD"/&gt;
 *     &lt;enumeration value="XDR"/&gt;
 *     &lt;enumeration value="XOF"/&gt;
 *     &lt;enumeration value="XPD"/&gt;
 *     &lt;enumeration value="XPF"/&gt;
 *     &lt;enumeration value="XPT"/&gt;
 *     &lt;enumeration value="YER"/&gt;
 *     &lt;enumeration value="ZAR"/&gt;
 *     &lt;enumeration value="ZMK"/&gt;
 *     &lt;enumeration value="ZWR"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ISOCurrencyCodeType")
@XmlEnum
public enum ISOCurrencyCodeType {


    /**
     * Dirham
     * 
     */
    AED,

    /**
     * Afghani
     * 
     */
    AFN,

    /**
     * Lek
     * 
     */
    ALL,

    /**
     * Dram
     * 
     */
    AMD,

    /**
     * Netherlands Antillian Guilder
     * 
     */
    ANG,

    /**
     * Kwanza
     * 
     */
    AOA,

    /**
     * Argentine Peso
     * 
     */
    ARS,

    /**
     * Australian Dollar
     * 
     */
    AUD,

    /**
     * Aruban Guilder
     * 
     */
    AWG,

    /**
     * Azerbaijanian Manat
     * 
     */
    AZN,

    /**
     * Convertible Mark
     * 
     */
    BAM,

    /**
     * Barbados Dollar
     * 
     */
    BBD,

    /**
     * Taka
     * 
     */
    BDT,

    /**
     * Bulgarian Lev
     * 
     */
    BGN,

    /**
     * Bahraini Dinar
     * 
     */
    BHD,

    /**
     * Burundi Franc
     * 
     */
    BIF,

    /**
     * Bermudian Dollar (customarily: Bermuda Dollar)
     * 
     */
    BMD,

    /**
     * Brunei Dollar
     * 
     */
    BND,

    /**
     * Boliviano
     * 
     */
    BOB,

    /**
     * Brazilian Real
     * 
     */
    BRL,

    /**
     * Bahamian Dollar
     * 
     */
    BSD,

    /**
     * Ngultrum
     * 
     */
    BTN,

    /**
     * Pula
     * 
     */
    BWP,

    /**
     * Belarussian Ruble
     * 
     */
    BYR,

    /**
     * Belize Dollar
     * 
     */
    BZD,

    /**
     * Canadian Dollar
     * 
     */
    CAD,

    /**
     * Franc Congolais
     * 
     */
    CDF,

    /**
     * Swiss Franc
     * 
     */
    CHF,

    /**
     * Chilean Peso
     * 
     */
    CLP,

    /**
     * Yuan Renminbi
     * 
     */
    CNY,

    /**
     * Colombian Peso
     * 
     */
    COP,

    /**
     * Costa Rican Colon
     * 
     */
    CRC,

    /**
     * Cuban Peso
     * 
     */
    CUP,

    /**
     * Cape Verde Escudo
     * 
     */
    CVE,

    /**
     * Czech Koruna
     * 
     */
    CZK,

    /**
     * Djibouti Franc
     * 
     */
    DJF,

    /**
     * Danish Krone
     * 
     */
    DKK,

    /**
     * Dominican Peso
     * 
     */
    DOP,

    /**
     * Algerian Dinar
     * 
     */
    DZD,

    /**
     * Kroon
     * 
     */
    EEK,

    /**
     * Egyptian Pound
     * 
     */
    EGP,

    /**
     * Nakfa
     * 
     */
    ERN,

    /**
     * Ethopian Birr
     * 
     */
    ETB,

    /**
     * Euro
     * 
     */
    EUR,

    /**
     * Fiji Dollar
     * 
     */
    FJD,

    /**
     * Falkland Islands Pound
     * 
     */
    FKP,

    /**
     * Pound Sterling
     * 
     */
    GBP,

    /**
     * Lari
     * 
     */
    GEL,

    /**
     * Cedi
     * 
     */
    GHS,

    /**
     * Gibraltar Pound
     * 
     */
    GIP,

    /**
     * Dalasi
     * 
     */
    GMD,

    /**
     * Guinea Franc
     * 
     */
    GNF,

    /**
     * Quetzal
     * 
     */
    GTQ,

    /**
     * Guyana Dollar
     * 
     */
    GYD,

    /**
     * Guinea-Bisau Peso
     * 
     */
    GWP,

    /**
     * Honk Kong Dollar
     * 
     */
    HKD,

    /**
     * Lempira
     * 
     */
    HNL,

    /**
     * Kuna
     * 
     */
    HRK,

    /**
     * Gourde
     * 
     */
    HTG,

    /**
     * Forint
     * 
     */
    HUF,

    /**
     * Rupiah
     * 
     */
    IDR,

    /**
     * New Israeli Sheqel
     * 
     */
    ILS,

    /**
     * Indian Rupee
     * 
     */
    INR,

    /**
     * Iraqi Dinar
     * 
     */
    IQD,

    /**
     * Iranian Rial
     * 
     */
    IRR,

    /**
     * Iceland Krona
     * 
     */
    ISK,

    /**
     * Jamaican Dollar
     * 
     */
    JMD,

    /**
     * Jordanian Dinar
     * 
     */
    JOD,

    /**
     * Yen
     * 
     */
    JPY,

    /**
     * Kenyan Shilling
     * 
     */
    KES,

    /**
     * Som
     * 
     */
    KGS,

    /**
     * Riel
     * 
     */
    KHR,

    /**
     * Comoro Franc
     * 
     */
    KMF,

    /**
     * North Korean Won
     * 
     */
    KPW,

    /**
     * Won
     * 
     */
    KRW,

    /**
     * Kuwaiti Dinar
     * 
     */
    KWD,

    /**
     * Cayman Islands Dollar
     * 
     */
    KYD,

    /**
     * Tenge
     * 
     */
    KZT,

    /**
     * Kip
     * 
     */
    LAK,

    /**
     * Lebanese Pound
     * 
     */
    LBP,

    /**
     * Sri Lanka Rupee
     * 
     */
    LKR,

    /**
     * Liberian Dollar
     * 
     */
    LRD,

    /**
     * Loti
     * 
     */
    LSL,

    /**
     * Lithuanian Litas
     * 
     */
    LTL,

    /**
     * Latvian Lats
     * 
     */
    LVL,

    /**
     * Libyan Dinar
     * 
     */
    LYD,

    /**
     * Morrocan Dirham
     * 
     */
    MAD,

    /**
     * Moldovan Leu
     * 
     */
    MDL,

    /**
     * Malagasy Ariary
     * 
     */
    MGA,

    /**
     * Denar
     * 
     */
    MKD,

    /**
     * Kyat
     * 
     */
    MMK,

    /**
     * Tugrik
     * 
     */
    MNT,

    /**
     * Pataca
     * 
     */
    MOP,

    /**
     * Ouguiya
     * 
     */
    MRO,

    /**
     * Mauritius Rupee
     * 
     */
    MUR,

    /**
     * Rufiyaa
     * 
     */
    MVR,

    /**
     * Kwacha
     * 
     */
    MWK,

    /**
     * Mexican Peso
     * 
     */
    MXN,

    /**
     * Malaysian Ringgit
     * 
     */
    MYR,

    /**
     * Metical
     * 
     */
    MZN,

    /**
     * Namibia Dollar
     * 
     */
    NAD,

    /**
     * Naira
     * 
     */
    NGN,

    /**
     * Cordoba Oro
     * 
     */
    NIO,

    /**
     * Norwegian Krone
     * 
     */
    NOK,

    /**
     * Nepalese Rupee
     * 
     */
    NPR,

    /**
     * New Zealand Dollar
     * 
     */
    NZD,

    /**
     * Rial Omani
     * 
     */
    OMR,

    /**
     * Balboa
     * 
     */
    PAB,

    /**
     * Nuevo Sol
     * 
     */
    PEN,

    /**
     * Kina
     * 
     */
    PGK,

    /**
     * Philippine Peso
     * 
     */
    PHP,

    /**
     * Pakistan Rupee
     * 
     */
    PKR,

    /**
     * Zloty
     * 
     */
    PLN,

    /**
     * Guarani
     * 
     */
    PYG,

    /**
     * Qatari Rial
     * 
     */
    QAR,

    /**
     * New Leu
     * 
     */
    RON,

    /**
     * Serbian Dinar
     * 
     */
    RSD,

    /**
     * Russian Ruble
     * 
     */
    RUB,

    /**
     * Rwanda Franc
     * 
     */
    RWF,

    /**
     * Saudi Riyal
     * 
     */
    SAR,

    /**
     * Solomon Islands Dollar
     * 
     */
    SBD,

    /**
     * Seychelles Rupee
     * 
     */
    SCR,

    /**
     * Sudanese Pound
     * 
     */
    SDG,

    /**
     * Swedish Krona
     * 
     */
    SEK,

    /**
     * Singapore Dollar
     * 
     */
    SGD,

    /**
     * St. Helena Pound
     * 
     */
    SHP,

    /**
     * Slovak Koruna
     * 
     */
    SKK,

    /**
     * Leone
     * 
     */
    SLL,

    /**
     * Somali Shilling
     * 
     */
    SOS,

    /**
     * Suriname Dollar
     * 
     */
    SRD,

    /**
     * Dobra
     * 
     */
    STD,

    /**
     * El Salvador Colon
     * 
     */
    SVC,

    /**
     * Syrian Pound
     * 
     */
    SYP,

    /**
     * Lilangeni
     * 
     */
    SZL,

    /**
     * Baht
     * 
     */
    THB,

    /**
     * Somoni
     * 
     */
    TJS,

    /**
     * Manat
     * 
     */
    TMM,

    /**
     * Tunisian Dinar
     * 
     */
    TND,

    /**
     * Pa'anga
     * 
     */
    TOP,

    /**
     * New Turkish Lira
     * 
     */
    TRY,

    /**
     * Trinidad and Tobago Dollar
     * 
     */
    TTD,

    /**
     * New Taiwan Dollar
     * 
     */
    TWD,

    /**
     * Tanzanian Shilling
     * 
     */
    TZS,

    /**
     * Hryvnia
     * 
     */
    UAH,

    /**
     * Uganda Shilling
     * 
     */
    UGX,

    /**
     * US Dollar
     * 
     */
    USD,

    /**
     * Peso Uruguayo
     * 
     */
    UYU,

    /**
     * Uzbekistan Sum
     * 
     */
    UZS,

    /**
     * Bolivar Fuerte
     * 
     */
    VEF,

    /**
     * Dong
     * 
     */
    VND,

    /**
     * Vatu
     * 
     */
    VUV,

    /**
     * Tala
     * 
     */
    WST,

    /**
     * CFA Franc
     * 
     */
    XAF,

    /**
     * Silver
     * 
     */
    XAG,

    /**
     * Gold
     * 
     */
    XAU,

    /**
     * East Carribean Dollar
     * 
     */
    XCD,

    /**
     * SDR
     * 
     */
    XDR,

    /**
     * CFA Franc
     * 
     */
    XOF,

    /**
     * Palladium
     * 
     */
    XPD,

    /**
     * CFP Franc
     * 
     */
    XPF,

    /**
     * Platinum
     * 
     */
    XPT,

    /**
     * Yemeni Rial
     * 
     */
    YER,

    /**
     * Rand
     * 
     */
    ZAR,

    /**
     * Kwacha
     * 
     */
    ZMK,

    /**
     * Zimbabwe Dollar
     * 
     */
    ZWR;

    public String value() {
        return name();
    }

    public static ISOCurrencyCodeType fromValue(String v) {
        return valueOf(v);
    }

}
