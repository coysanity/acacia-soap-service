
package com.ecm.test.soap;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				=====
 * 				amountDescriptor:: specifies currency amount in the eCMS. Scale of all currency amounts is set during system provisioning. It's typically 2 decimal
 * 				places in the inputs and final results. All internal calculations are performed with full precision (BigDecimal)
 * 				====
 * 			
 * 
 * <p>Java class for amountDescriptor complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="amountDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="currencyCode" type="{urn:com:ecmhp}ISOCurrencyCodeType"/&gt;
 *         &lt;element name="currencyAmount"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal"&gt;
 *               &lt;fractionDigits value="2"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "amountDescriptor", propOrder = {
    "currencyCode",
    "currencyAmount"
})
public class AmountDescriptor {

    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected ISOCurrencyCodeType currencyCode;
    @XmlElement(required = true)
    protected BigDecimal currencyAmount;

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link ISOCurrencyCodeType }
     *     
     */
    public ISOCurrencyCodeType getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link ISOCurrencyCodeType }
     *     
     */
    public void setCurrencyCode(ISOCurrencyCodeType value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the currencyAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCurrencyAmount() {
        return currencyAmount;
    }

    /**
     * Sets the value of the currencyAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCurrencyAmount(BigDecimal value) {
        this.currencyAmount = value;
    }

}
