
package com.ecm.test.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for transferGroupResponseEntry complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="transferGroupResponseEntry"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="groupEntryType" type="{urn:com:ecmhp}possibleTransferGroupEntryTypes"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="subscriberTransferResponse" type="{urn:com:ecmhp}subscriberTransferResponse"/&gt;
 *           &lt;element name="cashInResponse" type="{urn:com:ecmhp}cashInResponse"/&gt;
 *           &lt;element name="cashOutResponse" type="{urn:com:ecmhp}cashOutResponse"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transferGroupResponseEntry", propOrder = {
    "groupEntryType",
    "subscriberTransferResponse",
    "cashInResponse",
    "cashOutResponse"
})
public class TransferGroupResponseEntry {

    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected PossibleTransferGroupEntryTypes groupEntryType;
    protected SubscriberTransferResponse subscriberTransferResponse;
    protected CashInResponse cashInResponse;
    protected CashOutResponse cashOutResponse;

    /**
     * Gets the value of the groupEntryType property.
     * 
     * @return
     *     possible object is
     *     {@link PossibleTransferGroupEntryTypes }
     *     
     */
    public PossibleTransferGroupEntryTypes getGroupEntryType() {
        return groupEntryType;
    }

    /**
     * Sets the value of the groupEntryType property.
     * 
     * @param value
     *     allowed object is
     *     {@link PossibleTransferGroupEntryTypes }
     *     
     */
    public void setGroupEntryType(PossibleTransferGroupEntryTypes value) {
        this.groupEntryType = value;
    }

    /**
     * Gets the value of the subscriberTransferResponse property.
     * 
     * @return
     *     possible object is
     *     {@link SubscriberTransferResponse }
     *     
     */
    public SubscriberTransferResponse getSubscriberTransferResponse() {
        return subscriberTransferResponse;
    }

    /**
     * Sets the value of the subscriberTransferResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubscriberTransferResponse }
     *     
     */
    public void setSubscriberTransferResponse(SubscriberTransferResponse value) {
        this.subscriberTransferResponse = value;
    }

    /**
     * Gets the value of the cashInResponse property.
     * 
     * @return
     *     possible object is
     *     {@link CashInResponse }
     *     
     */
    public CashInResponse getCashInResponse() {
        return cashInResponse;
    }

    /**
     * Sets the value of the cashInResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link CashInResponse }
     *     
     */
    public void setCashInResponse(CashInResponse value) {
        this.cashInResponse = value;
    }

    /**
     * Gets the value of the cashOutResponse property.
     * 
     * @return
     *     possible object is
     *     {@link CashOutResponse }
     *     
     */
    public CashOutResponse getCashOutResponse() {
        return cashOutResponse;
    }

    /**
     * Sets the value of the cashOutResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link CashOutResponse }
     *     
     */
    public void setCashOutResponse(CashOutResponse value) {
        this.cashOutResponse = value;
    }

}
