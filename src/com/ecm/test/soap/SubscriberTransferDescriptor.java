
package com.ecm.test.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				=====
 * 				subscriberTransferDescriptor:: describes a concrete transaction. It identifies its source, destination
 * 				and the amount of currency 'exchanging hands'
 * 				=====
 * 			
 * 
 * <p>Java class for subscriberTransferDescriptor complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="subscriberTransferDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="sourceSubscriber" type="{urn:com:ecmhp}subscriberWithCurrencyDescriptor"/&gt;
 *         &lt;element name="destinationSubscriber" type="{urn:com:ecmhp}subscriberWithCurrencyDescriptor"/&gt;
 *         &lt;element name="transaction" type="{urn:com:ecmhp}transactionDescriptor"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "subscriberTransferDescriptor", propOrder = {
    "sourceSubscriber",
    "destinationSubscriber",
    "transaction"
})
public class SubscriberTransferDescriptor {

    @XmlElement(required = true)
    protected SubscriberWithCurrencyDescriptor sourceSubscriber;
    @XmlElement(required = true)
    protected SubscriberWithCurrencyDescriptor destinationSubscriber;
    @XmlElement(required = true)
    protected TransactionDescriptor transaction;

    /**
     * Gets the value of the sourceSubscriber property.
     * 
     * @return
     *     possible object is
     *     {@link SubscriberWithCurrencyDescriptor }
     *     
     */
    public SubscriberWithCurrencyDescriptor getSourceSubscriber() {
        return sourceSubscriber;
    }

    /**
     * Sets the value of the sourceSubscriber property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubscriberWithCurrencyDescriptor }
     *     
     */
    public void setSourceSubscriber(SubscriberWithCurrencyDescriptor value) {
        this.sourceSubscriber = value;
    }

    /**
     * Gets the value of the destinationSubscriber property.
     * 
     * @return
     *     possible object is
     *     {@link SubscriberWithCurrencyDescriptor }
     *     
     */
    public SubscriberWithCurrencyDescriptor getDestinationSubscriber() {
        return destinationSubscriber;
    }

    /**
     * Sets the value of the destinationSubscriber property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubscriberWithCurrencyDescriptor }
     *     
     */
    public void setDestinationSubscriber(SubscriberWithCurrencyDescriptor value) {
        this.destinationSubscriber = value;
    }

    /**
     * Gets the value of the transaction property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionDescriptor }
     *     
     */
    public TransactionDescriptor getTransaction() {
        return transaction;
    }

    /**
     * Sets the value of the transaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionDescriptor }
     *     
     */
    public void setTransaction(TransactionDescriptor value) {
        this.transaction = value;
    }

}
