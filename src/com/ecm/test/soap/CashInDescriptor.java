
package com.ecm.test.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				=====
 * 				cashInDescriptor:: describes a concrete transaction. It identifies its source, destination
 * 				and the amount of currency 'exchanging hands'. ALL transactions which result in conversion of any other form of money or
 * 				currency, such as coin, bank notes, or promissory notes such as loans, which result in delivery of eCurrency to a subscriber
 * 				are treated as cash-in. All cash-in amounts come from the ECD and move to subscribers.
 * 				=====
 * 			
 * 
 * <p>Java class for cashInDescriptor complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cashInDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="sourceEcd" type="{urn:com:ecmhp}ecdWithCurrencyDescriptor"/&gt;
 *         &lt;element name="destinationSubscriber" type="{urn:com:ecmhp}subscriberWithCurrencyDescriptor"/&gt;
 *         &lt;element name="transaction" type="{urn:com:ecmhp}transactionDescriptor"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cashInDescriptor", propOrder = {
    "sourceEcd",
    "destinationSubscriber",
    "transaction"
})
public class CashInDescriptor {

    @XmlElement(required = true)
    protected EcdWithCurrencyDescriptor sourceEcd;
    @XmlElement(required = true)
    protected SubscriberWithCurrencyDescriptor destinationSubscriber;
    @XmlElement(required = true)
    protected TransactionDescriptor transaction;

    /**
     * Gets the value of the sourceEcd property.
     * 
     * @return
     *     possible object is
     *     {@link EcdWithCurrencyDescriptor }
     *     
     */
    public EcdWithCurrencyDescriptor getSourceEcd() {
        return sourceEcd;
    }

    /**
     * Sets the value of the sourceEcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link EcdWithCurrencyDescriptor }
     *     
     */
    public void setSourceEcd(EcdWithCurrencyDescriptor value) {
        this.sourceEcd = value;
    }

    /**
     * Gets the value of the destinationSubscriber property.
     * 
     * @return
     *     possible object is
     *     {@link SubscriberWithCurrencyDescriptor }
     *     
     */
    public SubscriberWithCurrencyDescriptor getDestinationSubscriber() {
        return destinationSubscriber;
    }

    /**
     * Sets the value of the destinationSubscriber property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubscriberWithCurrencyDescriptor }
     *     
     */
    public void setDestinationSubscriber(SubscriberWithCurrencyDescriptor value) {
        this.destinationSubscriber = value;
    }

    /**
     * Gets the value of the transaction property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionDescriptor }
     *     
     */
    public TransactionDescriptor getTransaction() {
        return transaction;
    }

    /**
     * Sets the value of the transaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionDescriptor }
     *     
     */
    public void setTransaction(TransactionDescriptor value) {
        this.transaction = value;
    }

}
