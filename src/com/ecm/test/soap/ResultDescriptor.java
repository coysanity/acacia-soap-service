
package com.ecm.test.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * 
 * 				=====
 * 				resultDescriptor:: describes a 'positive' (error-free) outcome of all eCMS operations. The mere presence of this element indicates a positive
 * 				conclusion of the requested operation resultMessage may indicate some additional advisory condition but in most cases will
 * 				be a simple two character string 'OK' eCurrencyTransactionId is a globally unique identifier of the transaction performed. It can be
 * 				persisted by the ECD for future audit and reference eCurrencyTransactionTime is the time of the operation performed by eCMS as recorded by
 * 				eCMS. It is persisted in a historical transaction stream. multiple eCurrency transactions can occur within a single eCurrencyTransactionTime
 * 				=====
 * 			
 * 
 * <p>Java class for resultDescriptor complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="resultDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="resultMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="eCurrencyTransactionId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="eCurrencyTransactionTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "resultDescriptor", propOrder = {
    "resultMessage",
    "eCurrencyTransactionId",
    "eCurrencyTransactionTime"
})
public class ResultDescriptor {

    protected String resultMessage;
    @XmlElement(required = true)
    protected String eCurrencyTransactionId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar eCurrencyTransactionTime;

    /**
     * Gets the value of the resultMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResultMessage() {
        return resultMessage;
    }

    /**
     * Sets the value of the resultMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResultMessage(String value) {
        this.resultMessage = value;
    }

    /**
     * Gets the value of the eCurrencyTransactionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getECurrencyTransactionId() {
        return eCurrencyTransactionId;
    }

    /**
     * Sets the value of the eCurrencyTransactionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setECurrencyTransactionId(String value) {
        this.eCurrencyTransactionId = value;
    }

    /**
     * Gets the value of the eCurrencyTransactionTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getECurrencyTransactionTime() {
        return eCurrencyTransactionTime;
    }

    /**
     * Sets the value of the eCurrencyTransactionTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setECurrencyTransactionTime(XMLGregorianCalendar value) {
        this.eCurrencyTransactionTime = value;
    }

}
